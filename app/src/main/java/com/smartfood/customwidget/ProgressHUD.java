package com.smartfood.customwidget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowManager.BadTokenException;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartfood.R;
import com.smartfood.utility.Constants;
import com.smartfood.utility.Utility;


public class ProgressHUD extends Dialog {
    public ProgressHUD(Context context) {
        super(context);
    }

    public ProgressHUD(Context context, int theme) {
        super(context, theme);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        ImageView imageView = (ImageView) findViewById(R.id.spinnerImageView);
        try {
            if (Utility.getIngerSharedPreferences(getContext(), Constants.THEME) == 2) {
                imageView.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.spinner_green));
            } else {
                imageView.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.spinner_yellow));
            }
            AnimationDrawable spinner = (AnimationDrawable) imageView.getBackground();
            spinner.start();
        } catch (Exception e) {
            imageView.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    public void setMessage(CharSequence message) {
        if (message != null && message.length() > 0) {
            //findViewById(R.id.message).setVisibility(View.VISIBLE);
            TextView txt = (TextView) findViewById(R.id.message);
            txt.setText(message);
            txt.invalidate();
        }
    }

    public void setMessage(int message) {
        if (message != 0 && message > 0) {
            //findViewById(R.id.message).setVisibility(View.VISIBLE);
            TextView txt = (TextView) findViewById(R.id.message);
            txt.setText(message);
            txt.invalidate();
        }
    }

    public static ProgressHUD show(Context context, CharSequence message, boolean indeterminate, boolean cancelable,
                                   OnCancelListener cancelListener) {


        ProgressHUD dialog = new ProgressHUD(context, R.style.DialogTheme);
        try {
            dialog.setContentView(R.layout.progress_hud);
            WindowManager manager = (WindowManager) context.getSystemService(Activity.WINDOW_SERVICE);
            int width, height;
            WindowManager.LayoutParams params;

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
                width = manager.getDefaultDisplay().getWidth();
                height = manager.getDefaultDisplay().getHeight();
            } else {
                Point point = new Point();
                manager.getDefaultDisplay().getSize(point);
                width = point.x;
                height = point.y;
            }
            /*dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
            dialog.setTitle("");

            if (message == null || message.length() == 0) {
                dialog.findViewById(R.id.message).setVisibility(View.GONE);
            } else {
                TextView txt = (TextView) dialog.findViewById(R.id.message);
                txt.setText(message);
            }
            dialog.setCancelable(cancelable);
            dialog.setOnCancelListener(cancelListener);
           // dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
            WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = width;
            lp.height = height;
          //  lp.dimAmount = 0.1f;
            dialog.getWindow().setAttributes(lp);

        //    dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            dialog.show();
        } catch (BadTokenException e) {
            e.printStackTrace();
        }
        return dialog;
    }
}
