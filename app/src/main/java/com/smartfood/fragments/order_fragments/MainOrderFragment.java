package com.smartfood.fragments.order_fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smartfood.R;
import com.smartfood.adapter.ViewPagerAdapter;
import com.smartfood.customwidget.CustomTextView;

/**
 * Created by and-05 on 26/12/17.
 */

public class MainOrderFragment extends Fragment {

    ViewPager viewPager;
    TabLayout tabLayout;
    Context context;
    private int[] tabIcons = {
            R.drawable.ic_rejected_white,
            R.drawable.ic_pending_yellow,
            R.drawable.ic_complete_white
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_order, container, false);
        context = getActivity();
        bind(view);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            createTabIcons();

            viewPager.setCurrentItem(1);
        }
    }

    private void bind(View view) {

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        createViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        createTabIcons();

        viewPager.setCurrentItem(1);
    }

    private void createTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.item_tabs, null);
        tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rejected_white, 0, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.item_tabs, null);
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_pending_yellow, 0, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.item_tabs, null);
        tabThree.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_complete_white, 0, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                CustomTextView selectedText = (CustomTextView) view.findViewById(android.R.id.text1);
                selectedText.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                if (tab.getPosition() == 0) {
                    selectedText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rejected_yellow, 0, 0, 0);

                }
                if (tab.getPosition() == 1) {
                    viewPager.setCurrentItem(tab.getPosition());

                    selectedText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_pending_yellow, 0, 0, 0);

                }
                if (tab.getPosition() == 2) {
                    selectedText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_complete_yellow, 0, 0, 0);

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                CustomTextView selectedText = (CustomTextView) view.findViewById(android.R.id.text1);
                selectedText.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
                if (tab.getPosition() == 0) {
                    selectedText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rejected_white, 0, 0, 0);

                }
                if (tab.getPosition() == 1) {
                    selectedText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_pending_white, 0, 0, 0);

                }
                if (tab.getPosition() == 2) {
                    selectedText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_complete_white, 0, 0, 0);

                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void createViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new RejectedFragment(), getString(R.string.rejected));
        adapter.addFragment(new InProgressFragment(), getString(R.string.in_progress));
        adapter.addFragment(new CompletedFragment(), getString(R.string.completed));
        viewPager.setAdapter(adapter);
    }
}
