package com.smartfood.fragments.order_fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartfood.R;
import com.smartfood.adapter.OrderAdapter;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.model.OrderItem;
import com.smartfood.model.OrderParent;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 26/12/17.
 */

public class RejectedFragment extends Fragment {

    Context context;
    ProgressHUD progressHUD;
    ResponseTask responseTask;
    ArrayList<OrderParent> list = new ArrayList<>();
    OrderAdapter orderAdapter;
    final String TAG = RejectedFragment.class.getSimpleName();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recyclerview, container, false);
        context = getActivity();

        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        orderAdapter = new OrderAdapter(context, list);
        recyclerView.setAdapter(orderAdapter);

        serverGetList();


        return view;
    }

    private void serverGetList() {
        if (Utility.isConnectingToInternet(context)){
            progressHUD = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
                jsonObject.put(Constants.STATUS, Constants.REJECT);

                responseTask = new ResponseTask(context, jsonObject,Constants.GET_ORDER, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progressHUD != null && progressHUD.isShowing()) {
                            progressHUD.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    list.clear();

                                    JSONArray jsonArray = json.getJSONArray(Constants.OBJECT);
                                    for (int i = 0; i < jsonArray.length() ; i++) {

                                        JSONObject objOrder = jsonArray.getJSONObject(i);
                                        JSONArray arrayMeal = objOrder.getJSONArray(Constants.MY_ORDER);

                                        ArrayList<OrderItem> mealItem = new ArrayList<>();

                                        for (int j = 0; j < arrayMeal.length() ; j++) {

                                            JSONObject obj = arrayMeal.getJSONObject(j);

                                            JSONArray arrayExtra = obj.getJSONArray(Constants.EXTRA);

                                            StringBuilder str_extra = new StringBuilder();
                                            for (int k = 0; k < arrayExtra.length(); k++) {
                                                if (k == arrayExtra.length() - 1) {
                                                    str_extra.append(arrayExtra.getJSONObject(k).getString(Constants.NAME));
                                                } else {
                                                    str_extra.append(arrayExtra.getJSONObject(k).getString(Constants.NAME)).append(" + ");
                                                }
                                            }

                                            mealItem.add(new OrderItem(obj.getString(Constants.MENU_ID), obj.getString(Constants.MENU_NAME),
                                                    obj.getString(Constants.TOTAL),obj.getString(Constants.QUANTITY), obj.getString(Constants.MENU_IMAGE),
                                                    obj.getString(Constants.MENU_TITLE),str_extra.toString(),obj.getString(Constants.SUBTOTAL)));
                                        }

                                        String cancel_reason="";
                                        if (objOrder.has(Constants.REJECT_WHY)){
                                            cancel_reason = objOrder.getString(Constants.REJECT_WHY);
                                        }

                                        list.add(new OrderParent(objOrder.getString(Constants.ORDER_ID),objOrder.getString(Constants.RESTRO_NAME),
                                                objOrder.getString(Constants.STATUS),cancel_reason, objOrder.getString(Constants.DELIVERY_CHARGE),
                                                objOrder.getString(Constants.GRAND_TOTAL),objOrder.getString(Constants.DATE),
                                                objOrder.getString(Constants.ORDER_ID),objOrder.getString(Constants.RESTRO_IMAGE),
                                                objOrder.getString(Constants.DELIVERY_LAT),objOrder.getString(Constants.DELIVERY_LON),mealItem));


                                    }

                                    orderAdapter.notifyDataSetChanged();

                                } else {
                                    list.clear();
                                    orderAdapter.notifyDataSetChanged();
                                    // Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else{
            Utility.ShowToastMessage(context , context.getString(R.string.not_connected_to_internet));
        }
    }


}
