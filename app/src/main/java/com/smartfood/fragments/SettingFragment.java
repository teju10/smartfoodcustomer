package com.smartfood.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartfood.R;
import com.smartfood.activities.HomeActivity;
import com.smartfood.customwidget.CustomButton;
import com.smartfood.utility.Constants;
import com.smartfood.utility.Utility;

/**
 * Created by and-05 on 8/11/17.
 */

public class SettingFragment extends Fragment implements View.OnClickListener{

    CustomButton arabic,english,btn_off,btn_on;
    Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting , container , false);
        context = getActivity();
        bind(view);
        return view;
    }

    private void bind(View view) {

        arabic = (CustomButton)view.findViewById(R.id.arabic);
        english = (CustomButton)view.findViewById(R.id.english);
        btn_off = (CustomButton)view.findViewById(R.id.btn_off);
        btn_on = (CustomButton)view.findViewById(R.id.btn_on);

        arabic.setOnClickListener(this);
        english.setOnClickListener(this);
        btn_off.setOnClickListener(this);
        btn_on.setOnClickListener(this);

        if (Utility.getLanSharedPreferences(context, Constants.LANGUAGE,0) == 1){
            changeColor(arabic , english);
        }else if (Utility.getLanSharedPreferences(context, Constants.LANGUAGE,0) == 0) {
            changeColor(english , arabic);
        }

            changeColor(btn_on , btn_off);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.arabic:
                Utility.setLanSharedPreference(context , Constants.LANGUAGE, 1);
                changeColor(arabic , english);
                startActivity(new Intent(context , HomeActivity.class));
                getActivity().finish();
                break;
            case R.id.english:
                Utility.setLanSharedPreference(context , Constants.LANGUAGE, 0);
                changeColor(english , arabic);
                startActivity(new Intent(context , HomeActivity.class));
                getActivity().finish();
                break;
            case R.id.btn_off:
                changeColor(btn_off , btn_on);
                break;
            case R.id.btn_on:
                changeColor(btn_on, btn_off);
                break;
        }
    }

    private void changeColor(CustomButton btn1, CustomButton btn2) {

        if(Utility.getIngerSharedPreferences(context,Constants.THEME)==2){
            btn1.setBackgroundColor(ContextCompat.getColor(context, R.color.primary_green));
        }else{
            btn1.setBackgroundColor(ContextCompat.getColor(context, R.color.primary_green));
        }
        btn1.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
        btn2.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite));
        btn2.setTextColor(ContextCompat.getColor(context, R.color.colorBlue));

    }
}
