package com.smartfood.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartfood.R;
import com.smartfood.activities.MapAddressActivity;
import com.smartfood.adapter.AddressAdapter;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 9/12/17.
 */

public class MyAddressFragment extends Fragment implements View.OnClickListener{

    Context context;
    ResponseTask responseTask;
    ProgressHUD progressHUD;
    final String TAG = MyAddressFragment.class.getSimpleName();
    ArrayList<JSONObject> list = new ArrayList<>();
    AddressAdapter addressAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_address, container , false);
        context = getActivity();
        ((CardView)view.findViewById(R.id.btn_add_loc)).setOnClickListener(this);
        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        addressAdapter = new AddressAdapter(context , list,"");
        recyclerView.setAdapter(addressAdapter);
        serverAddress();
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_add_loc:
                startActivity(new Intent(context , MapAddressActivity.class)
                        .putExtra(Constants.IS_UPDATE,""));
                Utility.activityTransition(context);
                break;
        }
    }

    private void serverAddress() {

        progressHUD = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context , Constants.USER_ID));

            responseTask = new ResponseTask(context, jsonObject,Constants.GET_MY_ADDRESS, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progressHUD != null && progressHUD.isShowing()) {
                        progressHUD.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {
                                list.clear();
                                JSONArray jsonArray = json.getJSONArray(Constants.OBJECT);

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    list.add(jsonArray.getJSONObject(i));
                                }
                                addressAdapter.notifyDataSetChanged();
                            } else {
                                Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (MapAddressActivity.IsUpdate){
            serverAddress();
            MapAddressActivity.IsUpdate = false;
        }
    }
}
