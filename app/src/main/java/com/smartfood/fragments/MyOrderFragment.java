package com.smartfood.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.smartfood.R;
import com.smartfood.activities.OrderDetailsActivity;
import com.smartfood.adapter.OrderAdapter;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.model.OrderItem;
import com.smartfood.model.OrderParent;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 24/11/17.
 */

public class MyOrderFragment extends Fragment implements View.OnClickListener {

    Context context;
    LinearLayout ln_in_progress, ln_completed, ln_rejected;
    CustomTextView rejected, completed , in_progress;
    ImageView ic_rejected, ic_in_progress, ic_completed;
    ResponseTask responseTask;
    ProgressHUD progressHUD;
    final String TAG = MyOrderFragment.class.getSimpleName();
    String str_status = Constants.INPROGRESS;

    ArrayList<OrderParent> list = new ArrayList<>();
    OrderAdapter orderAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View  view = inflater.inflate(R.layout.fragment_myorder, container , false);
        context = getActivity();
        bind(view);
        return view;
    }

    private void bind(View view) {

        ln_rejected = (LinearLayout)view.findViewById(R.id.ln_rejected);
        ln_completed = (LinearLayout)view.findViewById(R.id.ln_completed);
        ln_in_progress = (LinearLayout)view.findViewById(R.id.ln_in_progress);
        completed = (CustomTextView)view.findViewById(R.id.completed);
        in_progress = (CustomTextView)view.findViewById(R.id.in_progress);
        rejected = (CustomTextView)view.findViewById(R.id.rejected);
        ic_completed = (ImageView)view.findViewById(R.id.ic_completed);
        ic_in_progress = (ImageView)view.findViewById(R.id.ic_in_progress);
        ic_rejected = (ImageView)view.findViewById(R.id.ic_rejected);

        ln_rejected.setOnClickListener(this);
        ln_completed.setOnClickListener(this);
        ln_in_progress.setOnClickListener(this);

        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        orderAdapter = new OrderAdapter(context, list);
        recyclerView.setAdapter(orderAdapter);


        ChangeColors(ln_in_progress, ln_rejected , ln_completed , in_progress, rejected, completed);
        ic_completed.setBackgroundResource(R.drawable.ic_complete_white);
        ic_in_progress.setBackgroundResource(R.drawable.ic_pending_yellow);
        ic_rejected.setBackgroundResource(R.drawable.ic_rejected_white);

        serverGetList();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ln_rejected:
                str_status = Constants.REJECT;
                ChangeColors(ln_rejected , ln_in_progress, ln_completed, rejected , in_progress, completed);
                ic_completed.setBackgroundResource(R.drawable.ic_complete_white);
                ic_in_progress.setBackgroundResource(R.drawable.ic_pending_white);
                ic_rejected.setBackgroundResource(R.drawable.ic_rejected_yellow);
                serverGetList();

                break;
            case R.id.ln_in_progress:
                str_status = Constants.INPROGRESS;
                ChangeColors(ln_in_progress, ln_rejected , ln_completed , in_progress, rejected, completed);
                ic_completed.setBackgroundResource(R.drawable.ic_complete_white);
                ic_in_progress.setBackgroundResource(R.drawable.ic_pending_yellow);
                ic_rejected.setBackgroundResource(R.drawable.ic_rejected_white);
                serverGetList();
                break;
            case R.id.ln_completed:
                str_status = Constants.COMPLETE;
                ChangeColors( ln_completed,ln_rejected , ln_in_progress,completed,rejected , in_progress);
                ic_completed.setBackgroundResource(R.drawable.ic_complete_yellow);
                ic_in_progress.setBackgroundResource(R.drawable.ic_pending_white);
                ic_rejected.setBackgroundResource(R.drawable.ic_rejected_white);
                serverGetList();

                break;

        }
    }

    private void ChangeColors(LinearLayout ln1 , LinearLayout ln2,LinearLayout ln3, CustomTextView tv1 , CustomTextView tv2, CustomTextView tv3) {

        ln1.setBackgroundColor(ContextCompat.getColor(context,R.color.colorWhite));
        ln2.setBackgroundColor(ContextCompat.getColor(context,R.color.colorPrimary));
        ln3.setBackgroundColor(ContextCompat.getColor(context,R.color.colorPrimary));
        tv1.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
        tv2.setTextColor(ContextCompat.getColor(context,R.color.colorWhite));
        tv3.setTextColor(ContextCompat.getColor(context,R.color.colorWhite));

    }

    private void serverGetList() {
        if (Utility.isConnectingToInternet(context)){
            progressHUD = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
                jsonObject.put(Constants.STATUS, str_status);

                responseTask = new ResponseTask(context, jsonObject,Constants.GET_ORDER, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progressHUD != null && progressHUD.isShowing()) {
                            progressHUD.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    list.clear();

                                    JSONArray jsonArray = json.getJSONArray(Constants.OBJECT);
                                    for (int i = 0; i < jsonArray.length() ; i++) {

                                        JSONObject objOrder = jsonArray.getJSONObject(i);
                                        JSONArray arrayMeal = objOrder.getJSONArray(Constants.MY_ORDER);

                                        ArrayList<OrderItem> mealItem = new ArrayList<>();

                                        for (int j = 0; j < arrayMeal.length() ; j++) {

                                            JSONObject obj = arrayMeal.getJSONObject(j);

                                            JSONArray arrayExtra = obj.getJSONArray(Constants.EXTRA);

                                            StringBuilder str_extra = new StringBuilder();
                                            for (int k = 0; k < arrayExtra.length(); k++) {
                                                if (k == arrayExtra.length() - 1) {
                                                    str_extra.append(arrayExtra.getJSONObject(k).getString(Constants.NAME));
                                                } else {
                                                    str_extra.append(arrayExtra.getJSONObject(k).getString(Constants.NAME)).append(" + ");
                                                }
                                            }

                                            mealItem.add(new OrderItem(obj.getString(Constants.MENU_ID), obj.getString(Constants.MENU_NAME),
                                                    obj.getString(Constants.TOTAL),obj.getString(Constants.QUANTITY), obj.getString(Constants.MENU_IMAGE),
                                                    obj.getString(Constants.MENU_TITLE),str_extra.toString(),obj.getString(Constants.SUBTOTAL)));
                                        }

                                        String cancel_reason="";
                                        if (objOrder.has(Constants.REJECT_WHY)){
                                            cancel_reason = objOrder.getString(Constants.REJECT_WHY);
                                        }

                                        list.add(new OrderParent(objOrder.getString(Constants.ORDER_ID),objOrder.getString(Constants.RESTRO_NAME),
                                                objOrder.getString(Constants.STATUS),cancel_reason, objOrder.getString(Constants.DELIVERY_CHARGE),
                                                objOrder.getString(Constants.GRAND_TOTAL),objOrder.getString(Constants.DATE),
                                                objOrder.getString(Constants.ORDER_ID),objOrder.getString(Constants.RESTRO_IMAGE),
                                                objOrder.getString(Constants.DELIVERY_LAT),objOrder.getString(Constants.DELIVERY_LON),mealItem));


                                    }

                                    orderAdapter.notifyDataSetChanged();

                                } else {
                                    list.clear();
                                    orderAdapter.notifyDataSetChanged();
                                   // Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else{
            Utility.ShowToastMessage(context , context.getString(R.string.not_connected_to_internet));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (OrderDetailsActivity.update){
            serverGetList();
            OrderDetailsActivity.update = false;
        }
    }
}
