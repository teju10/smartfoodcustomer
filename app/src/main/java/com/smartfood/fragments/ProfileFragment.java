package com.smartfood.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.smartfood.BuildConfig;
import com.smartfood.R;
import com.smartfood.activities.CartListActivity;
import com.smartfood.customwidget.CustomButton;
import com.smartfood.customwidget.CustomEditText;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.customwidget.other.Util;
import com.smartfood.utility.Constants;
import com.smartfood.utility.MultipartResponseListener;
import com.smartfood.utility.MultipartTask;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;
import com.soundcloud.android.crop.Crop;

import org.json.JSONObject;

import java.io.File;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;

/**
 * Created by and-05 on 9/11/17.
 */

public class ProfileFragment extends Fragment implements View.OnClickListener {

    CustomEditText first_name,last_name,phone_no,password;
    CustomTextView full_name;
    ImageView profile_image;
    ResponseTask responseTask;
    ProgressHUD progresshud;
    Context context;
    final String TAG = ProfileFragment.class.getSimpleName();
    MultipartTask multipartTask;
    String imagepath;
    final int PERMISSION_REQUEST_CODE = 101;
    String [] requestedPermissions = {CAMERA, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE};
    Dialog dialog;
    RelativeLayout rl_password;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_profile , container , false);
        context = getActivity();
        bindView(view);

        return view;
    }

    private void bindView(View view) {
        first_name = (CustomEditText) view.findViewById(R.id.first_name);
        last_name = (CustomEditText) view.findViewById(R.id.last_name);
        phone_no = (CustomEditText) view.findViewById(R.id.phone_no);
        password = (CustomEditText) view.findViewById(R.id.password);
        full_name = (CustomTextView) view.findViewById(R.id.full_name);
        profile_image = (ImageView) view.findViewById(R.id.profile_image);
        rl_password = (RelativeLayout) view.findViewById(R.id.rl_password);

        (view.findViewById(R.id.ln_first)).setOnClickListener(this);
        (view.findViewById(R.id.ln_last)).setOnClickListener(this);
        (view.findViewById(R.id.ln_phone)).setOnClickListener(this);
        (view.findViewById(R.id.ln_password)).setOnClickListener(this);
        (view.findViewById(R.id.btn_update)).setOnClickListener(this);
        (view.findViewById(R.id.btn_edit_img)).setOnClickListener(this);

        if (Utility.isConnectingToInternet(context))
            serverGetProfile();
        else
            Utility.ShowToastMessage(context , context.getString(R.string.not_connected_to_internet));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ln_first:
                first_name.setFocusableInTouchMode(true);
                first_name.requestFocus();
                first_name.setSelection(first_name.getText().length());
                break;
            case R.id.ln_last:
                last_name.setFocusableInTouchMode(true);
                last_name.requestFocus();
                last_name.setSelection(last_name.getText().length());
                break;
            case R.id.ln_phone:
                phone_no.setFocusableInTouchMode(true);
                phone_no.requestFocus();
                phone_no.setSelection(phone_no.getText().length());
                break;
            case R.id.ln_password:
                openDialog();
                break;
            case R.id.btn_update:
                if (Utility.isConnectingToInternet(context)) {
                    if (first_name.getText().toString().isEmpty()) {
                        Utility.ShowToastMessage(context, getString(R.string.error_enter_first_name));
                    } else if (!Utility.isValidText(first_name.getText().toString())) {
                        Utility.ShowToastMessage(context, getString(R.string.error_valid_first_name));
                    } else if (last_name.getText().toString().isEmpty()) {
                        Utility.ShowToastMessage(context, getString(R.string.error_enter_last_name));
                    } else if (!Utility.isValidText(last_name.getText().toString())) {
                        Utility.ShowToastMessage(context, getString(R.string.error_valid_last_name));
                    } else if (phone_no.getText().toString().isEmpty()) {
                        Utility.ShowToastMessage(context, getString(R.string.error_enter_phone_number));
                    } else {
                        serverUpdateProfile();
                    }
                }else{
                    Utility.ShowToastMessage(context , context.getString(R.string.not_connected_to_internet));
                }
                break;
            case R.id.btn_edit_img:

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(), CAMERA) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        selectImage();
                    } else {
                        // Show rationale and request permission.
                        requestCameraPermission();
                    }
                } else {
                    selectImage();
                }

                break;
        }
    }

    private void openDialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_change_pass);
        dialog.getWindow().setLayout(DrawerLayout.LayoutParams.MATCH_PARENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        dialog.show();

        final CustomEditText old_password = (CustomEditText)dialog.findViewById(R.id.old_password);
        final CustomEditText new_password = (CustomEditText)dialog.findViewById(R.id.new_password);
        final CustomEditText confirm_password = (CustomEditText)dialog.findViewById(R.id.confirm_password);

        ((CustomButton)dialog.findViewById(R.id.btn_submit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utility.isConnectingToInternet(context)) {
                    if (old_password.getText().toString().isEmpty()) {
                        Utility.ShowToastMessage(context, getString(R.string.error_enter_old_password));
                    } else if (new_password.getText().toString().isEmpty()) {
                        Utility.ShowToastMessage(context, getString(R.string.error_enter_new_password));
                    } else if (new_password.getText().length() < 6) {
                        Utility.ShowToastMessage(context, getString(R.string.error_password_six_character));
                    } else if (confirm_password.getText().toString().isEmpty()) {
                        Utility.ShowToastMessage(context, getString(R.string.error_enter_confirm_password));
                    } else if (!new_password.getText().toString().equals(confirm_password.getText().toString())) {
                        Utility.ShowToastMessage(context, getString(R.string.error_passwords_doesnt_match));
                    } else {
                        serverChangePassword(old_password.getText().toString(),
                                new_password.getText().toString());
                    }
                }else{
                    Utility.ShowToastMessage(context , context.getString(R.string.not_connected_to_internet));
                }
            }
        });


    }

    private void serverChangePassword(String oldpass, String newpass) {
        progresshud = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context , Constants.USER_ID));
            jsonObject.put(Constants.OPW, oldpass);
            jsonObject.put(Constants.NPW, newpass);

            responseTask = new ResponseTask(context, jsonObject,Constants.CHANGE_PASSWORD, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                Utility.ShowToastMessage(context , getString(R.string.password_changed_successfully));
                                dialog.dismiss();
                            } else {
                                Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void serverGetProfile() {

        progresshud = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context , Constants.USER_ID));

            responseTask = new ResponseTask(context, jsonObject,Constants.GET_PROFILE, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                JSONObject object = json.getJSONObject(Constants.OBJECT);
                                first_name.setText(object.getString(Constants.F_NAME));
                                last_name.setText(object.getString(Constants.L_NAME));
                                phone_no.setText(object.getString(Constants.CONTACT));

                                String str_full_name= first_name.getText().toString() +" "+last_name.getText().toString();
                                full_name.setText(str_full_name);

                                Glide.with(context).load(object.getString(Constants.PROFILE_IMAGE))
                                        .into(profile_image);

                                if (!object.getString(Constants.REGISTER_BY).equals("")){
                                    rl_password.setVisibility(View.GONE);
                                }

                            } else {
                                Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void serverUpdateProfile() {

        progresshud = ProgressHUD.show(context, getString(R.string.loading), false, false, null);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.USER_ID , Utility.getSharedPreferences(context , Constants.USER_ID));
            jsonObject.put(Constants.F_NAME , first_name.getText().toString().trim());
            jsonObject.put(Constants.L_NAME , last_name.getText().toString());
            jsonObject.put(Constants.CONTACT , phone_no.getText().toString());

            if (imagepath != null && !imagepath.equals("")) {
                multipartTask = new MultipartTask(context, jsonObject, Constants.UPDATE_PROFILE, Constants.PROFILE_IMAGE,
                        imagepath, TAG, "POST");
                multipartTask.execute();
                multipartTask.setListener(new MultipartResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progresshud.isShowing()) {
                            progresshud.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject jsonObject1 = new JSONObject(result);
                                if (jsonObject1.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {
                                    Utility.ShowToastMessage(context, jsonObject1.getString(Constants.MSG));
                                    updateUi();
                                } else {
                                    Utility.ShowToastMessage(context, jsonObject1.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }else{
                responseTask = new ResponseTask(context, jsonObject,Constants.UPDATE_PROFILE, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progresshud != null && progresshud.isShowing()) {
                            progresshud.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                    updateUi();
                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateUi() {

        String str = first_name.getText().toString()+" "+last_name.getText().toString();

        first_name.setFocusable(false);
        last_name.setFocusable(false);
        phone_no.setFocusable(false);
        full_name.setText(str);

        Utility.setSharedPreference(context , Constants.FULLNAME,str);
        Utility.setSharedPreference(context , Constants.CONTACT,phone_no.getText().toString());

    }

    //------------------------Profile image crop-----------------------------

    private void selectImage() {

        final CharSequence[] options = {"From Camera", "From Gallery", "Close"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add your photo...");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("From Camera")) {
                    Uri outputFileUri = getCaptureImageOutputUri();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, 1);
                } else if (options[item].equals("From Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals("Close")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            File getImage = context.getExternalCacheDir();
            if (getImage != null) {
                outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
            }
        } else {
            File getImage = context.getExternalCacheDir();
            if (getImage != null) {
                outputFileUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider",
                        new File(getImage.getPath(), "pickImageResult.jpeg"));
            }
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private void beginCrop(Uri source) {
        File pro = new File(Utility.MakeDir(Constants.SDCARD_FOLDER_PATH, context), System.currentTimeMillis() + ".jpg");
        Uri destination1 = Uri.fromFile(pro);
        //Crop.of(source, destination1).asSquare().withAspect(200, 200).start(c);
        Crop.of(source, destination1).asSquare().start(context, this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            imagepath = Crop.getOutput(result).getPath();
            Glide.with(context).load(imagepath).into(profile_image);

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(getActivity(), Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                Uri imageUri = getPickImageResultUri(data);
                beginCrop(imageUri);
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                beginCrop(selectedImage);
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }
        else{
            Log.e(TAG, "onActivityResult: "+"else " );
        }
    }
    //----------------------permission request---------------------------------------------------
    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), CAMERA)
                | ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), WRITE_EXTERNAL_STORAGE)
                |ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getString(R.string.camera_permission_needed));
            builder.setPositiveButton(R.string.grant, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ActivityCompat.requestPermissions(getActivity(), requestedPermissions, PERMISSION_REQUEST_CODE);
                }
            }).create().show();

        }else {
            requestPermissions(new String[]{CAMERA, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Checking the request code of our request
        if (requestCode == PERMISSION_REQUEST_CODE) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectImage();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getString(R.string.camera_permission_needed));
                builder.setPositiveButton(R.string.enable, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.setData(Uri.parse("package:" + context.getPackageName()));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        startActivity(intent);
                    }
                }).create().show();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.cart_menu, menu);


        MenuItem item = menu.findItem(R.id.cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();
        // Log.e(TAG, "onCreateOptionsMenu: "+Utility.getSharedPreferences(context, COUNT,0));
        Util.setBadgeCount(getActivity(), icon,Utility.getSharedPreferences(context, Constants.CART_COUNT,0));

        super.onCreateOptionsMenu(menu,inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.cart){
            startActivity(new Intent(context , CartListActivity.class));
            Utility.activityTransition(context);
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().invalidateOptionsMenu();
    }

}
