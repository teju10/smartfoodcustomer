package com.smartfood.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.smartfood.R;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.model.Notification;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 19/12/17.
 */

public class Notification1Fragment extends Fragment {

    private ArrayList<Notification> listNotification = new ArrayList<>();
    private NotificationAdapter notificationAdapter;
    Context context;
    ResponseTask responseTask;
    ProgressHUD progresshud;
    final String TAG = Notification1Fragment.class.getSimpleName();
    private SwipeMenuListView listviewNoti;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification1, container, false);
        context = getActivity();

        setViews(view);

        return view;
    }

    private void setViews(View view) {
        listviewNoti = (SwipeMenuListView) view.findViewById(R.id.notification_list);
        callNotification();

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(context);
                if (Utility.getIngerSharedPreferences(context, Constants.THEME) == 2) {
                    deleteItem.setBackground(new ColorDrawable(getResources().getColor(R.color.primary_green)));
                } else {
                    deleteItem.setBackground(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
                }
                deleteItem.setWidth(120);
                deleteItem.setIcon(R.drawable.ic_delete);
                menu.addMenuItem(deleteItem);
            }
        };

        listviewNoti.setMenuCreator(creator);
        listviewNoti.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        listviewNoti.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                callDelteNotification(listNotification.get(position).getId(), position);
                return false;
            }
        });

    }

    private void callNotification() {
        if (Utility.isConnectingToInternet(context)) {
            progresshud = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));

                responseTask = new ResponseTask(context, jsonObject, Constants.GET_NOTIFICATION, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progresshud != null && progresshud.isShowing()) {
                            progresshud.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    JSONArray jsonArray = json.getJSONArray(Constants.OBJECT);

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        listNotification.add(new Notification(jsonArray.getJSONObject(i).getString(Constants.NOTI_ID),
                                                jsonArray.getJSONObject(i).getString(Constants.MSG), jsonArray.getJSONObject(i).getString(Constants.STATUS)));

                                    }
                                    notificationAdapter = new NotificationAdapter(context, 0, listNotification);
                                    listviewNoti.setAdapter(notificationAdapter);

                                } else {
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utility.ShowToastMessage(context, context.getString(R.string.not_connected_to_internet));
        }

    }

    private void callDelteNotification(String id, final int position) {
        if (Utility.isConnectingToInternet(context)) {
            progresshud = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
                jsonObject.put(Constants.NOTI_ID, id);

                responseTask = new ResponseTask(context, jsonObject, Constants.DELETE_NOTI, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progresshud != null && progresshud.isShowing()) {
                            progresshud.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    listNotification.remove(position);
                                    notificationAdapter.notifyDataSetChanged();
                                    Utility.ShowToastMessage(context, context.getString(R.string.notification_has_deleted));

                                } else {
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utility.ShowToastMessage(context, context.getString(R.string.not_connected_to_internet));
        }

    }

    public class NotificationAdapter extends ArrayAdapter<Notification>     {

        public NotificationAdapter(@NonNull Context con, @LayoutRes int resource, @NonNull ArrayList<Notification> list) {
            super(con, resource, list);
            this.con = con;
            listNotification = list;

        }

        private ArrayList<Notification> listNotification;
        private Context con;

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public int getCount() {
            return listNotification.size();
        }

        @Override
        public Notification getItem(int position) {
            return listNotification.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater mInflater = (LayoutInflater) con
                        .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = mInflater.inflate(
                        R.layout.item_notification, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.notification = (CustomTextView) convertView.findViewById(R.id.notification);
                viewHolder.img = (ImageView) convertView.findViewById(R.id.ic_notification);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            Notification notification = listNotification.get(position);
            viewHolder.notification.setText(notification.getValue());

            switch (notification.getStatus()) {
                case "0":
                    viewHolder.img.setBackgroundResource(R.drawable.ic_order_pending);
                    break;
                case "1":
                    viewHolder.img.setBackgroundResource(R.drawable.ic_inprogress);
                    break;
                case "2":
                    viewHolder.img.setBackgroundResource(R.drawable.ic_on_the_way_br);
                    break;
                case "3":
                    viewHolder.img.setBackgroundResource(R.drawable.ic_order_complete);
                    break;
                case "4":
                    viewHolder.img.setBackgroundResource(R.drawable.ic_cross_red);
                    break;
            }


            return convertView;
        }

        class ViewHolder {
            CustomTextView notification;
            ImageView img;
        }
    }
}
