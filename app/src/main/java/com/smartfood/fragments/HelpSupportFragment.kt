package com.smartfood.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.smartfood.R
import com.smartfood.customwidget.CustomEditText
import com.smartfood.customwidget.ProgressHUD
import com.smartfood.utility.Constants
import com.smartfood.utility.ResponseTask
import com.smartfood.utility.Utility
import org.json.JSONObject

/**
 * Created by and-05 on 23/11/17.
 */
class HelpSupportFragment : Fragment() , View.OnClickListener {

    lateinit var name: CustomEditText
    lateinit var phone_no:CustomEditText
    lateinit var email:CustomEditText
    lateinit var question:CustomEditText
    lateinit var spinner:Spinner
    internal lateinit var context: Context
    lateinit var responseTask : ResponseTask
    lateinit var progressHUD : ProgressHUD
    internal var listCountry  =  ArrayList<String>()
    internal val TAG = HelpSupportFragment::class.java.simpleName


    /*override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val view = inflater!!.inflate(R.layout.fragment_help_support , container , false)
        context = activity

    }*/
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_help_support , container , false)
        context = activity!!.applicationContext

        view.findViewById<View>(R.id.btn_submit).setOnClickListener(this)
        name = view.findViewById<View>(R.id.full_name) as CustomEditText
        phone_no = view.findViewById<View>(R.id.phone_no) as CustomEditText
        email = view.findViewById<View>(R.id.email) as CustomEditText
        question = view.findViewById<View>(R.id.questions) as CustomEditText
        spinner = view.findViewById<View>(R.id.spinner) as Spinner

        listCountry.add(getString(R.string.select_country))

        serverGetCountry()

        return  view
    }

    private fun serverGetCountry() {

        progressHUD = ProgressHUD.show(context, getString(R.string.loading), true, false, null)
        try {

            responseTask = ResponseTask(context,Constants.LIST_COUNTRY, TAG, "GET")
            responseTask.execute()
            responseTask.setListener { result ->
                if (progressHUD.isShowing()) {
                    progressHUD.dismiss()
                }
                if (result == null) {
                    Utility.ShowToastMessage(context, getString(R.string.server_not_responding))
                } else {
                    try {
                        val json = JSONObject(result)
                        if (json.getString(Constants.SUCCESS).equals("1", ignoreCase = true)) {

                            val `jsonArray` = json.getJSONArray(Constants.OBJECT)
                            for (i in 0 until jsonArray.length()){
                                listCountry.add(jsonArray.getJSONObject(i).getString(Constants.COUNTRY_NAME))
                            }

                            val adapter = ArrayAdapter<String>(context, R.layout.item_spinner_, R.id.text, listCountry)
                            spinner.setAdapter(adapter)

                        } else {
                            Utility.ShowToastMessage(context, json.getString(Constants.MSG))
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    override fun onClick(view: View?) {
        when(view!!.id){
            R.id.btn_submit -> if (name.text.isEmpty()){
                Utility.ShowToastMessage(context , getString(R.string.error_enter_name))
            }else if (!Utility.isValidText(name.text.toString())){
                Utility.ShowToastMessage(context , getString(R.string.error_enter_valid_name))
            }else if (email.text.isEmpty()){
                Utility.ShowToastMessage(context , getString(R.string.error_enter_email_address))
            }else if (!Utility.isValidEmail(email.text.toString())){
                Utility.ShowToastMessage(context , getString(R.string.error_valid_email))
            }else if (phone_no.text.isEmpty()){
                Utility.ShowToastMessage(context , getString(R.string.error_enter_phone_number))
            }else if (spinner.selectedItemPosition == 0){
                Utility.ShowToastMessage(context , getString(R.string.error_select_country))
            }else if (question.text.isEmpty()){
                Utility.ShowToastMessage(context , getString(R.string.error_enter_problem_question))
            }else{
                serverSubmit()
            }
        }
    }

    private fun serverSubmit() {
        progressHUD = ProgressHUD.show(context, getString(R.string.loading), true, false, null)
        try {
            val jsonObject = JSONObject()
            jsonObject.put(Constants.NAME,name.text.trim())
            jsonObject.put(Constants.MSG,question.text.trim())
            jsonObject.put(Constants.PHONE,phone_no.text.trim())
            jsonObject.put(Constants.COUNTRY, spinner.selectedItem.toString())
            jsonObject.put(Constants.EMAIL,email.text.trim())
            jsonObject.put(Constants.USER_TYPE,"customer")


            responseTask = ResponseTask(context,jsonObject,Constants.HELP_SUPPORT, TAG, "post")
            responseTask.execute()
            responseTask.setListener { result ->
                if (progressHUD.isShowing()) {
                    progressHUD.dismiss()
                }
                if (result == null) {
                    Utility.ShowToastMessage(context, getString(R.string.server_not_responding))
                } else {
                    try {
                        val json = JSONObject(result)
                        if (json.getString(Constants.SUCCESS).equals("1", ignoreCase = true)) {
                            Utility.ShowToastMessage(context, getString(R.string.question_problem_sent_successfully))

                            phone_no.setText("")
                            email.setText("")
                            name.setText("")
                            question.setText("")
                            spinner.setSelection(0)

                        } else {
                            Utility.ShowToastMessage(context, json.getString(Constants.MSG))
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}