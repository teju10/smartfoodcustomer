package com.smartfood.fragments.fav_fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.smartfood.R;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.utility.Constants;
import com.smartfood.utility.Utility;

/**
 * Created by and-05 on 24/11/17.
 */

public class FavoriteFragment extends Fragment implements View.OnClickListener {

    Context context;
    LinearLayout ln_meals, ln_restro;
    ImageView ic_restro, ic_meals;
    CustomTextView fav_restro, fav_meals;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);
        context = getActivity();
        bind(view);
        return view;
    }

    private void bind(View view) {

        ln_restro = (LinearLayout) view.findViewById(R.id.ln_restro);
        ln_meals = (LinearLayout) view.findViewById(R.id.ln_meals);
        ic_meals = (ImageView) view.findViewById(R.id.ic_meals);
        ic_restro = (ImageView) view.findViewById(R.id.ic_restro);
        fav_meals = (CustomTextView) view.findViewById(R.id.fav_meals);
        fav_restro = (CustomTextView) view.findViewById(R.id.fav_restro);

        ln_restro.setOnClickListener(this);
        ln_meals.setOnClickListener(this);

        ChangeColors(ln_meals, ln_restro, fav_meals, fav_restro);
        if (Utility.getIngerSharedPreferences(context, Constants.THEME) == 2) {
            ic_meals.setBackgroundResource(R.drawable.ic_meals_green);
        } else {
            ic_meals.setBackgroundResource(R.drawable.ic_meals_yellow);
        }
        ic_restro.setBackgroundResource(R.drawable.ic_restro_white);

        getFragmentManager().beginTransaction().add(R.id.container, new FavMealFragment()).commit();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ln_restro:
                ChangeColors(ln_restro, ln_meals, fav_restro, fav_meals);

                if (Utility.getIngerSharedPreferences(context, Constants.THEME) == 2) {
                    ic_restro.setBackgroundResource(R.drawable.ic_restro_green);
                } else {
                    ic_restro.setBackgroundResource(R.drawable.ic_restro_yellow);
                }
                ic_meals.setBackgroundResource(R.drawable.ic_meals_white);
                getFragmentManager().beginTransaction().replace(R.id.container, new FavRestaurantFragment()).commit();

                break;
            case R.id.ln_meals:
                ChangeColors(ln_meals, ln_restro, fav_meals, fav_restro);
                if (Utility.getIngerSharedPreferences(context, Constants.THEME) == 2) {
                    ic_meals.setBackgroundResource(R.drawable.ic_meals_green);
                } else {
                    ic_meals.setBackgroundResource(R.drawable.ic_meals_yellow);
                }
                ic_restro.setBackgroundResource(R.drawable.ic_restro_white);
                getFragmentManager().beginTransaction().replace(R.id.container, new FavMealFragment()).commit();

                break;
        }
    }

    private void ChangeColors(LinearLayout ln1, LinearLayout ln2,
                              CustomTextView tv1, CustomTextView tv2) {
        if (Utility.getIngerSharedPreferences(context, Constants.THEME) == 2) {
            ln1.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite));
            ln2.setBackgroundColor(ContextCompat.getColor(context, R.color.primary_green));

            tv1.setTextColor(ContextCompat.getColor(context, R.color.primary_green));
            tv2.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
        } else {
            ln1.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite));
            ln2.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));

            tv1.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            tv2.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
        }
    }
}
