package com.smartfood.fragments.fav_fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smartfood.R;
import com.smartfood.activities.MenuDetailsActivity;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 24/11/17.
 */

public class FavMealFragment extends Fragment {

    Context context;
    ResponseTask responseTask;
    ProgressHUD progressHUD;
    final String TAG = FavMealFragment.class.getSimpleName();
    FavMealAdapter favMealAdapter;
    ArrayList<JSONObject> list = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recyclerview, container , false);
        context = getActivity();
        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        favMealAdapter = new FavMealAdapter(context , list);
        recyclerView.setAdapter(favMealAdapter);

        serverGetList();

        return view;
    }

    private void serverGetList() {
        if (Utility.isConnectingToInternet(context)){
            progressHUD = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));

                responseTask = new ResponseTask(context, jsonObject,Constants.FAV_ITEM, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progressHUD != null && progressHUD.isShowing()) {
                            progressHUD.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    JSONArray jsonArray = json.getJSONArray(Constants.OBJECT);
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        list.add(jsonArray.getJSONObject(i));

                                    }
                                    favMealAdapter.notifyDataSetChanged();


                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else{
            Utility.ShowToastMessage(context , context.getString(R.string.not_connected_to_internet));
        }
    }

    class FavMealAdapter extends RecyclerView.Adapter<FavMealAdapter.MainViewHolder> {

        private ArrayList<JSONObject> favList;
        private Context context;
        private final int EMPTY_TYPE =0;

        public FavMealAdapter(Context context, ArrayList<JSONObject> favList) {
            this.context = context;
            this.favList = favList;
        }

        @Override
        public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == EMPTY_TYPE){
                return new EmptyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_empty_textview, parent, false));

            }else{
                return new FavViewHolder(LayoutInflater.from(context).inflate(R.layout.item_fav_meal, parent, false));
            }
        }

        @Override
        public void onBindViewHolder(MainViewHolder holder, int position) {

            if (holder instanceof EmptyViewHolder){
                EmptyViewHolder vh = (EmptyViewHolder)holder;
                vh.textView.setText(R.string.no_fav_meal);

            }else if (holder instanceof FavViewHolder){
                FavViewHolder vh = (FavViewHolder)holder;
                try{

                    String str_price = favList.get(position).getString(Constants.MENU_PRICE)+" "+context.getString(R.string.currency);
                    String str_off_price = favList.get(position).getString(Constants.DISCOUNT_PRICE)+" "+context.getString(R.string.currency);

                    if (str_price.equals(str_off_price)){
                        vh.discount_price.setText(str_off_price);
                        vh.item_price.setText("");
                    }else{
                        vh.item_price.setPaintFlags(vh.item_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        vh.discount_price.setText(str_off_price);
                        vh.item_price.setText(str_price);
                    }


                 //   String item_price = favList.get(position).getString(Constants.MENU_PRICE)+" "+context.getString(R.string.currency);
                    vh.menu_title.setText(favList.get(position).getString(Constants.MENU_TITLE));
                    vh.menu_name.setText(favList.get(position).getString(Constants.MENU_NAME));
                    vh.restaurant_name.setText(favList.get(position).getString(Constants.RESTRO_NAME));
                   // vh.discount_price.setText(favList.get(position).getString(Constants.DISCOUNT_PRICE));
                  //  vh.item_price.setText(item_price);


                    Glide.with(context).load(favList.get(position).getString(Constants.MENU_IMAGE)).into(vh.ic_menu);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }

        }

        @Override
        public int getItemCount() {
            if (favList.size() == 0){
                return favList.size()+1;
            }else{
                return favList.size();
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (favList.size() == 0){
                return EMPTY_TYPE;
            }else{
                return 1;
            }
        }

        class MainViewHolder extends RecyclerView.ViewHolder{

            MainViewHolder(View itemView) {
                super(itemView);
            }
        }

        public class FavViewHolder extends MainViewHolder implements View.OnClickListener {

            CustomTextView menu_name, menu_title,discount_price, restaurant_name, item_price;
            ImageView ic_menu;

            FavViewHolder(View itemView) {
                super(itemView);
                menu_title = (CustomTextView)itemView.findViewById(R.id.menu_title);
                menu_name = (CustomTextView)itemView.findViewById(R.id.menu_name);
                restaurant_name = (CustomTextView)itemView.findViewById(R.id.restaurant_name);
                item_price = (CustomTextView)itemView.findViewById(R.id.item_price);
                discount_price = (CustomTextView)itemView.findViewById(R.id.discount_price);
                ic_menu = (ImageView) itemView.findViewById(R.id.ic_menu);

                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {

                try {

                    JSONObject object = favList.get(getAdapterPosition());
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.RESTAURANT_ID,object.getString(Constants.RESTAURANT_ID));
                    bundle.putString(Constants.MENU_ID,object.getString(Constants.MENU_ID));
                    bundle.putString(Constants.MENU_NAME,object.getString(Constants.MENU_NAME));
                    bundle.putString(Constants.MENU_TITLE,object.getString(Constants.MENU_TITLE));
                    bundle.putString(Constants.MENU_PRICE,object.getString(Constants.MENU_PRICE));
                    bundle.putString(Constants.MENU_IMAGE,object.getString(Constants.MENU_IMAGE));
                    bundle.putString(Constants.DISCOUNT_PRICE,object.getString(Constants.DISCOUNT_PRICE));
                    bundle.putString(Constants.IS_UPDATE,"");

                    startActivity(new Intent(context , MenuDetailsActivity.class)
                            .putExtras(bundle));
                    Utility.activityTransition(context);
                }catch (Exception e){
                    e.printStackTrace();
                }



            }
        }

        public class EmptyViewHolder extends MainViewHolder {

            CustomTextView textView;

            public EmptyViewHolder(View itemView) {
                super(itemView);
                textView = (CustomTextView)itemView.findViewById(R.id.text);
            }
        }
    }
}
