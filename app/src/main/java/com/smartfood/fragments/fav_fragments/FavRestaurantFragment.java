package com.smartfood.fragments.fav_fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.smartfood.R;
import com.smartfood.activities.RestaurantDetailActivity;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 24/11/17.
 */

public class FavRestaurantFragment extends Fragment {

    Context context;
    ResponseTask responseTask;
    ProgressHUD progressHUD;
    final String TAG = FavRestaurantFragment.class.getSimpleName();
    ArrayList<JSONObject> list = new ArrayList<>();
    FavRestroAdapter favRestroAdapter;
    double lat, lon;
    private FusedLocationProviderClient mFusedLocationClient;
    protected Location mLastLocation;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recyclerview, container, false);
        context = getActivity();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        favRestroAdapter = new FavRestroAdapter(context, list);
        recyclerView.setAdapter(favRestroAdapter);
        //serverGetList();
        return view;
    }

    private void serverGetList() {
        if (Utility.isConnectingToInternet(context)) {
            progressHUD = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
                jsonObject.put(Constants.LATITUDE, lat);
                jsonObject.put(Constants.LONGITUDE, lon);

                responseTask = new ResponseTask(context, jsonObject, Constants.FAV_RESTRO, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progressHUD != null && progressHUD.isShowing()) {
                            progressHUD.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {
                                    list.clear();
                                    JSONArray jsonArray = json.getJSONArray(Constants.OBJECT);
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        list.add(jsonArray.getJSONObject(i));

                                    }
                                    favRestroAdapter.notifyDataSetChanged();

                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Utility.ShowToastMessage(context, context.getString(R.string.not_connected_to_internet));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getLastLocation();
    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.getLastLocation().addOnCompleteListener(getActivity(), new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                if (task.isSuccessful() && task.getResult() != null) {
                    mLastLocation = task.getResult();
                    lat = mLastLocation.getLatitude();
                    lon = mLastLocation.getLongitude();
                    serverGetList();
                } else {
                    Log.w(TAG, "getLastLocation:exception", task.getException());
                    Utility.ShowToastMessage(context, getString(R.string.no_location_detected));
                }
            }
        });
    }


    class FavRestroAdapter extends RecyclerView.Adapter<FavRestroAdapter.MainViewHolder> {

        private ArrayList<JSONObject> favList;
        private Context context;
        private final int EMPTY_TYPE =0;

        public FavRestroAdapter(Context context, ArrayList<JSONObject> favList) {
            this.context = context;
            this.favList = favList;
        }

        @Override
        public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == EMPTY_TYPE){
                return new EmptyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_empty_textview, parent, false));

            }else{
                return new FavViewHolder(LayoutInflater.from(context).inflate(R.layout.item_fav_restro, parent, false));
            }
        }

        @Override
        public void onBindViewHolder(MainViewHolder holder, int position) {

            if (holder instanceof EmptyViewHolder){
                EmptyViewHolder vh = (EmptyViewHolder)holder;
                vh.textView.setText(R.string.no_fav_restro);

            }else if (holder instanceof FavViewHolder){
                FavViewHolder vh = (FavViewHolder)holder;
                try{
                    vh.restro_name.setText(favList.get(position).getString(Constants.RESTRO_NAME));

                    if (favList.get(position).getString(Constants.RESTRO_TITLE).equals(""))
                        vh.restro_title.setText(context.getString(R.string.n_a));
                    else
                        vh.restro_title.setText(favList.get(position).getString(Constants.RESTRO_TITLE));

                    if (favList.get(position).getString(Constants.OPEN_NOW).equals("1"))
                        vh.is_open.setText(R.string.open_now);
                    else
                        vh.is_open.setText(R.string.closed_now);

                    Glide.with(context).load(favList.get(position).getString(Constants.LOGO_IMAGE)).into(vh.ic_logo);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }

        }

        @Override
        public int getItemCount() {
            if (favList.size() == 0){
                return favList.size()+1;
            }else{
                return favList.size();
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (favList.size() == 0){
                return EMPTY_TYPE;
            }else{
                return 1;
            }
        }

        class MainViewHolder extends RecyclerView.ViewHolder{

            MainViewHolder(View itemView) {
                super(itemView);
            }
        }

        public class FavViewHolder extends MainViewHolder implements View.OnClickListener{

            CustomTextView is_open, restro_name, restro_title;
            ImageView ic_logo;

            FavViewHolder(View itemView) {
                super(itemView);
                restro_title = (CustomTextView)itemView.findViewById(R.id.restro_title);
                restro_name = (CustomTextView)itemView.findViewById(R.id.restro_name);
                is_open = (CustomTextView)itemView.findViewById(R.id.is_open);
                ic_logo = (ImageView) itemView.findViewById(R.id.ic_logo);

                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                startActivity(new Intent(context , RestaurantDetailActivity.class)
                        .putExtra(Constants.OBJECT , favList.get(getAdapterPosition()).toString()));
            }
        }

        public class EmptyViewHolder extends MainViewHolder {

            CustomTextView textView;

            public EmptyViewHolder(View itemView) {
                super(itemView);
                textView = (CustomTextView)itemView.findViewById(R.id.text);
            }
        }
    }

}
