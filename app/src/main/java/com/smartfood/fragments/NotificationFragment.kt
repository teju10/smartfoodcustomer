package com.smartfood.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.smartfood.R
import com.smartfood.customwidget.CustomTextView
import com.smartfood.customwidget.ProgressHUD
import com.smartfood.model.Simple
import com.smartfood.utility.Constants
import com.smartfood.utility.ResponseListener
import com.smartfood.utility.ResponseTask
import com.smartfood.utility.Utility
import org.json.JSONObject

/**
 * Created by and-05 on 14/12/17.
 */
class NotificationFragment : Fragment(){

    internal lateinit var context : Context
    internal lateinit var notificationAdapter : NotificationAdapter
    internal var list = ArrayList<Simple>()
    internal var TAG = NotificationFragment::class.simpleName
    internal lateinit var progresshud : ProgressHUD
    internal lateinit var responseTask : ResponseTask

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_recyclerview, container, false)
        context = activity!!.applicationContext

     /*   list.add("ddddddddddddddd")
        list.add("wwwwwwwwww")
        list.add("ddddddddd")
        list.add("eeeeeeeee")
        list.add("cccccccccc")
        list.add("qqqqqqqqq")*/

        val recyclerview = view.findViewById<View>(R.id.recyclerview) as RecyclerView
        recyclerview.layoutManager = LinearLayoutManager(context)
        notificationAdapter = NotificationAdapter(context, list)
        recyclerview.adapter = notificationAdapter

        serverGetNotification()


        return view
    }

    private fun serverGetNotification() {
        progresshud = ProgressHUD.show(context, getString(R.string.loading), true, false, null)
        try {
            val jsonObject = JSONObject()
             jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context , Constants.USER_ID))

            responseTask = ResponseTask(context, jsonObject, Constants.GET_NOTIFICATION, TAG, "post")
            responseTask.execute()
            responseTask.setListener(ResponseListener { result ->
                if (progresshud.isShowing()) {
                    progresshud.dismiss()
                }
                if (result == null) {
                    Utility.ShowToastMessage(context, getString(R.string.server_not_responding))
                } else {
                    try {
                        val json = JSONObject(result)
                        if (json.getString(Constants.SUCCESS).equals("1", ignoreCase = true)) {

                            val `jsonArray` = json.getJSONArray(Constants.OBJECT)

                            for (i in 0 until jsonArray.length()) {
                                list.add(Simple(jsonArray.getJSONObject(i).getString(Constants.STATUS),
                                        jsonArray.getJSONObject(i).getString(Constants.MSG)))
                            }

                            notificationAdapter.notifyDataSetChanged()

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    class NotificationAdapter(private val context: Context,private val list: ArrayList<Simple>) : RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder>() {


        override fun onBindViewHolder(holder: NotificationViewHolder?, position: Int) {

            holder!!.message.text = list[position].value
            when {
                list[position].id == "0" -> holder.img.setBackgroundResource(R.drawable.ic_order_pending)
                list[position].id == "1" -> holder.img.setBackgroundResource(R.drawable.ic_inprogress)
                list[position].id == "2" -> holder.img.setBackgroundResource(R.drawable.ic_on_the_way_br)
                list[position].id == "3" -> holder.img.setBackgroundResource(R.drawable.ic_order_complete)
                list[position].id == "4" -> holder.img.setBackgroundResource(R.drawable.ic_cross_red)
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): NotificationViewHolder {
            return NotificationViewHolder(LayoutInflater.from(context)
                    .inflate(R.layout.item_notification, parent , false))
        }

        override fun getItemCount()= list.size


        inner class NotificationViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView){

            var message: CustomTextView
            var img : ImageView

            init {
                message = itemView!!.findViewById<View>(R.id.notification) as CustomTextView
                img = itemView.findViewById<View>(R.id.ic_notification) as ImageView
            }
        }

    }

}