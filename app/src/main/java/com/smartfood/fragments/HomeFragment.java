package com.smartfood.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.smartfood.R;
import com.smartfood.activities.CartListActivity;
import com.smartfood.activities.FeelingLuckyActivity;
import com.smartfood.activities.FeelingLucky_SpinnerActivity;
import com.smartfood.activities.HomeActivity;
import com.smartfood.activities.LuckyWheel_SpinnerActivity;
import com.smartfood.activities.RestaurantListActivity;
import com.smartfood.adapter.HomeOrderAdapter;
import com.smartfood.customwidget.CustomButton;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.customwidget.other.Util;
import com.smartfood.model.OrderItem;
import com.smartfood.model.OrderParent;
import com.smartfood.model.Simple;
import com.smartfood.utility.Constants;
import com.smartfood.utility.MyBounceInterpolator;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 8/11/17.
 */

public class HomeFragment extends Fragment implements View.OnClickListener {

    final String TAG = HomeFragment.class.getSimpleName();
    ArrayList<OrderParent> orderList = new ArrayList<>();
    HomeOrderAdapter homeOrderAdapter;
    Context context;
    Spinner spinner_kitchen;
    ResponseTask responseTask;
    ProgressHUD progressHUD, progressHUD1;
    ArrayList<Simple> listKitchen = new ArrayList<>();
    ArrayAdapter<Simple> adapter;
    String kitchen_id = "";
    ImageView lucky_wheel_btn;
    boolean CLICK = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        context = getActivity();

        if (Utility.getIngerSharedPreferences(context, Constants.THEME) == 2) {
            ((ScrollView) view.findViewById(R.id.scroll_main)).setBackground(ContextCompat.getDrawable(context, R.drawable.bg_green));
        } else {
            ((ScrollView) view.findViewById(R.id.scroll_main)).setBackground(ContextCompat.getDrawable(context, R.drawable.bg_yellow));

        }

        bindView(view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);

    }

    private void bindView(View view) {

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        lucky_wheel_btn = view.findViewById(R.id.lucky_wheel_btn);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        homeOrderAdapter = new HomeOrderAdapter(context, orderList);
        recyclerView.setAdapter(homeOrderAdapter);

        spinner_kitchen = (Spinner) view.findViewById(R.id.spinner_kitchen);

        (view.findViewById(R.id.find_restaurant)).setOnClickListener(this);
        lucky_wheel_btn.setOnClickListener(this);

        view.findViewById(R.id.feel_lucky_meals) .setOnClickListener(this);
        view.findViewById(R.id.feel_lucky_restaurant).setOnClickListener(this);

        listKitchen.add(new Simple("", "All types"));
        adapter = new ArrayAdapter<>(context, R.layout.item_spinner, R.id.text, listKitchen);
        spinner_kitchen.setAdapter(adapter);

        spinner_kitchen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (CLICK){
                    Simple simple = listKitchen.get(i);
                    kitchen_id = simple.getId();
                    if (kitchen_id.equals("10")) {
                        //2=green & 1=yellow
                        ((HomeActivity) context).Dialog();
                    } else {
                        ((HomeActivity) context).TurnstoYellow();
                    }
                }else{
                    CLICK = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        getKitchens();
        getOrders();
    }

    private void getKitchens() {

        if (Utility.isConnectingToInternet(context)) {

            progressHUD = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                responseTask = new ResponseTask(context, Constants.LIST_KITCHEN, TAG, "get");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progressHUD != null && progressHUD.isShowing()) {
                            progressHUD.dismiss();
                        }
                        if (result == null) {

                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    JSONArray jsonArray = json.getJSONArray(Constants.OBJECT);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        listKitchen.add(new Simple(object.getString(Constants.KITCHEN_ID),
                                                object.getString(Constants.KITCHEN_NAME)));
                                    }
                                    adapter.notifyDataSetChanged();
                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else
            Utility.ShowToastMessage(context, context.getString(R.string.not_connected_to_internet));
    }

    private void getOrders() {

        if (Utility.isConnectingToInternet(context)) {

            progressHUD1 = ProgressHUD.show(getActivity(), getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));

                responseTask = new ResponseTask(getActivity(), jsonObject, Constants.GET_ORDER, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progressHUD1 != null && progressHUD1.isShowing()) {
                            progressHUD1.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {
                                    //BubbleAnimBtn();
                                    JSONArray jsonArray = json.getJSONArray(Constants.OBJECT);
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject objOrder = jsonArray.getJSONObject(i);
                                        JSONArray arrayMeal = objOrder.getJSONArray(Constants.MY_ORDER);

                                        ArrayList<OrderItem> mealItem = new ArrayList<>();

                                        for (int j = 0; j < arrayMeal.length(); j++) {

                                            JSONObject obj = arrayMeal.getJSONObject(j);

                                            JSONArray arrayExtra = obj.getJSONArray(Constants.EXTRA);

                                            StringBuilder str_extra = new StringBuilder();
                                            for (int k = 0; k < arrayExtra.length(); k++) {
                                                if (k == arrayExtra.length() - 1) {
                                                    str_extra.append(arrayExtra.getJSONObject(k).getString(Constants.NAME));
                                                } else {
                                                    str_extra.append(arrayExtra.getJSONObject(k).getString(Constants.NAME)).append(" + ");
                                                }
                                            }

                                            mealItem.add(new OrderItem(obj.getString(Constants.MENU_ID), obj.getString(Constants.MENU_NAME),
                                                    obj.getString(Constants.TOTAL), obj.getString(Constants.QUANTITY), obj.getString(Constants.MENU_IMAGE),
                                                    obj.getString(Constants.MENU_TITLE), str_extra.toString(), obj.getString(Constants.SUBTOTAL)));
                                        }

                                        String cancel_reason = "";
                                        if (objOrder.has(Constants.REJECT_WHY)) {
                                            cancel_reason = objOrder.getString(Constants.REJECT_WHY);
                                        }

                                        orderList.add(new OrderParent(objOrder.getString(Constants.ORDER_ID), objOrder.getString(Constants.RESTRO_NAME),
                                                objOrder.getString(Constants.STATUS), cancel_reason, objOrder.getString(Constants.DELIVERY_CHARGE),
                                                objOrder.getString(Constants.GRAND_TOTAL), objOrder.getString(Constants.DATE),
                                                objOrder.getString(Constants.ORDER_ID), objOrder.getString(Constants.RESTRO_IMAGE),
                                                objOrder.getString(Constants.DELIVERY_LAT), objOrder.getString(Constants.DELIVERY_LON), mealItem));


                                    }

                                    homeOrderAdapter.notifyDataSetChanged();

                                } /*else {
                                    list.clear();
                                    orderAdapter.notifyDataSetChanged();
                                    // Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }*/
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else
            Utility.ShowToastMessage(context, context.getString(R.string.not_connected_to_internet));


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.find_restaurant:
                startActivity(new Intent(context, RestaurantListActivity.class)
                        .putExtra(Constants.KITCHEN_TYPES, kitchen_id));
                Utility.activityTransition(context);
                break;
            case R.id.lucky_wheel_btn:
                startActivity(new Intent(context, LuckyWheel_SpinnerActivity.class));
             //   Utility.activityTransition(context);
                break;

            case R.id.feel_lucky_meals:
                Utility.setSharedPreference(context,Constants.Lucky_Meal_Btn,"2");
                startActivity(new Intent(context,FeelingLucky_SpinnerActivity.class));
                break;

            case R.id.feel_lucky_restaurant:
                Utility.setSharedPreference(context,Constants.Lucky_Meal_Btn,"1");
                startActivity(new Intent(context,FeelingLucky_SpinnerActivity.class));
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.cart_menu, menu);


        MenuItem item = menu.findItem(R.id.cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();
        // Log.e(TAG, "onCreateOptionsMenu: "+Utility.getSharedPreferences(context, COUNT,0));
        Util.setBadgeCount(getActivity(), icon, Utility.getSharedPreferences(context, Constants.CART_COUNT, 0));

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.cart) {
            startActivity(new Intent(context, CartListActivity.class));
            Utility.activityTransition(context);
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().invalidateOptionsMenu();
    }

    private void BubbleAnimBtn() {

        final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);
        double animationDuration = 2 * 1000;
        myAnim.setDuration((long) animationDuration);

        MyBounceInterpolator interpolator = new MyBounceInterpolator(.25, 15);

        myAnim.setInterpolator(interpolator);
        lucky_wheel_btn.startAnimation(myAnim);
    }

}
