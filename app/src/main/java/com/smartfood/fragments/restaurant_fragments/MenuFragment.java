package com.smartfood.fragments.restaurant_fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;

import com.smartfood.R;
import com.smartfood.adapter.MenuListAdapter;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.model.MenuItem;
import com.smartfood.model.MenuList;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 13/11/17.
 */

public class MenuFragment extends Fragment{

    Context context;
    ProgressHUD progresshud;
    ResponseTask responseTask;
    final String TAG = MenuFragment.class.getSimpleName();
    ArrayList<MenuItem> listMenuItem = new ArrayList<>();
    ArrayList<MenuList> listMenu = new ArrayList<>();
    ExpandableListView lvExp;
    Bundle bundle;
    MenuListAdapter menuAdapter;
    RelativeLayout rl_view;
    CustomTextView empty_text;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container , false);
        context = getActivity();
        lvExp = (ExpandableListView)view.findViewById(R.id.lvExp);
        rl_view = (RelativeLayout) view.findViewById(R.id.rl_view);
        empty_text = (CustomTextView) view.findViewById(R.id.text);
        lvExp.setVisibility(View.GONE);

        bundle = getArguments();
        menuAdapter = new MenuListAdapter(context, listMenu, listMenuItem, bundle.getString(Constants.RESTAURANT_ID));
        lvExp.setAdapter(menuAdapter);

        if (Utility.isConnectingToInternet(context))
            serverGetMenu();
        else
            Utility.ShowToastMessage(context , context.getString(R.string.not_connected_to_internet));


        return view;
    }

    private void serverGetMenu() {

        progresshud = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ID, bundle.getString(Constants.RESTAURANT_ID));

            responseTask = new ResponseTask(context, jsonObject,Constants.GET_MENU_BY_ID, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, context.getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                listMenu.clear();
                                rl_view.setVisibility(View.GONE);
                                lvExp.setVisibility(View.VISIBLE);

                                JSONObject object = json.getJSONObject(Constants.OBJECT);
                                JSONArray jsonArray = object.getJSONArray(Constants.MENU_LIST);
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    JSONArray jarray = obj.getJSONArray(Constants.MENU_ITEM);

                                    listMenuItem = new ArrayList<>();

                                    for (int j = 0; j < jarray.length() ; j++) {

                                        JSONObject ob = jarray.getJSONObject(j);
                                        listMenuItem.add(new MenuItem(ob.getString(Constants.MENU_ID),ob.getString(Constants.MENU_NAME),
                                                ob.getString(Constants.MENU_PRICE),ob.getString(Constants.DISCOUNT_PRICE),
                                                ob.getString(Constants.MENU_IMAGE),ob.getString(Constants.MENU_TITLE)));
                                    }

                                    listMenu.add(new MenuList(obj.getString(Constants.CAT_ID),
                                            obj.getString(Constants.CAT_NAME),listMenuItem));

                                }

                                menuAdapter.notifyDataSetChanged();
                            } else {
                                lvExp.setVisibility(View.GONE);
                                rl_view.setVisibility(View.VISIBLE);
                                empty_text.setText(R.string.no_menu_available);
                                //Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
