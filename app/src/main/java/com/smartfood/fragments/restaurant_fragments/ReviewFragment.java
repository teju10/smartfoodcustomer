package com.smartfood.fragments.restaurant_fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smartfood.R;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 13/11/17.
 */

public class ReviewFragment extends Fragment {

    Context context;
    ArrayList<JSONObject> listReview = new ArrayList<>();
    ProgressHUD progresshud;
    ResponseTask responseTask;
    final String TAG = ReviewFragment.class.getSimpleName();
    ReviewAdapter reviewAdapter;
    Bundle bundle;
    RecyclerView rv_review;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_review, container , false);
        context = getActivity();
        bundle = getArguments();
        rv_review = (RecyclerView)view.findViewById(R.id.rv_review);
        rv_review.setLayoutManager(new LinearLayoutManager(context));
        reviewAdapter = new ReviewAdapter(context , listReview );
        rv_review.setAdapter(reviewAdapter);

        if (Utility.isConnectingToInternet(context))
            serverGetReview();
        else
            Utility.ShowToastMessage(context , context.getString(R.string.not_connected_to_internet));


        return view;
    }

    private void serverGetReview() {

        progresshud = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.RESTAURANT_ID, bundle.getString(Constants.RESTAURANT_ID));

            responseTask = new ResponseTask(context, jsonObject, Constants.GET_REVIEW_BY_ID, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                JSONArray jsonArray = json.getJSONArray(Constants.OBJECT);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    listReview.add(object);
                                }

                                reviewAdapter.notifyDataSetChanged();


                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MainViewHolder> {

        Context context;
        ArrayList<JSONObject> listReview;
        private final int EMPTY_TYPE =0;
        private final int Main_TYPE =1;

        @Override
        public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == EMPTY_TYPE){
                return new EmptyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_empty_textview, parent, false));

            }else{
                return new ReviewViewHolder(LayoutInflater.from(context).inflate(R.layout.item_review, parent, false));
            }

        }

        public ReviewAdapter(Context context, ArrayList<JSONObject> listReview) {
            this.context = context;
            this.listReview = listReview;
            Log.e(TAG, "ReviewAdapter: "+listReview.size() );
        }

        @Override
        public void onBindViewHolder(MainViewHolder holder, int position) {


            if (holder instanceof EmptyViewHolder){
                EmptyViewHolder vh = (EmptyViewHolder)holder;
                vh.textView.setText(R.string.there_are_no_reviews);

            }else if (holder instanceof ReviewViewHolder) {
                ReviewViewHolder  viewHolder = (ReviewViewHolder)holder;
                try {
                    JSONObject object = listReview.get(position);
                    viewHolder.review.setText(object.getString(Constants.REVIEW));
                    viewHolder.user_name.setText(object.getString(Constants.FULLNAME));
                    viewHolder.time.setText(object.getString(Constants.DIFF));

                    Glide.with(context).load(object.getString(Constants.PROFILE_IMAGE)).into(viewHolder.img_profile);

                    Utility.Rating(Float.parseFloat(object.getString(Constants.RATE)), viewHolder.star1, viewHolder.star2, viewHolder.star3,
                            viewHolder.star4, viewHolder.star5, R.drawable.ic_star_y_empty,
                            R.drawable.ic_star_y_half, R.drawable.ic_star_y_filled);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

        @Override
        public int getItemCount() {
            if (listReview.size() == 0){
                return listReview.size()+1;
            }else{
                return listReview.size();
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (listReview.size() == 0){
                return EMPTY_TYPE;
            }else{
                return Main_TYPE;
            }
        }

        class MainViewHolder extends RecyclerView.ViewHolder{

            MainViewHolder(View itemView) {
                super(itemView);
            }
        }

        class ReviewViewHolder extends MainViewHolder  {

            CustomTextView review, time , user_name;
            ImageView img_profile, star1, star2, star3, star4, star5;
            ReviewViewHolder(View itemView) {
                super(itemView);
                user_name = (CustomTextView)itemView.findViewById(R.id.user_name);
                time = (CustomTextView)itemView.findViewById(R.id.time);
                review = (CustomTextView)itemView.findViewById(R.id.review);
                img_profile = (ImageView)itemView.findViewById(R.id.profile_image);
                star5 = (ImageView)itemView.findViewById(R.id.star5);
                star4 = (ImageView)itemView.findViewById(R.id.star4);
                star3 = (ImageView)itemView.findViewById(R.id.star3);
                star2 = (ImageView)itemView.findViewById(R.id.star2);
                star1 = (ImageView)itemView.findViewById(R.id.star1);
            }
        }
        public class EmptyViewHolder extends MainViewHolder {

            CustomTextView textView;

            public EmptyViewHolder(View itemView) {
                super(itemView);
                textView = (CustomTextView)itemView.findViewById(R.id.text);
            }
        }
    }
}
