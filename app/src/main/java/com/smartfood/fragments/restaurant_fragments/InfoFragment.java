package com.smartfood.fragments.restaurant_fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartfood.R;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.json.JSONObject;

/**
 * Created by and-05 on 13/11/17.
 */

public class InfoFragment extends Fragment {

    Context context;
    ProgressHUD progresshud;
    ResponseTask responseTask;
    final String TAG = InfoFragment.class.getSimpleName();
    Bundle bundle;
    CustomTextView info;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container , false);
        context = getActivity();
        bundle = getArguments();

        info = (CustomTextView)view.findViewById(R.id.info);

        if (Utility.isConnectingToInternet(context))
            serverGetInfo();
        else
            Utility.ShowToastMessage(context , context.getString(R.string.not_connected_to_internet));

        return view;
    }

    private void serverGetInfo() {
        progresshud = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.RESTAURANT_ID, bundle.getString(Constants.RESTAURANT_ID));

            responseTask = new ResponseTask(context, jsonObject,Constants.GET_DESC_BY_ID, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                JSONObject object = json.getJSONObject(Constants.OBJECT);
                                info.setText(object.getString(Constants.DESCRIPTION));

                            } else {
                                Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
