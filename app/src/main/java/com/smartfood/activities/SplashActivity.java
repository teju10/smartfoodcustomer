package com.smartfood.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.smartfood.R;
import com.crashlytics.android.Crashlytics;
import com.smartfood.utility.Constants;
import com.smartfood.utility.Utility;
import com.smartfood.utility.Utils;

import io.fabric.sdk.android.Fabric;

/**
 * Created by and-05 on 7/11/17.
 */

public class SplashActivity extends AppCompatActivity {
    Context context;
/*keytool -list -v -keystore ~/.android/debug.keystore -alias androiddebugkey -storepass android -keypass android
*/
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this,Utility.getIngerSharedPreferences(getApplicationContext(),Constants.THEME));

        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        context = this;
        if (Utility.getIngerSharedPreferences(context, Constants.THEME) != 1) {
            Utility.setIntegerSharedPreference(context, Constants.THEME, 2);
        } else {
            Utility.setIntegerSharedPreference(context, Constants.THEME, 1);
        }
        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(SplashActivity.this, LanguageChangeActivity.class));
                Utility.activityTransition(context);
                finish();
            }
        }, secondsDelayed * 1000);

    }
}
