package com.smartfood.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.smartfood.R;
import com.smartfood.customwidget.CustomAutoCompleteTextView;
import com.smartfood.customwidget.CustomButton;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.utility.Constants;
import com.smartfood.utility.GetResponseTask;
import com.smartfood.utility.PlaceJSONParser;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.Utility;
import com.smartfood.utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by and-05 on 20/11/17.
 */

public class MapAddressActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    MapView mapView;
    Double lat, lon;
    Marker marker;
   // CustomTextView tv_address;
    public static boolean IsUpdate = false;
    ListView list;

    private GoogleMap mGoogleMap;
   // private FusedLocationProviderClient mFusedLocationClient;
    protected Location mLastLocation;
    Context context;
    final int Request_code = 101;
    JSONObject object = new JSONObject();
    CustomAutoCompleteTextView pickup_address;
    ParserTaskforautotv parserTask;
    ProgressHUD progressHUD;
    String str_address="";


    final String TAG = MapAddressActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this, Utility.getIngerSharedPreferences(getApplicationContext(), Constants.THEME));
        setContentView(R.layout.activity_pickupaddress);

        context = this;
        bind();
     //   mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    private void bind() {

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);setTitle("");
        ((CustomTextView)findViewById(R.id.toolbar_title)).setText(R.string.add_new_address);

      //  tv_address = (CustomTextView)findViewById(R.id.tv_address);
        ((CustomButton)findViewById(R.id.btn_confirm)).setOnClickListener(this);
       // tv_address.setOnClickListener(this);
        Utility.ChangeTheme(context, toolbar);


        mapView = (MapView)findViewById(R.id.mapview);
        if (mapView != null)
        {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
        Initialize();
        updateUi();

    }

    private void updateUi() {
        if (getIntent().getStringExtra(Constants.IS_UPDATE).equals("1")){
            try {
                object = new JSONObject(getIntent().getStringExtra(Constants.OBJECT));
                pickup_address.setText(object.getString(Constants.ADDRESS));
                lat = object.getDouble(Constants.LATITUDE);
                lon = object.getDouble(Constants.LONGITUDE);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize(context);
        mGoogleMap = googleMap;

        if (lat != null && lon !=null){
            marker = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));
            CameraPosition cameraPosition = CameraPosition.builder().target(new LatLng(lat, lon)).zoom(10).build();
            if (mGoogleMap != null)
                mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }

        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                GetLocationAsync(latLng.latitude, latLng.longitude);

                lat = latLng.latitude;
                lon = latLng.longitude;

                if (marker != null) {
                    marker.remove();
                }
                marker = mGoogleMap.addMarker(new MarkerOptions().position(latLng));
                //    mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15.0f));
            }
        });
    }

    private void GetLocationAsync(final double x, final double y) {
        String origin = "";
        origin = "latlng=" + x + "," + y;
        String sensor = "sensor=false";
        String output = "json";
        String parameters = origin + "&" + Constants.MAP_KEY + "&" + sensor;
        String url = "https://maps.googleapis.com/maps/api/geocode/" + output + "?" + parameters;
        Log.e("GetDirections=== ", url);
        GetResponseTask getResponseTask = new GetResponseTask(url, context, "HomeMapFragment");
        getResponseTask.execute();
        getResponseTask.setListener(new ResponseListener() {
            @Override
            public void onPickSuccess(String result) {
                if (result != null) {
                    try {
                        JSONObject jsonObj = new JSONObject(result);
                        String status = jsonObj.getString("status");
                        if (status.equalsIgnoreCase("OK")) {
                            JSONArray Results = jsonObj.getJSONArray("results");
                            JSONObject zero = Results.getJSONObject(0);
                            String address = zero.getString("formatted_address");
                            Log.e("onPickSuccess: ", address);

                            pickup_address.setText(address);
                        }else if (status.equalsIgnoreCase("ZERO_RESULTS")){
                            Utility.ShowToastMessage(context, getString(R.string.invalid_address));
                            pickup_address.setText("");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Utility.ShowToastMessage(context, "Invalid Address");
                }
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();

        if (IsUpdate){
            finish();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_confirm:
                if (pickup_address.getText().toString().isEmpty()){
                    Utility.ShowToastMessage(context , getString(R.string.error_select_address));
                }else if (lat ==null && lon == null ){
                    Utility.ShowToastMessage(context , getString(R.string.error_enter_correct_address));
                }else{
                    startActivity(new Intent(context, AddressMoreDetailsActivity.class)
                            .putExtra(Constants.ADDRESS, pickup_address.getText().toString())
                            .putExtra(Constants.LATITUDE, String.valueOf(lat))
                            .putExtra(Constants.LONGITUDE, String.valueOf(lon))
                            .putExtra(Constants.IS_UPDATE, getIntent().getStringExtra(Constants.IS_UPDATE))
                            .putExtra(Constants.OBJECT, object.toString()));
                    Utility.activityTransition(context);
                }

                break;
           /* case R.id.tv_address:
                startActivityForResult(new Intent(context , PickAddressActivity.class),Request_code);
                break;*/
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Request_code && resultCode == Activity.RESULT_OK){

            pickup_address.setText(data.getStringExtra(Constants.ADDRESS));
            lat = data.getDoubleExtra(Constants.LATITUDE,0);
            lon = data.getDoubleExtra(Constants.LONGITUDE,0);
            LatLng latLng= new LatLng(lat, lon);

            if (marker != null) {
                marker.remove();
            }
            marker = mGoogleMap.addMarker(new MarkerOptions().position(latLng));

            if (lat != null && lon !=null) {
                CameraPosition cameraPosition = CameraPosition.builder().target(latLng).zoom(10).build();
                if (mGoogleMap != null)
                    mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        }
    }

    public void Initialize() {
        /*FOR AUTOCOMPLETE METHOD*/
        pickup_address = (CustomAutoCompleteTextView) findViewById(R.id.pickup_address);
        list = (ListView) findViewById(R.id.list);

        pickup_address.setThreshold(1);
        pickup_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int i, int i1, int i2) {
                Log.e("onTextChanged", "CharSequence ===" + s.toString());

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                str_address = "";

            }

            @Override
            public void afterTextChanged(Editable s) {
                ((CardView) findViewById(R.id.listcardview)).setVisibility(View.VISIBLE);
                PlacesTask placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }
        });
    }

    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            String data = "";
            try {
                String key = "key="+ Constants.MAP_KEY;
                String input = "";
                try {
                    input = "input=" + URLEncoder.encode(place[0], "utf-8");
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
                // https://maps.googleapis.com/maps/api/place/autocomplete/xml?input=Amoeba&types=establishment&
                // location=37.76999,-122.44696&radius=500&key=YOUR_API_KEY
                // String location = "location=" + String.valueOf(latitude) + "," + String.valueOf(longitude);
                // String country = "components=country:jd";
                //String country = "components=country:in";
                String types = "types=geocode";
                String sensor = "sensor=true";
                String parameters = input + "&" + types + "&" /*+ country*/ + "&" + sensor + "&" + key;
                String output = "json";
                String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;
                try {
                    Log.e("downloadUrl", "url ==== " + url);
                    data = downloadUrl(url);
                } catch (Exception e) {
                    Log.d("Background Task", e.toString());
                }
                return data;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            parserTask = new ParserTaskforautotv();
            parserTask.execute(result);
        }
    }

    private class ParserTaskforautotv extends AsyncTask<String, Integer, List<HashMap<String, String>>> {
        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {
            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();
            try {
                jObject = new JSONObject(jsonData[0]);
                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);
            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            String[] from = new String[]{"description"};
            try {
                int[] to = new int[]{android.R.id.text1};
                // Creating a SimpleAdapter for the AutoCompleteTextView

                SimpleAdapter adapter = new SimpleAdapter(context, result, android.R.layout.simple_list_item_1, from, to);
                // Setting the adapter
                adapter.notifyDataSetChanged();
                list.setAdapter(adapter);
                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        str_address = ((HashMap<String, String>) parent.getItemAtPosition(position)).get("description");

                       /* Intent intent = new Intent();
                        intent.putExtra("ADDRESS", address);
                        setResult(Activity.RESULT_OK,intent);
                        finish();
*/

                        new GetLatLongFromAddressTask().execute(str_address);

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class GetLatLongFromAddressTask extends AsyncTask<String, String, Address> {

        String errorMessage = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressHUD = ProgressHUD.show(context,getString(R.string.loading), true, false, null);

        }


        @Override
        protected Address doInBackground(String... params) {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = null;

            try {
                addresses = geocoder.getFromLocationName(params[0], 1);
                Log.e(TAG, "doInBackground: "+addresses );
            } catch (IOException e) {
                errorMessage = "Service not available";
                Log.e(TAG, errorMessage, e);
            }
            if (addresses != null && addresses.size() > 0) {
                return addresses.get(0);
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Address address) {
            if (progressHUD != null && progressHUD.isShowing()) {
                progressHUD.dismiss();
            }
            if (address == null) {

            } else {
                try {
                    String addressName = "";
                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                        addressName += " --- " + address.getAddressLine(i);
                    }
                    Log.e(TAG, "Latitude: " + address.getLatitude() + "\n" + "Longitude: " + address.getLongitude() + "\n" +
                            "Address: " + addressName);
                    Log.e(TAG, "newlat === " + address.getLatitude());
                    Log.e(TAG, "newlong === " + address.getLongitude());
                    lat = address.getLatitude();
                    lon = address.getLongitude();

                    pickup_address.setText(str_address);
                    ((CardView) findViewById(R.id.listcardview)).setVisibility(View.GONE);

                    if (marker != null){
                        marker.remove();
                    }

                    if (lat != null && lon !=null){
                        marker = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));
                        CameraPosition cameraPosition = CameraPosition.builder().target(new LatLng(lat, lon)).zoom(10).build();
                        if (mGoogleMap != null)
                            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }



                } catch (Exception e) {
                    Log.e(TAG, "onPostExecute Exception ====== " + e.toString());
                    e.printStackTrace();
                }
            }
            super.onPostExecute(address);
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

}
