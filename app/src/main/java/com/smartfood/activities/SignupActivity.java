package com.smartfood.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.smartfood.R;
import com.smartfood.customwidget.CustomEditText;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.RuntimePermissionsActivity;
import com.smartfood.utility.Utility;

import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by and-05 on 7/11/17.
 */

public class SignupActivity extends RuntimePermissionsActivity implements View.OnClickListener
        ,FacebookCallback<LoginResult> {

    CustomEditText first_name,last_name,phone_no,password,confirm_password;
    Context context;
    ResponseTask responseTask;
    CallbackManager callbackManager;
    final String TAG = SignupActivity.class.getSimpleName();
    ProgressHUD progresshud;
    String str_id, str_fname, str_lname, str_image;

    GoogleSignInClient mGoogleSignInClient;
    final int RC_SIGN_IN = 101;
    final int PERMISSION_REQUEST_CODE = 100;
    String str_gender="1";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        Utility.ChangeLang(context , Utility.getLanSharedPreferences(context , Constants.LANGUAGE, 0));
        setContentView(R.layout.activity_signup_);
        bindViews();
    }

    private void bindViews() {

        //--------------- Initialize facebook--------------------------------------------
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, this);
        //--------------- Initialize Google--------------------------------------------
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail().build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        first_name = (CustomEditText)findViewById(R.id.first_name);
        last_name = (CustomEditText)findViewById(R.id.last_name);
        phone_no = (CustomEditText)findViewById(R.id.phone_no);
        password = (CustomEditText)findViewById(R.id.password);
        confirm_password = (CustomEditText)findViewById(R.id.confirm_password);
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.rg_gender);

        ((CustomTextView)findViewById(R.id.btn_switch_login))
                .setPaintFlags(((CustomTextView)findViewById(R.id.btn_switch_login)).getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        (findViewById(R.id.btn_signup)).setOnClickListener(this);
        (findViewById(R.id.btn_facebook)).setOnClickListener(this);
        (findViewById(R.id.btn_google)).setOnClickListener(this);
        (findViewById(R.id.btn_switch_login)).setOnClickListener(this);
        ((RadioButton)findViewById(R.id.male)).setChecked(true);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.male){
                    str_gender = "1";
                }else{
                    str_gender = "0";
                }
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_signup:
                if (first_name.getText().toString().isEmpty()) {
                    Utility.ShowToastMessage(context , getString(R.string.error_enter_first_name));
                }else if (!Utility.isValidText(first_name.getText().toString())){
                    Utility.ShowToastMessage(context , getString(R.string.error_valid_first_name));
                }
                else if (last_name.getText().toString().isEmpty()){
                    Utility.ShowToastMessage(context , getString(R.string.error_enter_last_name));
                }else if (!Utility.isValidText(last_name.getText().toString())){
                    Utility.ShowToastMessage(context , getString(R.string.error_valid_last_name));
                } else if (phone_no.getText().toString().isEmpty()){
                    Utility.ShowToastMessage(context , getString(R.string.error_enter_phone_number));
                } else if (password.getText().toString().isEmpty()){
                    Utility.ShowToastMessage(context , getString(R.string.error_enter_password));
                } else if (password.getText().toString().length() <6){
                    Utility.ShowToastMessage(context , getString(R.string.error_password_six_character));
                }
                else if (confirm_password.getText().toString().isEmpty()){
                    Utility.ShowToastMessage(context , getString(R.string.error_enter_confirm_password));
                } else if (!password.getText().toString().equals(confirm_password.getText().toString())){
                    Utility.ShowToastMessage(context , getString(R.string.error_passwords_doesnt_match));
                }else{
                    serverCall();
                }


                break;
            case R.id.btn_facebook:
                if (Utility.isConnectingToInternet(context)) {
                    LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
                } else
                    Utility.ShowToastMessage(context, getString(R.string.not_connected_to_internet));
                break;
            case R.id.btn_google:
                if (hasPermissions(context, Manifest.permission.GET_ACCOUNTS)) {
                    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                }else{
                    requestAppPermissions(new String[]{Manifest.permission.GET_ACCOUNTS},
                            R.string.account_permission_needed, PERMISSION_REQUEST_CODE);
                }
                break;
            case R.id.btn_switch_login:
                startActivity(new Intent(context , LoginActivity.class));
                Utility.activityTransition(context);
                break;

        }
    }

    private void serverCall() {

        progresshud = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.F_NAME, first_name.getText().toString().trim());
            jsonObject.put(Constants.L_NAME, last_name.getText().toString().trim());
            jsonObject.put(Constants.CONTACT, phone_no.getText().toString());
            jsonObject.put(Constants.PASSWORD, password.getText().toString());
            jsonObject.put(Constants.DEVICE_ID, Utility.getSharedPreferences(context , Constants.DEVICE_ID));
            jsonObject.put(Constants.DEVICE_TYPE, "android");
            jsonObject.put(Constants.GENDER, str_gender);

            responseTask = new ResponseTask(context, jsonObject,Constants.SIGNUP, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                JSONObject object = json.getJSONObject(Constants.OBJECT);
                                startActivity(new Intent(context , VerificationActivity.class)
                                        .putExtra(Constants.USER_ID , object.getString(Constants.USER_ID))
                                        .putExtra(Constants.CONTACT, object.getString(Constants.CONTACT))
                                        .putExtra(Constants.FULLNAME, object.getString(Constants.F_NAME)
                                                +" "+object.getString(Constants.L_NAME)));
                                finish();
                                Utility.activityTransition(context);

                            } else {
                                Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //------------------------facebook login---------------------------------------------------
    private void GraphRequestMethod(AccessToken accessToken) {

        progresshud = ProgressHUD.show(context,getString(R.string.loading), true, false, null);
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    Log.e(TAG, "onCompleted: "+object.toString() );
                    str_fname = object.getString("first_name");
                    str_lname = object.getString("last_name");
                  //  str_image = "http://graph.facebook.com/" + object.getString("id") + "/picture?type=large";
                    str_id = object.getString("id");

                    Log.e(TAG, "onCompleted: "+str_fname+","+str_id+","+str_image);


                    serverSocialCall("facebook");

                    LoginManager.getInstance().logOut();

                } catch (Exception e) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name,last_name, gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        GraphRequestMethod(loginResult.getAccessToken());
    }

    @Override
    public void onCancel() {
        Utility.ShowToastMessage(context, getString(R.string.facebook_login_cancel));
        if (progresshud != null && progresshud.isShowing()) {
            progresshud.dismiss();
        }
    }

    @Override
    public void onError(FacebookException error) {
        Utility.ShowToastMessage(context, getString(R.string.error_in_facebook_login));
        if (progresshud != null && progresshud.isShowing()) {
            progresshud.dismiss();
        }
    }

    //-----------------google login ---------------------------------------------------------------
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount acct = completedTask.getResult(ApiException.class);

            if (acct != null) {
                String full_name = acct.getDisplayName();

                assert full_name != null;
                if (!full_name.equals("")){
                    if (full_name.contains(" ")) {
                        str_fname = full_name.split(" ")[0];
                        str_lname = full_name.split(" ")[1];
                    }else{
                        str_fname = full_name;
                    }
                }
                str_id = acct.getId();
                Uri personPhoto = null;
                if (acct.getPhotoUrl() != null) {
                    personPhoto = acct.getPhotoUrl();
                    str_image = personPhoto.toString();
                }
                Log.e(TAG, "handleSignInResult: "+str_fname+","+personPhoto );
                serverSocialCall("google");
            }


            // Signed in successfully, show authenticated UI.
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.e(TAG, "signInResult:failed code="+e.getStatusCode() + e.getMessage()+","+e.toString());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }

    }

    @Override
    public void onPermissionsGranted(int requestCode) {

        if (requestCode == PERMISSION_REQUEST_CODE){
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    }

    private void serverSocialCall(String auth_provider) {

        progresshud = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.F_NAME, str_fname);
            jsonObject.put(Constants.L_NAME, str_lname);
            jsonObject.put(Constants.OUTHID, str_id );
            jsonObject.put(Constants.DEVICE_ID, Utility.getSharedPreferences(context , Constants.DEVICE_ID));
            jsonObject.put(Constants.DEVICE_TYPE, "android");
            jsonObject.put(Constants.OUTH_PROVIDER, auth_provider);
            jsonObject.put(Constants.PROFILE_IMAGE, str_image);

            responseTask = new ResponseTask(context, jsonObject,Constants.SOCIAL_LOGIN, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                JSONObject object = json.getJSONObject(Constants.OBJECT);
                                Utility.setSharedPreference(context , Constants.USER_ID,object.getString(Constants.USER_ID));
                                Utility.setSharedPreference(context , Constants.FULLNAME,object.getString(Constants.F_NAME)
                                        +" "+object.getString(Constants.L_NAME));

                                Utility.setSharedPreference(context,Constants.CART_COUNT,object.getInt(Constants.CART_COUNT));

                                startActivity(new Intent(context , HomeActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                Utility.activityTransition(context);

                            } else {
                                Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
