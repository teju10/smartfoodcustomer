package com.smartfood.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.smartfood.Cursor_WheelLayout.CursorWheelLayout;
import com.smartfood.Cursor_WheelLayout.MenuItemData;
import com.smartfood.Cursor_WheelLayout.SimpleTextAdapter;
import com.smartfood.Cursor_WheelLayout.SimpleTextCursorWheelLayout;
import com.smartfood.R;
import com.smartfood.adapter.SimpleTextAdapter1;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.materialTrasitionScreen.Sample;
import com.smartfood.model.MenuItem;
import com.smartfood.spinner.SpinningWheelView;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by and-04 on 8/2/18.
 */

public class LuckyWheel_SpinnerActivity extends AppCompatActivity implements /*CursorWheelLayout.OnMenuSelectedListener,
        CursorWheelLayout.OnStartListner*/SpinningWheelView.OnRotationListener<String>

{

    Context mContext;
    // SimpleTextCursorWheelLayout test_circle_menu_left;
    ImageView imageView;
    int val = 4;
    boolean TRANS = false;
    long mCurrentTime;
    ResponseTask rt;
    ProgressHUD progresshud;
    private String TAG = "FeelingLucky_SpinnerActivity";
    ArrayList<JSONObject> mlist_rest = new ArrayList<>();
    ArrayList<JSONObject> mlist_meal = new ArrayList<>();
    RadioGroup rg_select_rest_meal;
    RadioButton rb_rest, rb_meal;
    int SelectType = 1;
    boolean MEALCLICK, APICALL = true, INDEX = false;
    Handler handler;
    String Spin_count;
    private SpinningWheelView wheelView;
    int Time_counter = 0;
    CountDownTimer countDownTimer;
    CustomTextView round_left_txt, spin_limit_msg;
    RelativeLayout spinner_layout;
    boolean isCounterRunning = false;
    ArrayList<String> resturant_mlist = new ArrayList<>();
    ArrayList<String> meal_mlist = new ArrayList<>();
    MediaPlayer mp;

    int[] rainbow;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_feel_lucky_spnr);
        mContext = this;

        //  rainbow = getResources().getIntArray(R.array.rainbow_dash);

        find();

        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //GetData();
        if (Utility.getSharedPreferences(mContext, Constants.SPINCOUNT).equals("0")) {

            SpinnerGone();

            spin_limit_msg.setVisibility(View.VISIBLE);
            findViewById(R.id.lay_counter).setVisibility(View.VISIBLE);

            SetTimer();

        } else {
            isCounterRunning = true;

            round_left_txt.setText(Utility.getSharedPreferences(mContext, Constants.SPINCOUNT));
            SetSpinnerVisible();

            CounterLayGone();
        }
    }

    public void find() {

        //test_circle_menu_left = findViewById(R.id.test_circle_menu_left);
        rg_select_rest_meal = findViewById(R.id.rg_select_rest_meal);
        round_left_txt = findViewById(R.id.round_left_txt);


        wheelView = findViewById(R.id.wheelView);
        spinner_layout = findViewById(R.id.spinner_layout);
        imageView = findViewById(R.id.imageView);

        rb_rest = findViewById(R.id.rb_rest);
        rb_meal = findViewById(R.id.rb_meal);
        spin_limit_msg = findViewById(R.id.spin_limit_msg);
/*
        Toolbar toolbar = (Toolbar) findViewById(R.id.ly_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");

        ((CustomTextView) findViewById(R.id.toolbar_title)).
                setText(mContext.getResources().getString(R.string.lucky_wheel));*/

        rb_rest.setChecked(true);

        findViewById(R.id.cross_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void initData() {
        // GetData();
        if (Utility.isConnectingToInternet(mContext)) {
            GetFoodMealTask();
        } else {
            Utility.ShowToastMessage(mContext, mContext.getResources().getString(R.string.not_connected_to_internet));
        }

        handler = new Handler();

        rg_select_rest_meal.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int i) {
                if (i == R.id.rb_rest) {
                    SelectType = 1;
                   /* SimpleTextAdapter simpleTextAdapter = new SimpleTextAdapter(mContext, mlist_rest, Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                    test_circle_menu_left.setAdapter(simpleTextAdapter);*/
                    wheelView.setItems(resturant_mlist);
                } else {
                    SelectType = 2;
                    if (APICALL) {
                        GetFoodMealTask();
                    }
                    if (MEALCLICK) {
                        wheelView.setItems(meal_mlist);
                       /* SimpleTextAdapter simpleTextAdapter = new SimpleTextAdapter(mContext, mlist_meal, Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                        test_circle_menu_left.setAdapter(simpleTextAdapter);*/
                    }
                }
            }
        });
        wheelView.setOnRotationListener(this);
      /*  test_circle_menu_left.setOnMenuSelectedListener(this);
        test_circle_menu_left.OnStartSpinnerListnerClick(this);*/

    }

    private void SetClickable() {
        rb_rest.setClickable(true);
        rb_meal.setClickable(true);
    }

    private void SendData(int item) {

        if (SelectType == 1) {
            for (int i = 0; i < mlist_rest.size(); i++) {
                try {
                    if ((mlist_rest.get(i).getString(Constants.INDEX)).equals(String.valueOf(item))) {
                        startActivity(new Intent(mContext, ResultScreen_ofSpin_Activity.class)
                                        .putExtra(Constants.OBJECT, mlist_rest.get(i).toString()));
                        Utility.activityTransition(mContext);
                        Utility.setSharedPreference(mContext, Constants.SPIN_TYPE_CLICK, "1");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            for (int i = 0; i < mlist_meal.size(); i++) {
                try {
                    if (mlist_meal.get(i).getString(Constants.INDEX).equals(String.valueOf(item))) {
                        startActivity(new Intent(mContext, ResultScreen_ofSpin_Activity.class)
                                        .putExtra(Constants.OBJECT, mlist_meal.get(i).toString()));
                        Utility.activityTransition(mContext);
                        Utility.setSharedPreference(mContext, Constants.SPIN_TYPE_CLICK, "2");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//    @Override
//    public void onItemSelected(CursorWheelLayout p, View view, int pos) {
//
//        if (INDEX) {
//           // SendData(pos);
//
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    SetClickable();
//                }
//            }, 500);
//
//        }
//        // transitionToActivity(FeelingLuckyActivity.class,);
//    }

    public void GetFoodMealTask() {
        progresshud = ProgressHUD.show(mContext, getString(R.string.loading), true, false, null);

        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.USER_ID, Utility.getSharedPreferences(mContext, Constants.USER_ID));
            rt = new ResponseTask(mContext, jo, Constants.GET_LUCKYMEAL_LIST, TAG, "post");
            rt.execute();
            rt.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null || result.equals("")) {
                        Utility.ShowToastMessage(mContext, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                JSONObject jobj = json.getJSONObject("object");

                                JSONArray jara_meal = jobj.getJSONArray("lucky_meal");

                                JSONArray jara_rest = jobj.getJSONArray("lucky_restaurant");

                                if ((SelectType == 1) && !(jobj.getString("spin_count").equals("0"))) {
                                    for (int i = 0; i < jara_rest.length(); i++) {
                                        mlist_rest.add(jara_rest.getJSONObject(i));
                                        resturant_mlist.add(jara_rest.getJSONObject(i).getString("index"));
                                    }
                                    if (!jobj.getString("spin_count").equals("0")) {
                                        SetSpinnerVisible();
                                        CounterLayGone();
                                    }

                                    wheelView.setItems(resturant_mlist);
                                    /*SimpleTextAdapter simpleTextAdapter = new SimpleTextAdapter(mContext, mlist_rest, Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                                    test_circle_menu_left.setAdapter(simpleTextAdapter);
*/
                                } else if (SelectType == 2) {
                                    MEALCLICK = true;
                                    APICALL = false;

                                    if (!jobj.getString("spin_count").equals("0")) {
                                        SetSpinnerVisible();
                                        CounterLayGone();
                                    }
                                    for (int i = 0; i < jara_meal.length(); i++) {
                                        mlist_meal.add(jara_meal.getJSONObject(i));
                                        meal_mlist.add(jara_meal.getJSONObject(i).getString("index"));

                                    }
                                    wheelView.setItems(meal_mlist);
//                                    SimpleTextAdapter simpleTextAdapter = new SimpleTextAdapter(mContext, mlist_meal, Gravity.TOP | Gravity.CENTER_HORIZONTAL);
//                                    test_circle_menu_left.setAdapter(simpleTextAdapter);
                                }
                                round_left_txt.setText(jobj.getString("spin_count"));
                                Spin_count = jobj.getString("spin_count");

                                if (Spin_count.equals("0")) {
                                    wheelView.setVisibility(View.GONE);
                                    imageView.setVisibility(View.GONE);
                                }

                            } else if (json.getString(Constants.SUCCESS).equals("0")) {

                            }
                        } catch (Exception je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

   /* @Override
    public void OnSpinnerStart() {
        INDEX = true;

        rb_rest.setClickable(false);
        rb_meal.setClickable(false);
    }*/

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Utility.activityTransition(mContext);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void SetTimer() {
        DateFormat df = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
//        df.setTimeZone(TimeZone.getTimeZone("Jordon"));
        String currentDate = df.format(Calendar.getInstance().getTime());
        Log.e(TAG, "bind: " + currentDate);
        Date d1 = null, d2 = null;

        try {
            d1 = df.parse(currentDate);
            d2 = df.parse("23:59");

            long diff = d2.getTime() - d1.getTime();

            String time = Utility.parseElapsedTime(diff);
            Log.e(TAG, "initData: " + Utility.parseElapsedTime(diff));

            countDownTimer = new CountDownTimer(diff, 1000) {
                @Override
                public void onTick(long l) {
                    countDownTimer.cancel();
                    long millis = l;
                    String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                            TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                            TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                    System.out.println("RAB NE BNAA DI====== " + hms);
                    ((CustomTextView) findViewById(R.id.left_countdown_txt)).setText(hms);
                }

                @Override
                public void onFinish() {
                    countDownTimer.cancel();
                    isCounterRunning = false;
                    findViewById(R.id.lay_counter).setVisibility(View.GONE);

                    findViewById(R.id.draw_left_layout).setVisibility(View.VISIBLE);
                    if (Utility.isConnectingToInternet(mContext)) {
                        GetFoodMealTask();
                    } else {
                        Utility.ShowToastMessage(mContext, mContext.getResources().getString(R.string.not_connected_to_internet));
                    }

                }
            }.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void GetData() {
        String[] res = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "OFF"};
        List<MenuItemData> menuItemDatas = new ArrayList<MenuItemData>();
        for (int i = 0; i < res.length; i++) {
            menuItemDatas.add(new MenuItemData(res[i]));
        }
        SimpleTextAdapter1 simpleTextAdapter = new SimpleTextAdapter1(this, menuItemDatas, Gravity.TOP | Gravity.CENTER_HORIZONTAL);
        // test_circle_menu_left.setAdapter(simpleTextAdapter);
    }

    private void SetSpinnerVisible() {
        spinner_layout.setVisibility(View.VISIBLE);
        findViewById(R.id.draw_left_layout).setVisibility(View.VISIBLE);

    }

    private void CounterLayGone() {
        findViewById(R.id.lay_counter).setVisibility(View.GONE);
        spin_limit_msg.setVisibility(View.GONE);

    }


    private void SpinnerGone() {
        findViewById(R.id.draw_left_layout).setVisibility(View.GONE);
        spinner_layout.setVisibility(View.GONE);

    }

    @Override
    public void onRotation() {
        INDEX = true;

        rb_rest.setClickable(false);
        rb_meal.setClickable(false);
        wheelView.rotate(20, 5000, 30);

        mp = MediaPlayer.create(this, R.raw.wheel_sound);
        mp.setLooping(true);

        if (!mp.isPlaying()) {
            mp.start();
        }
    }

    @Override
    public void onStopRotation(final String item) {
        System.out.println("SELECTED_INDEX=== " + item);
        if (mp.isPlaying()) {
            mp.stop();
        }

        mp = MediaPlayer.create(this, R.raw.stop_sound);
        mp.start();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (INDEX) {
                    mp.stop();
                    rainbow = new int[]{R.color.rainbowDashRed, R.color.rainbowDashPeach,
                            R.color.rainbowDashOrange, R.color.rainbowDashGreen,
                            R.color.rainbowDashBlue};

                    SendData(Integer.parseInt(item));
                    SetClickable();

                }
            }
        }, 200);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void transitionToActivity(Class target, Sample sample) {
        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(this, true);
        startActivity(target, pairs, sample);
    }

    private void startActivity(Class target, Pair<View, String>[] pairs, Sample sample) {
        Intent i = new Intent(mContext, target);
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);
        i.putExtra("sample", sample);
        mContext.startActivity(i, transitionActivityOptions.toBundle());
    }
}
