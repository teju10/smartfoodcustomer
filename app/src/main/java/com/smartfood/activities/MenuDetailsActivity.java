package com.smartfood.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.smartfood.R;
import com.smartfood.customwidget.CustomButton;
import com.smartfood.customwidget.CustomEditText;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.customwidget.other.Util;
import com.smartfood.model.PriceCal;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;
import com.smartfood.utility.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 22/11/17.
 */

public class MenuDetailsActivity extends BaseActivity implements View.OnClickListener{

    Context context;
    ResponseTask responseTask;
    ProgressHUD progressHUD;
    final String TAG = MenuDetailsActivity.class.getSimpleName();
    Bundle bundle;
    LinearLayout mContainer;
    final int REQUEST_CODE = 1011;
    int quantity=1, already_added = 0;
    CustomTextView txt_quantity;
    ArrayList<PriceCal> listPrice = new ArrayList<>();
    String str_special_req="";
    CustomButton btn_add_cart;
    ImageView img_tick;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this, Utility.getIngerSharedPreferences(getApplicationContext(), Constants.THEME));
        setContentView(R.layout.activity_menu_details);
        context = this;
        bind();

    }

    private void bind() {

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);setTitle("");
        ((CustomTextView)findViewById(R.id.toolbar_title)).setText(R.string.menu_details);

        Utility.ChangeTheme(context, toolbar);
        txt_quantity = (CustomTextView)findViewById(R.id.quantity);
        btn_add_cart = (CustomButton)findViewById(R.id.add_to_cart);
        img_tick = (ImageView) findViewById(R.id.img_tick);

        btn_add_cart.setOnClickListener(this);
        ((ImageView)findViewById(R.id.minus)).setOnClickListener(this);
        ((ImageView)findViewById(R.id.plus)).setOnClickListener(this);
        ((CustomTextView)findViewById(R.id.btn_special_req)).setOnClickListener(this);
        mContainer = (LinearLayout)findViewById(R.id.container);

        updateUi();

        ((ImageView)findViewById(R.id.btn_fav)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serverFavMeal();
            }
        });

        serverGetDetails();

    }

    private void updateUi() {

        bundle = getIntent().getExtras();
        String menu_price = bundle.getString(Constants.DISCOUNT_PRICE)+" "+getString(R.string.currency);
        ((CustomTextView)findViewById(R.id.menu_price)).setText(menu_price);
        ((CustomTextView)findViewById(R.id.menu_name)).setText(bundle.getString(Constants.MENU_NAME));
        ((CustomTextView)findViewById(R.id.menu_title)).setText(bundle.getString(Constants.MENU_TITLE));
        Glide.with(context).load(bundle.getString(Constants.MENU_IMAGE)).into((ImageView)findViewById(R.id.img_menu));
        listPrice.add(new PriceCal("0",bundle.getString(Constants.DISCOUNT_PRICE)));

        if (bundle.getString(Constants.IS_UPDATE).equals("1")){
            listPrice.addAll((ArrayList<PriceCal>)getIntent().getSerializableExtra(Constants.MENU_LIST));
            quantity = Integer.parseInt(bundle.getString(Constants.QUANTITY));
            txt_quantity.setText(bundle.getString(Constants.QUANTITY));
            str_special_req = bundle.getString(Constants.SPECIAL_REQ);

            btn_add_cart.setText(R.string.update);


            double total = 0;
            for (int i = 0; i < listPrice.size() ; i++) {
                total= total + Double.parseDouble(listPrice.get(i).getPrice());
            }
            Log.e(TAG, "onActivityResult: " +total);
            String price = String.valueOf(total)+" "+getString(R.string.currency);
            ((CustomTextView)findViewById(R.id.menu_price)).setText(price);

        }
    }

    private void serverFavMeal() {

        if (Utility.isConnectingToInternet(context)) {

            progressHUD = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put(Constants.ITEM_ID, bundle.getString(Constants.MENU_ID));
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context , Constants.USER_ID));

                responseTask = new ResponseTask(context, jsonObject, Constants.FAVOURITE_ITEM, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progressHUD != null && progressHUD.isShowing()) {
                            progressHUD.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    JSONObject object = json.getJSONObject(Constants.OBJECT);
                                    if (object.getString(Constants.FAVOURITE).equals("1"))
                                        ((ImageView)findViewById(R.id.btn_fav)).setBackgroundResource(R.drawable.ic_fav_filled);
                                    else
                                        ((ImageView)findViewById(R.id.btn_fav)).setBackgroundResource(R.drawable.ic_fav_b);

                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Utility.ShowToastMessage(context , getString(R.string.not_connected_to_internet));
        }
    }

    private void serverGetDetails() {

        if (Utility.isConnectingToInternet(context)) {

            progressHUD = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put(Constants.ITEM_ID, bundle.getString(Constants.MENU_ID));
                jsonObject.put(Constants.RESTAURANT_ID, bundle.getString(Constants.RESTAURANT_ID));
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context , Constants.USER_ID));

                responseTask = new ResponseTask(context, jsonObject, Constants.GET_ITEM_BY_ID, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progressHUD != null && progressHUD.isShowing()) {
                            progressHUD.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    JSONObject object = json.getJSONObject(Constants.OBJECT);
                                    JSONArray jsonArray = object.getJSONArray(Constants.SUBITEMS);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject obj = jsonArray.getJSONObject(i);
                                        inflateRow(obj.getString(Constants.SUBITEMS_NAME), obj.getJSONArray(Constants.EXTRA));
                                    }

                                    if (object.getJSONObject(Constants.FOOD_DETAILS).getString(Constants.IS_FAV).equals("1"))
                                        ((ImageView)findViewById(R.id.btn_fav)).setBackgroundResource(R.drawable.ic_fav_filled);
                                    else
                                        ((ImageView)findViewById(R.id.btn_fav)).setBackgroundResource(R.drawable.ic_fav_b);

                                    if (object.getJSONObject(Constants.FOOD_DETAILS).getString(Constants.ADDED_CART).equals("1")
                                            && bundle.getString(Constants.IS_UPDATE).equals("")){
                                        btn_add_cart.setText(R.string.added_to_cart);
                                        already_added = 1;

                                    }


                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Utility.ShowToastMessage(context , getString(R.string.not_connected_to_internet));
        }
    }

    private void inflateRow(final String subitem_name, final JSONArray jsonArray)
    {

        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.item_menu_details, null);

        CustomTextView textView = (CustomTextView)rowView.findViewById(R.id.menu_text);
        textView.setText(subitem_name);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(context , AddExtrasActivity.class)
                        .putExtra(Constants.SUBITEMS_NAME, subitem_name )
                        .putExtra(Constants.OBJECT, jsonArray.toString())
                        .putExtra(Constants.MENU_LIST, (ArrayList<PriceCal>) listPrice), REQUEST_CODE);

                Utility.activityTransition(context);

            }
        });

        // Inflate at the end of all rows but before the "Add new" button
        mContainer.addView(rowView, mContainer.getChildCount() - 1);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add_to_cart:
                if (already_added == 0)
                    serverAddToCart();
              //  startActivity(new Intent(context , CartListActivity.class));
            break;
            case R.id.minus:
                if (quantity !=1)
                {
                    quantity = quantity -1;
                    txt_quantity.setText(String.valueOf(quantity));
                }
                break;
            case R.id.plus:
                quantity = quantity +1;
                txt_quantity.setText(String.valueOf(quantity));
                break;
            case R.id.btn_special_req:
                openSpecialDialog();
                break;
        }
    }

    private void openSpecialDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_special_req);
        dialog.getWindow().setLayout(DrawerLayout.LayoutParams.MATCH_PARENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        final CustomEditText edit_special_req = (CustomEditText)dialog.findViewById(R.id.special_req);

        if (!str_special_req.equals("")) {
            edit_special_req.setText(str_special_req);
            edit_special_req.setSelection(str_special_req.length());
        }


        ((ImageView)dialog.findViewById(R.id.btn_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ((CustomButton)dialog.findViewById(R.id.btn_done)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edit_special_req.getText().toString().isEmpty()){
                    Utility.ShowToastMessage(context , getString(R.string.error_special_request));
                }else {
                    str_special_req = edit_special_req.getText().toString();
                    img_tick.setBackgroundResource(R.drawable.ic_inprogress);
                    dialog.dismiss();
                }
            }
        });
    }

    private void serverAddToCart() {

        if (Utility.isConnectingToInternet(context)) {

            StringBuilder str_id = new StringBuilder();
            for (int i = 1; i < listPrice.size() ; i++) {
                if (i == listPrice.size()-1){
                    str_id = str_id.append(listPrice.get(i).getId());
                }else{
                    str_id = str_id.append(listPrice.get(i).getId()).append(",");
                }
            }

            progressHUD = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.PRODUCT_ID, bundle.getString(Constants.MENU_ID));
                jsonObject.put(Constants.SUBITEM_EXTRA_ID, str_id);
                jsonObject.put(Constants.QUANTITY, txt_quantity.getText().toString());
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
                jsonObject.put(Constants.SPECIAL_REQ, str_special_req);
                jsonObject.put(Constants.RESTAURANT_ID, bundle.getString(Constants.RESTAURANT_ID));
                jsonObject.put(Constants.IS_UPDATE, bundle.getString(Constants.IS_UPDATE));

                responseTask = new ResponseTask(context, jsonObject, Constants.ADD_TO_CART, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progressHUD != null && progressHUD.isShowing()) {
                            progressHUD.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    JSONObject object = json.getJSONObject(Constants.OBJECT);
                                    if (bundle.getString(Constants.IS_UPDATE).equals("1")){
                                        Utility.ShowToastMessage(context , getString(R.string.meal_updated_successfully));
                                    }else{
                                        Utility.ShowToastMessage(context , getString(R.string.meal_has_been_added));
                                        btn_add_cart.setText(R.string.added_to_cart);
                                        already_added = 1;
                                    }
                                    Utility.setSharedPreference(context,Constants.CART_COUNT,object.getInt(Constants.CART_COUNT));
                                    invalidateOptionsMenu();
                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Utility.ShowToastMessage(context , getString(R.string.not_connected_to_internet));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK){
            listPrice =  (ArrayList<PriceCal>)data.getSerializableExtra(Constants.MENU_LIST);

            double total = 0;
            for (int i = 0; i < listPrice.size() ; i++) {
                total= total + Double.parseDouble(listPrice.get(i).getPrice());
            }
            Log.e(TAG, "onActivityResult: " +total);
            String menu_price = String.valueOf(total)+" "+getString(R.string.currency);
            ((CustomTextView)findViewById(R.id.menu_price)).setText(menu_price);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cart_menu , menu);

        MenuItem item = menu.findItem(R.id.cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();
        Util.setBadgeCount(this, icon,Utility.getSharedPreferences(context, Constants.CART_COUNT,0));


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.cart){
            startActivity(new Intent(context , CartListActivity.class));
            Utility.activityTransition(context);
            finish();
        }else {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }
}
