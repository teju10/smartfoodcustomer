package com.smartfood.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.smartfood.R;
import com.smartfood.utility.Constants;
import com.smartfood.utility.Utility;

/**
 * Created by and-05 on 21/11/17.
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context context = this;
        Utility.ChangeLang(context , Utility.getLanSharedPreferences(context , Constants.LANGUAGE, 0));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }
}
