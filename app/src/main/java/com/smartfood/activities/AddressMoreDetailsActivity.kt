package com.smartfood.activities

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.view.MenuItem
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import com.smartfood.R
import com.smartfood.customwidget.CustomEditText
import com.smartfood.customwidget.CustomTextView
import com.smartfood.customwidget.ProgressHUD
import com.smartfood.utility.Constants
import com.smartfood.utility.ResponseTask
import com.smartfood.utility.Utility
import com.smartfood.utility.Utils
import org.json.JSONObject

/**
 * Created by and-05 on 9/12/17.
 */
class AddressMoreDetailsActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var et_street: CustomEditText
    private lateinit var et_building: CustomEditText
    private lateinit var et_floor: CustomEditText
    private lateinit var et_app_no: CustomEditText
    private lateinit var other_note: CustomEditText
    lateinit var responseTask: ResponseTask
    lateinit var progressHud: ProgressHUD
    internal lateinit var context: Context
    internal var TAG = AddressMoreDetailsActivity::class.java.simpleName
    internal var str_mark = ""
    internal var str_address_id = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Utils.onActivityCreateSetTheme(this, Utility.getIngerSharedPreferences(applicationContext, Constants.THEME))

        setContentView(R.layout.activity_address_more)
        context = this
        bindView()
    }

    private fun bindView() {
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setTitle("")
        Utility.ChangeTheme(context, toolbar, findViewById(R.id.lay_bg))


        (findViewById<View>(R.id.toolbar_title) as CustomTextView).text = getString(R.string.more_details)

        (findViewById<View>(R.id.near_by_address) as CustomTextView).text = intent.getStringExtra(Constants.ADDRESS)

        et_street = findViewById<View>(R.id.street) as CustomEditText
        et_building = findViewById<View>(R.id.building) as CustomEditText
        et_floor = findViewById<View>(R.id.floor) as CustomEditText
        other_note = findViewById<View>(R.id.other_note) as CustomEditText
        et_app_no = findViewById<View>(R.id.app_no) as CustomEditText
        (findViewById<View>(R.id.btn_update)).setOnClickListener(this)
        val radioGroup = findViewById<View>(R.id.rg_mark) as RadioGroup

        radioGroup.setOnCheckedChangeListener { _, i ->
            if (i == R.id.home) {
                other_note.visibility = View.GONE
                str_mark = "home"
            } else if (i == R.id.work) {
                other_note.visibility = View.GONE
                str_mark = "work"

            } else if (i == R.id.other) {
                other_note.visibility = View.VISIBLE
                str_mark = "other"
            }
        }
        updateUI()
    }

    private fun updateUI() {
        if (intent.getStringExtra(Constants.IS_UPDATE) == "1") {
            val json = JSONObject(intent.getStringExtra(Constants.OBJECT))
            et_street.setText(json.getString(Constants.STREET))
            et_app_no.setText(json.getString(Constants.APPARTMENT_NO))
            et_floor.setText(json.getString(Constants.FLOOR))
            et_building.setText(json.getString(Constants.BUILDING))
            str_address_id = json.getString(Constants.ID)


            if (json.getString(Constants.MARKS).equals("home", true)) {
                (findViewById<View>(R.id.home) as RadioButton).isChecked = true
                str_mark = "home"
            } else if (json.getString(Constants.MARKS).equals("work", true)) {
                (findViewById<View>(R.id.work) as RadioButton).isChecked = true
                str_mark = "work"
            } else if (json.getString(Constants.MARKS).equals("other", true)) {
                (findViewById<View>(R.id.other) as RadioButton).isChecked = true
                str_mark = "other"
            }
        }
    }

    override fun onClick(p0: View?) {

        when (p0!!.id) {
            R.id.btn_update ->
                if (str_mark.equals("", true)) {
                    Utility.ShowToastMessage(context, getString(R.string.error_select_mark))
                } else {
                    if (intent.getStringExtra(Constants.IS_UPDATE) == "1") {
                        serverUpdateAddress()
                    } else {
                        serverAddAddress()
                    }
                }
        }
    }

    private fun serverAddAddress() {

        progressHud = ProgressHUD.show(context, getString(R.string.loading), true, false, null)
        try {

            val jsonObject = JSONObject()
            jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID))
            jsonObject.put(Constants.STREET, et_street.text.trim())
            jsonObject.put(Constants.BUILDING, et_building.text.trim())
            jsonObject.put(Constants.FLOOR, et_floor.text.trim())
            jsonObject.put(Constants.APPARTMENT_NO, et_app_no.text.trim())
            jsonObject.put(Constants.LATITUDE, intent.getStringExtra(Constants.LATITUDE))
            jsonObject.put(Constants.LONGITUDE, intent.getStringExtra(Constants.LONGITUDE))
            jsonObject.put(Constants.ADDRESS, intent.getStringExtra(Constants.ADDRESS))
            jsonObject.put(Constants.MARKS, str_mark)
            jsonObject.put(Constants.OTHER_NOTE, other_note.text.trim())

            responseTask = ResponseTask(context, jsonObject, Constants.ADD_ADDRESS, TAG, "post")
            responseTask.execute()
            responseTask.setListener { result ->
                if (progressHud.isShowing()) {
                    progressHud.dismiss()
                }
                if (result == null) {
                    Utility.ShowToastMessage(context, getString(R.string.server_not_responding))
                } else {
                    try {
                        val json = JSONObject(result)
                        if (json.getString(Constants.SUCCESS).equals("1", ignoreCase = true)) {

                            Utility.ShowToastMessage(context, getString(R.string.address_added_successfully))
                            MapAddressActivity.IsUpdate = true
                            finish()
                            /*startActivity(Intent(context, HomeActivity::class.java)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
*/

                        } else {
                            Utility.ShowToastMessage(context, json.getString(Constants.MSG))
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun serverUpdateAddress() {

        progressHud = ProgressHUD.show(context, getString(R.string.loading), true, false, null)
        val jsonObject = JSONObject()
        jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID))
        jsonObject.put(Constants.STREET, et_street.text.trim())
        jsonObject.put(Constants.BUILDING, et_building.text.trim())
        jsonObject.put(Constants.FLOOR, et_floor.text.trim())
        jsonObject.put(Constants.APPARTMENT_NO, et_app_no.text.trim())
        jsonObject.put(Constants.LATITUDE, intent.getStringExtra(Constants.LATITUDE))
        jsonObject.put(Constants.LONGITUDE, intent.getStringExtra(Constants.LONGITUDE))
        jsonObject.put(Constants.ADDRESS, intent.getStringExtra(Constants.ADDRESS))
        jsonObject.put(Constants.MARKS, str_mark)
        jsonObject.put(Constants.ID, str_address_id)
        jsonObject.put(Constants.OTHER_NOTE, other_note.text.trim())


        responseTask = ResponseTask(context, jsonObject, Constants.UPADATE_ADDRESS, TAG, "post")
        responseTask.execute()
        responseTask.setListener { result ->
            if (progressHud.isShowing()) {
                progressHud.dismiss()
            }
            if (result == null) {
                Utility.ShowToastMessage(context, getString(R.string.server_not_responding))
            } else {
                try {
                    val json = JSONObject(result)
                    if (json.getString(Constants.SUCCESS).equals("1", ignoreCase = true)) {


                        Utility.ShowToastMessage(context, getString(R.string.address_update_successfully))
                        MapAddressActivity.IsUpdate = true
                        finish()
                        /*   startActivity(Intent(context, HomeActivity::class.java)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
*/

                    } else {
                        Utility.ShowToastMessage(context, json.getString(Constants.MSG))
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        super.onBackPressed()
        return super.onOptionsItemSelected(item)
    }

}