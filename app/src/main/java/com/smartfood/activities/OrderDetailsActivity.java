package com.smartfood.activities;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.smartfood.R;
import com.smartfood.adapter.MenuAdapter;
import com.smartfood.customwidget.CustomButton;
import com.smartfood.customwidget.CustomEditText;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.model.OrderItem;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;
import com.smartfood.utility.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by and-05 on 5/12/17.
 */

public class OrderDetailsActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener {

    final String TAG = OrderDetailsActivity.class.getSimpleName();
    MapView mapView;
    Context context;
    ArrayList<OrderItem> listMenu = new ArrayList<>();
    CustomTextView cancel_reason, btn_track, timer;
    String str_reason = "", str_order_id="", str_date="",str_rating;
    Double del_lat, del_lon;
    Date d1 = null, d2 = null;
    CountDownTimer countDownTimer;
    CustomButton btn_cancel_order, btn_rate;
    ResponseTask responseTask;
    ProgressHUD progresshud;
    private GoogleMap mGoogleMap;
    // OrderParent orderParent;
    CustomTextView order_status, txt_status,order_no,delivery_charge, total_amount,
            restro_name, date, grand_total;
    ImageView ic_status, restro_image;
    MenuAdapter menuAdapter;
    public static boolean update = false;
    Dialog dialogRate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this, Utility.getIngerSharedPreferences(getApplicationContext(), Constants.THEME));
        setContentView(R.layout.activity_order_details);
        context = this;

        bind();
    }

    private void bind() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText(R.string.order_details);
        Utility.ChangeTheme(context, toolbar);
        //------------set adapter ------------------------------


        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        menuAdapter = new MenuAdapter(context, listMenu, 0);
        recyclerView.setAdapter(menuAdapter);
        //--------------common views--------------------------------------
        order_status = (CustomTextView) findViewById(R.id.order_status);
        txt_status = (CustomTextView) findViewById(R.id.txt_status);
        ic_status = (ImageView) findViewById(R.id.ic_status);
        date = (CustomTextView) findViewById(R.id.date);
        restro_name = (CustomTextView) findViewById(R.id.restro_name);
        total_amount = (CustomTextView) findViewById(R.id.total_amount);
        delivery_charge = (CustomTextView) findViewById(R.id.delivery_charge);
        order_no = (CustomTextView) findViewById(R.id.order_no);
        grand_total = (CustomTextView) findViewById(R.id.grand_total);
        restro_image = (ImageView) findViewById(R.id.restro_image);

        //----------------uncommon according to status-------------------
        cancel_reason = (CustomTextView) findViewById(R.id.cancel_reason);
        btn_track = (CustomTextView) findViewById(R.id.btn_track);
        timer = (CustomTextView) findViewById(R.id.timer);
        btn_cancel_order = (CustomButton) findViewById(R.id.btn_cancel);
        btn_rate = (CustomButton) findViewById(R.id.btn_rate);

        cancel_reason.setOnClickListener(this);
        btn_track.setOnClickListener(this);
        btn_cancel_order.setOnClickListener(this);
        btn_rate.setOnClickListener(this);

        //-------------------------set map -----------------------------


        MapView mapview_driver = (MapView) findViewById(R.id.mapview_driver);

        if (mapview_driver != null) {
            mapview_driver.onCreate(null);
            mapview_driver.onResume();
            mapview_driver.getMapAsync(this);
        }

        mapView = (MapView) findViewById(R.id.mapview);
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

        serverGetDetails();

    }

    private void serverGetDetails() {
        if (Utility.isConnectingToInternet(context)) {
            progresshud = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
                jsonObject.put(Constants.ORDER_ID, getIntent().getStringExtra(Constants.ORDER_ID));

                responseTask = new ResponseTask(context, jsonObject, Constants.GET_ORDER_BY_ID, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progresshud != null && progresshud.isShowing()) {
                            progresshud.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    JSONObject object = json.getJSONObject(Constants.OBJECT);

                                    UpdateUi(object);


                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utility.ShowToastMessage(context, getString(R.string.not_connected_to_internet));
        }
    }

    private void UpdateUi(JSONObject object) {

        try {
            String str_status = "";
            str_date = object.getString(Constants.DATE);

            if (object.getString(Constants.STATUS).equals("0")) {
                order_status.setBackgroundResource(R.drawable.z_back_pending);
                str_status = getString(R.string.status) + " " + getString(R.string.pending);
                txt_status.setText(R.string.pending);
                ((CustomTextView) findViewById(R.id.txt_call)).setVisibility(View.VISIBLE);
                ic_status.setBackgroundResource(R.drawable.ic_order_pending);
                timer.setVisibility(View.VISIBLE);
                btn_cancel_order.setVisibility(View.VISIBLE);
                setTimer();
            } else if (object.getString(Constants.STATUS).equals("1")) {
                order_status.setBackgroundResource(R.drawable.z_back_inprogress);
                str_status = getString(R.string.status) + " " + getString(R.string.in_progress);
                txt_status.setText(R.string.in_progress);
                ic_status.setBackgroundResource(R.drawable.ic_complete_lgreen);
            } else if (object.getString(Constants.STATUS).equals("2")) {
                order_status.setBackgroundResource(R.drawable.z_les_rounded_blue);
                str_status = getString(R.string.status) + " " + getString(R.string.on_the_way);
                txt_status.setText(R.string.on_the_way);
                ic_status.setBackgroundResource(R.drawable.ic_on_the_way);
                btn_track.setVisibility(View.VISIBLE);
                ((LinearLayout) findViewById(R.id.driver_details)).setVisibility(View.GONE);
            } else if (object.getString(Constants.STATUS).equals("3")) {
                order_status.setBackgroundResource(R.drawable.z_back_completed);
                str_status = getString(R.string.status) + " " + getString(R.string.completed);
                txt_status.setText(R.string.completed);
                ic_status.setBackgroundResource(R.drawable.ic_complete_lgreen);
                if (object.has(Constants.IS_RATE) && object.getString(Constants.IS_RATE).equals("0")){
                    btn_rate.setVisibility(View.VISIBLE);
                }
            } else if (object.getString(Constants.STATUS).equals("4")) {
                order_status.setBackgroundResource(R.drawable.z_back_rejected);
                str_status = getString(R.string.status) + " " + getString(R.string.rejected);
                txt_status.setText(R.string.rejected);
                ic_status.setBackgroundResource(R.drawable.ic_rejected);
                cancel_reason.setVisibility(View.VISIBLE);
                if (object.has(Constants.REJECT_WHY))
                    str_reason = object.getString(Constants.REJECT_WHY);
            } else if (object.getString(Constants.STATUS).equals("5")) {
                order_status.setBackgroundResource(R.drawable.z_back_rejected);
                str_status = getString(R.string.status) + " " + getString(R.string.cancelled);
                txt_status.setText(R.string.cancelled);
                ic_status.setBackgroundResource(R.drawable.ic_rejected);
                cancel_reason.setVisibility(View.GONE);
            }
            if (!object.getString(Constants.DELIVERY_LAT).equals("") && !object.getString(Constants.DELIVERY_LON).equals("") &&
                    !object.getString(Constants.DELIVERY_LAT).equals("-") && !object.getString(Constants.DELIVERY_LON).equals("-")){
                del_lat = object.getDouble(Constants.DELIVERY_LAT);
                del_lon = object.getDouble(Constants.DELIVERY_LON);
            }

            str_order_id = object.getString(Constants.ORDER_ID);
            String str_delivery_charge = object.getString(Constants.DELIVERY_CHARGE) + " " + getString(R.string.currency);
            String str_total = object.getString(Constants.GRAND_TOTAL) + " " + getString(R.string.currency);
            String str_order_no = getString(R.string.order_no) + " " + object.getString(Constants.ORDER_ID);
            String str_grand_total="";

            if (!object.getString(Constants.DELIVERY_CHARGE).equals("") && !object.getString(Constants.GRAND_TOTAL).equals("")){
                double d_grand_total = Double.parseDouble(object.getString(Constants.GRAND_TOTAL)) +
                        Double.parseDouble(object.getString(Constants.DELIVERY_CHARGE));
                str_grand_total = String.format( Locale.ENGLISH,"%.2f",(d_grand_total))+
                        " "+getString(R.string.currency);
            }

            order_status.setText(str_status);
            total_amount.setText(str_total);
            delivery_charge.setText(str_delivery_charge);
            order_no.setText(str_order_no);
            grand_total.setText(str_grand_total);
            date.setText(object.getString(Constants.DATE));
            restro_name.setText(object.getString(Constants.RESTRO_NAME));

            Glide.with(context).load(object.getString(Constants.RESTRO_IMAGE)).into(restro_image);

            JSONArray arrayMeal = object.getJSONArray(Constants.MY_ORDER);

            for (int j = 0; j < arrayMeal.length() ; j++) {

                JSONObject obj = arrayMeal.getJSONObject(j);

                JSONArray arrayExtra = obj.getJSONArray(Constants.EXTRA);

                StringBuilder str_extra = new StringBuilder();
                for (int k = 0; k < arrayExtra.length(); k++) {
                    if (k == arrayExtra.length() - 1) {
                        str_extra.append(arrayExtra.getJSONObject(k).getString(Constants.NAME));
                    } else {
                        str_extra.append(arrayExtra.getJSONObject(k).getString(Constants.NAME)).append(" + ");
                    }
                }

                listMenu.add(new OrderItem(obj.getString(Constants.MENU_ID), obj.getString(Constants.MENU_NAME),
                        obj.getString(Constants.TOTAL),obj.getString(Constants.QUANTITY), obj.getString(Constants.MENU_IMAGE),
                        obj.getString(Constants.MENU_TITLE),str_extra.toString(),obj.getString(Constants.SUBTOTAL)));
            }

            menuAdapter.notifyDataSetChanged();

            if (mGoogleMap != null && del_lon != null && del_lat != null) {
                LatLng latLng = new LatLng(del_lat,del_lon);
                mGoogleMap.addMarker(new MarkerOptions().position(latLng));
                CameraPosition cameraPosition = CameraPosition.builder().target(latLng).zoom(5).build();
                mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setTimer() {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("Jordon"));
        String currentDate = df.format(Calendar.getInstance().getTime());
        Log.e(TAG, "bind: " + currentDate);

        try {
            d1 = df.parse(currentDate);
            d2 = df.parse(str_date);
            long diff = d1.getTime() - d2.getTime();
            long diffMinutes = diff / (60 * 1000) % 60;
            Log.e(TAG, "setTimer: "+diffMinutes );

            if (diffMinutes >= 0 && diffMinutes < 10) {
                /*String str_time = getString(R.string.time_out) + " " + String.valueOf(10 - diffMinutes) + ":00";
                timer.setText(str_time);
                final long ab = (11 * 60 * 1000 - diffMinutes * 60 * 1000);
                Log.e(TAG, "setTimer: "+str_time );*/

                String str_time = getString(R.string.time_out) + " " + String.valueOf(10 - diffMinutes) + ":00";
                timer.setText(str_time);
                final long ab = (10 * 60 * 1000 - diffMinutes * 60 * 1000);
                Log.e(TAG, "setTimer: "+str_time);

                countDownTimer = new CountDownTimer(ab, 1000) {
                    @Override
                    public void onTick(long l) {
                        Log.e(TAG, "onTick: "+l);
                        long millis = l;
                        String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

                        //  String str_time = getString(R.string.time_out) + " " + (l / 1000) + ":00";
                        timer.setText(getString(R.string.time_out)+" "+hms);
                    }

                    @Override
                    public void onFinish() {
                        countDownTimer.cancel();
                        btn_cancel_order.setVisibility(View.GONE);
                        timer.setText("");

                    }
                }.start();

            }else{
                btn_cancel_order.setVisibility(View.GONE);
                timer.setText("");

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        if (countDownTimer != null){
            countDownTimer.cancel();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(context);
        mGoogleMap = googleMap;

      /*  if (mGoogleMap != null) {
            LatLng latLng = new LatLng(del_lat,del_lon);
            mGoogleMap.addMarker(new MarkerOptions().position(latLng));
            CameraPosition cameraPosition = CameraPosition.builder().target(latLng).zoom(5).build();
            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }*/
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel_reason:
                final Dialog dialog = new Dialog(this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_cancel_reason);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();

                ((CustomTextView) dialog.findViewById(R.id.text_reason)).setText(str_reason);

                ((ImageView) dialog.findViewById(R.id.btn_close)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                break;
            case R.id.btn_track:

                ((ScrollView) findViewById(R.id.scrollview)).fullScroll(ScrollView.FOCUS_DOWN);

                break;
            case R.id.btn_cancel:
                serverCancelOrder();
                break;
            case R.id.btn_rate:
                dialogRate = new Dialog(this);
                dialogRate.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogRate.setContentView(R.layout.dialog_rate_us);
                dialogRate.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialogRate.getWindow().setLayout(DrawerLayout.LayoutParams.MATCH_PARENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
                dialogRate.show();
                RatingBar rating_bar = (RatingBar)dialogRate.findViewById(R.id.rating_bar);
                final CustomEditText comment = (CustomEditText) dialogRate.findViewById(R.id.comment);
                str_rating = "";

                rating_bar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating,
                                                boolean fromUser) {
                        str_rating = String.valueOf(rating);
                    }
                });


                ((CustomButton)dialogRate.findViewById(R.id.btn_submit)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (str_rating.equals("")){
                            Utility.ShowToastMessage(context , getString(R.string.error_give_rating));
                        }else if (comment.getText().toString().isEmpty()){
                            Utility.ShowToastMessage(context , getString(R.string.error_give_comment));
                        }else{
                            serverGiveRating(str_rating, comment.getText().toString());
                        }

                    }
                });

                ((ImageView) dialogRate.findViewById(R.id.btn_close)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogRate.dismiss();
                    }
                });
                break;

        }
    }

    private void serverGiveRating(String str_rate, String review) {
        if (Utility.isConnectingToInternet(context)) {
            progresshud = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
                jsonObject.put(Constants.ORDER_ID,getIntent().getStringExtra(Constants.ORDER_ID));
                jsonObject.put(Constants.RATE, str_rate);
                jsonObject.put(Constants.REVIEW, review);

                responseTask = new ResponseTask(context, jsonObject, Constants.USER_RATE_REVIEW, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progresshud != null && progresshud.isShowing()) {
                            progresshud.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    Utility.ShowToastMessage(context, getString(R.string.thankyou_for_comment));
                                    dialogRate.dismiss();
                                    btn_rate.setVisibility(View.GONE);

                                } else {
                                    dialogRate.dismiss();
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utility.ShowToastMessage(context, getString(R.string.not_connected_to_internet));
        }
    }

    private void serverCancelOrder() {
        if (Utility.isConnectingToInternet(context)) {
            progresshud = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
                jsonObject.put(Constants.ORDER_ID,getIntent().getStringExtra(Constants.ORDER_ID));

                responseTask = new ResponseTask(context, jsonObject, Constants.CANCEL_ORDER, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progresshud != null && progresshud.isShowing()) {
                            progresshud.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    // JSONObject object = json.getJSONObject(Constants.OBJECT);
                                    update = true;
                                    Utility.ShowToastMessage(context , getString(R.string.order_cancelled_successfully));
                                    finish();

                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utility.ShowToastMessage(context, getString(R.string.not_connected_to_internet));
        }

    }
}
