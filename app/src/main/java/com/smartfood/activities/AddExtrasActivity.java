package com.smartfood.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.smartfood.R;
import com.smartfood.customwidget.CustomRadioButton;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.other.ItemOffsetDecoration;
import com.smartfood.model.MenuExtra;
import com.smartfood.model.PriceCal;
import com.smartfood.utility.Constants;
import com.smartfood.utility.Utility;
import com.smartfood.utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 28/11/17.
 */

public class AddExtrasActivity extends BaseActivity {

    Context context;
    ArrayList<MenuExtra> list = new ArrayList<>();
    ExtraAdapter extraAdapter;
    final String TAG = AddExtrasActivity.class.getSimpleName();
    ArrayList<PriceCal> listPrice;
    CustomTextView txt_price;
    private int selectedPosition = -1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this, Utility.getIngerSharedPreferences(getApplicationContext(), Constants.THEME));

        setContentView(R.layout.activity_add_extra);
        context =this;

        Toolbar toolbar = (Toolbar)findViewById(R.id.ly_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);setTitle("");
        ((CustomTextView)findViewById(R.id.toolbar_title)).setText(getIntent().getStringExtra(Constants.SUBITEMS_NAME));
        Utility.ChangeTheme(context, toolbar, findViewById(R.id.ln));


        txt_price = (CustomTextView)findViewById(R.id.txt_price);

        listPrice =  (ArrayList<PriceCal>)getIntent().getSerializableExtra(Constants.MENU_LIST);

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        extraAdapter = new ExtraAdapter(context, list, listPrice, txt_price);
        recyclerView.addItemDecoration(new ItemOffsetDecoration(context, R.dimen._5sdp));
        recyclerView.setAdapter(extraAdapter);
        setData();

        double total = 0;
        for (int i = 0; i < listPrice.size() ; i++) {
            total= total + Double.parseDouble(listPrice.get(i).getPrice());
        }

        txt_price.setText(String.valueOf(total));

    }

    private void setData() {

        String s = getIntent().getStringExtra(Constants.OBJECT);
        Log.e(TAG, "setData: "+s );

        try {
            JSONArray jsonArray = new JSONArray(s);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                list.add(new MenuExtra(object.getString(Constants.ID),object.getString(Constants.NAME),
                        object.getString(Constants.PRICE), false));
            }
            extraAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        for (int i = 0; i < listPrice.size() ; i++) {
            for (int j = 0; j < list.size() ; j++) {
                if (listPrice.get(i).getId().equals(list.get(j).getId())){
                    selectedPosition = j;
                    break;
                }
            }
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public void btn_done(View view) {
        Intent intent = new Intent();
        intent.putExtra(Constants.MENU_LIST , (ArrayList<PriceCal>) listPrice);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    //-------------------------------------------------------------------------------------------

    class ExtraAdapter extends RecyclerView.Adapter<ExtraAdapter.ExtraViewHolder> {

        Context context;
        ArrayList<MenuExtra> list;
        ArrayList<PriceCal> listPrice;
        CustomTextView txt_price;

        public ExtraAdapter(Context context, ArrayList<MenuExtra> list, ArrayList<PriceCal> listPrice, CustomTextView txt_price) {
            this.context = context;
            this.list = list;
            this.listPrice = listPrice;
            this.txt_price = txt_price;
        }

        @Override
        public ExtraViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ExtraViewHolder(LayoutInflater.from(context).inflate(R.layout.item_extra, parent, false));
        }

        @Override
        public void onBindViewHolder(ExtraViewHolder holder, int position) {
            holder.extra_name.setText(list.get(position).getExtra_name());
            holder.price.setText(list.get(position).getExtra_price());

            //check the radio button if both position and selectedPosition matches
            holder.check.setChecked(position == selectedPosition);
            holder.check.setTag(position);
            holder.rl.setTag(position);
        }

        @Override
        public int getItemCount() {
            return list.size();
        }


        class ExtraViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            CustomTextView price, extra_name;
            CustomRadioButton check;
            RelativeLayout rl;

            ExtraViewHolder(View itemView) {
                super(itemView);
                extra_name = (CustomTextView)itemView.findViewById(R.id.extra_name);
                price = (CustomTextView)itemView.findViewById(R.id.price);
                price = (CustomTextView)itemView.findViewById(R.id.price);
                rl = (RelativeLayout) itemView.findViewById(R.id.rl);
                check = (CustomRadioButton) itemView.findViewById(R.id.check);
                rl.setOnClickListener(this);
                check.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.rl:
                        selectItem(view, getAdapterPosition());
                        break;
                    case R.id.check:
                        selectItem(view, getAdapterPosition());
                        break;
                }
            }
        }

        private void selectItem(View view, int pos) {

            if (pos == selectedPosition){
                // select same item to uncheck
                selectedPosition = -1;

                for (int i = 0; i < listPrice.size() ; i++) {
                    for (int j = 0; j < list.size() ; j++) {
                        if (listPrice.get(i).getId().equals(list.get(j).getId())){
                            listPrice.remove(i);
                            break;
                        }
                    }
                }

            }else{
                //select different item and uncheck previous item
                selectedPosition = (Integer) view.getTag();

                for (int i = 0; i < listPrice.size() ; i++) {
                    for (int j = 0; j < list.size() ; j++) {
                        if (listPrice.get(i).getId().equals(list.get(j).getId())){
                            listPrice.remove(i);
                            break;
                        }
                    }
                }

                listPrice.add(new PriceCal(list.get(pos).getId(), list.get(pos).getExtra_price()));

            }
            notifyDataSetChanged();

            //calculate new price and set to textview
           double total = 0;
            for (int i = 0; i < listPrice.size() ; i++) {
                total= total + Double.parseDouble(listPrice.get(i).getPrice());
            }

            txt_price.setText(String.valueOf(total));
        }
    }
}
