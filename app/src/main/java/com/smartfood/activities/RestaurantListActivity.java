package com.smartfood.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.smartfood.R;
import com.smartfood.adapter.RestaurantAdapter;
import com.smartfood.customwidget.CustomEditText;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.customwidget.other.Util;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;
import com.smartfood.utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 13/11/17.
 */

public class RestaurantListActivity extends BaseActivity implements View.OnClickListener {

    Context context;
    ArrayList<JSONObject> listRestro = new ArrayList<>();
    ResponseTask responseTask;
    ProgressHUD progressHUD;
    final String TAG = RestaurantListActivity.class.getSimpleName();
    RestaurantAdapter restaurantAdapter;
    RelativeLayout rl_search;
    CustomEditText edit_search;
    final int Request_filter = 101;
    final int Kitchen_filter = 102;
    String str_budget="", str_has_offer="", str_is_open= "",
            str_sort_by="", str_kitchen_types="", str_rating="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Utils.onActivityCreateSetTheme(this, Utility.getIngerSharedPreferences(getApplicationContext(), Constants.THEME));
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_restaurant_list);

        bindViews();
    }

    private void bindViews() {

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);setTitle("");
        ((CustomTextView)findViewById(R.id.toolbar_title)).setText(R.string.restaurants);

        Utility.ChangeTheme(context, toolbar);

        rl_search = (RelativeLayout)findViewById(R.id.rl_search);
        edit_search = (CustomEditText)findViewById(R.id.edit_search);
        RecyclerView rv_restaurant = (RecyclerView)findViewById(R.id.rv_restaurant);
        rv_restaurant.setLayoutManager(new LinearLayoutManager(this));
        restaurantAdapter = new RestaurantAdapter(context , listRestro);
        rv_restaurant.setAdapter(restaurantAdapter);

        ((LinearLayout)findViewById(R.id.btn_filter)).setOnClickListener(this);
        ((LinearLayout)findViewById(R.id.search_filter)).setOnClickListener(this);
        ((ImageView)findViewById(R.id.ic_search)).setOnClickListener(this);
        ((LinearLayout)findViewById(R.id.kitchen_filter)).setOnClickListener(this);

        str_kitchen_types = getIntent().getStringExtra(Constants.KITCHEN_TYPES);

        if (Utility.isConnectingToInternet(context))
            serverGetList();
        else
            Utility.ShowToastMessage(context , context.getString(R.string.not_connected_to_internet));


    }

    private void serverGetList() {

        progressHUD = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.LATITUDE, "22.7195687");
            jsonObject.put(Constants.LONGITUDE, "75.85772580000003");
            jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context , Constants.USER_ID));
            jsonObject.put(Constants.SORT_BY , str_sort_by);
            jsonObject.put(Constants.BUDGET , str_budget);
            jsonObject.put(Constants.HAS_OFFER , str_has_offer);
            jsonObject.put(Constants.IS_OPEN , str_is_open);
            jsonObject.put(Constants.STAR_RATING , str_rating);
            jsonObject.put(Constants.KITCHEN_TYPES , str_kitchen_types);
            jsonObject.put(Constants.RESTRO_NAME , edit_search.getText().toString());

            responseTask = new ResponseTask(context, jsonObject,Constants.RESTAURANT_LIST, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progressHUD != null && progressHUD.isShowing()) {
                        progressHUD.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            setObject(json);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cart_menu , menu);

        MenuItem item = menu.findItem(R.id.cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();
        // Log.e(TAG, "onCreateOptionsMenu: "+Utility.getSharedPreferences(context, COUNT,0));
        Util.setBadgeCount(this, icon,Utility.getSharedPreferences(context, Constants.CART_COUNT,0));
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.cart){
            startActivity(new Intent(context , CartListActivity.class));
            Utility.activityTransition(context);

        }else {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_filter:
                edit_search.setText("");
                rl_search.setVisibility(View.GONE);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.BUDGET, str_budget);
                bundle.putString(Constants.SORT_BY, str_sort_by);
                bundle.putString(Constants.HAS_OFFER, str_has_offer);
                bundle.putString(Constants.IS_OPEN, str_is_open);
                bundle.putString( Constants.STAR_RATING, str_rating);
                bundle.putString( Constants.KITCHEN_TYPES, str_kitchen_types);

                startActivityForResult(new Intent(context , FilterActivity.class)
                        .putExtras(bundle), Request_filter );
                Utility.activityTransition(context);
                break;
            case R.id.search_filter:
                rl_search.setVisibility(View.VISIBLE);
                break;
            case R.id.ic_search:
                if (!edit_search.getText().toString().isEmpty()){
                    serverGetList();
                }
                break;
            case R.id.kitchen_filter:
                rl_search.setVisibility(View.GONE);
                startActivityForResult(new Intent(context , KitchenFilterActivity.class)
                        .putExtra(Constants.KITCHEN_TYPES, str_kitchen_types),
                        Kitchen_filter);
                Utility.activityTransition(context);

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Request_filter && resultCode==Activity.RESULT_OK){
            try {
                JSONObject object = new JSONObject(data.getStringExtra(Constants.OBJECT));
                setObject(object);
                str_sort_by = data.getStringExtra(Constants.SORT_BY);
                str_budget = data.getStringExtra(Constants.BUDGET);
                str_rating = data.getStringExtra(Constants.STAR_RATING);
                str_has_offer = data.getStringExtra(Constants.HAS_OFFER);
                str_is_open = data.getStringExtra(Constants.IS_OPEN);
                Log.e(TAG, "onActivityResult: "+object);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if (requestCode == Kitchen_filter && resultCode==Activity.RESULT_OK){
            str_kitchen_types = data.getStringExtra(Constants.KITCHEN_TYPES);
            serverGetList();
            Log.e(TAG, "onActivityResult: "+str_kitchen_types );
        }
    }

    private void setObject(JSONObject object) {
        if (object.has(Constants.SUCCESS)) {
            try {
                if (object.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {
                    listRestro.clear();
                    JSONArray jsonArray = object.getJSONArray(Constants.OBJECT);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        listRestro.add(obj);
                    }

                    restaurantAdapter.notifyDataSetChanged();


                } else {
                    Utility.ShowToastMessage(context, object.getString(Constants.MSG));
                    listRestro.clear();
                    restaurantAdapter.notifyDataSetChanged();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
