package com.smartfood.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.smartfood.R;
import com.smartfood.customwidget.CustomAutoCompleteTextView;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.utility.Constants;
import com.smartfood.utility.PlaceJSONParser;
import com.smartfood.utility.Utility;
import com.smartfood.utility.Utils;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by shilpa on 8/25/2016.
 */
public class PickAddressActivity extends AppCompatActivity {

    Context mContext;
    View rootView;
    CustomAutoCompleteTextView pickup_address;
    ParserTaskforautotv parserTask;
    ListView list;
    ProgressHUD progressHUD;
    String str_address="";
    final String TAG = PickAddressActivity.class.getSimpleName();
    double lat=0.0, lon=0.0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this, Utility.getIngerSharedPreferences(getApplicationContext(), Constants.THEME));
        setContentView(R.layout.activity_pick_address);

        mContext = this;
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText(getIntent().getStringExtra("TITLE"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Utility.ChangeTheme(mContext, toolbar);

        Find();
        Initialize();
    }

    public void Find() {
        pickup_address = (CustomAutoCompleteTextView) findViewById(R.id.pickup_address);
        list = (ListView) findViewById(R.id.list);
    }

    public void Initialize() {
        /*FOR AUTOCOMPLETE METHOD*/

        pickup_address.setThreshold(1);
        pickup_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int i, int i1, int i2) {
                Log.e("onTextChanged", "CharSequence ===" + s.toString());

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                str_address = "";

            }

            @Override
            public void afterTextChanged(Editable s) {
                ((CardView) findViewById(R.id.listcardview)).setVisibility(View.VISIBLE);
                PlacesTask placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }
        });
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        finish();//finishing activity
    }

    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            String data = "";
            try {
                String key = "key="+ Constants.MAP_KEY;
                String input = "";
                try {
                    input = "input=" + URLEncoder.encode(place[0], "utf-8");
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
                // https://maps.googleapis.com/maps/api/place/autocomplete/xml?input=Amoeba&types=establishment&
                // location=37.76999,-122.44696&radius=500&key=YOUR_API_KEY
               // String location = "location=" + String.valueOf(latitude) + "," + String.valueOf(longitude);
               // String country = "components=country:jd";
                //String country = "components=country:in";
                String types = "types=geocode";
                String sensor = "sensor=true";
                String parameters = input + "&" + types + "&" /*+ country*/ + "&" + sensor + "&" + key;
                String output = "json";
                String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;
                try {
                    Log.e("downloadUrl", "url ==== " + url);
                    data = downloadUrl(url);
                } catch (Exception e) {
                    Log.d("Background Task", e.toString());
                }
                return data;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            parserTask = new ParserTaskforautotv();
            parserTask.execute(result);
        }
    }

    private class ParserTaskforautotv extends AsyncTask<String, Integer, List<HashMap<String, String>>> {
        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {
            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();
            try {
                jObject = new JSONObject(jsonData[0]);
                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);
            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            String[] from = new String[]{"description"};
            try {
                int[] to = new int[]{android.R.id.text1};
                // Creating a SimpleAdapter for the AutoCompleteTextView

                SimpleAdapter adapter = new SimpleAdapter(mContext, result, android.R.layout.simple_list_item_1, from, to);
                // Setting the adapter
                adapter.notifyDataSetChanged();
                list.setAdapter(adapter);
                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        str_address = ((HashMap<String, String>) parent.getItemAtPosition(position)).get("description");

                       /* Intent intent = new Intent();
                        intent.putExtra("ADDRESS", address);
                        setResult(Activity.RESULT_OK,intent);
                        finish();
*/

                        new GetLatLongFromAddressTask().execute(str_address);

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private class GetLatLongFromAddressTask extends AsyncTask<String, String, Address> {

        String errorMessage = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressHUD = ProgressHUD.show(mContext,getString(R.string.loading), true, false, null);

        }


        @Override
        protected Address doInBackground(String... params) {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            List<Address> addresses = null;

            try {
                addresses = geocoder.getFromLocationName(params[0], 1);
                Log.e(TAG, "doInBackground: "+addresses );
            } catch (IOException e) {
                errorMessage = "Service not available";
                Log.e(TAG, errorMessage, e);
            }
            if (addresses != null && addresses.size() > 0) {
                return addresses.get(0);
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Address address) {
            if (progressHUD != null && progressHUD.isShowing()) {
                progressHUD.dismiss();
            }
            if (address == null) {

            } else {
                try {
                    String addressName = "";
                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                        addressName += " --- " + address.getAddressLine(i);
                    }
                    Log.e(TAG, "Latitude: " + address.getLatitude() + "\n" + "Longitude: " + address.getLongitude() + "\n" +
                            "Address: " + addressName);
                    Log.e(TAG, "newlat === " + address.getLatitude());
                    Log.e(TAG, "newlong === " + address.getLongitude());
                    lat = address.getLatitude();
                    lon = address.getLongitude();

                    Intent intent = new Intent();
                    intent.putExtra(Constants.ADDRESS, str_address);
                    intent.putExtra(Constants.LATITUDE, lat);
                    intent.putExtra(Constants.LONGITUDE, lon);
                    setResult(Activity.RESULT_OK,intent);
                    finish();

                } catch (Exception e) {
                    Log.e(TAG, "onPostExecute Exception ====== " + e.toString());
                    e.printStackTrace();
                }
            }
            super.onPostExecute(address);
        }
    }
}