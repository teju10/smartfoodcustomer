package com.smartfood.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.smartfood.R;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.fragments.HelpSupportFragment;
import com.smartfood.fragments.HomeFragment;
import com.smartfood.fragments.MyAddressFragment;
import com.smartfood.fragments.Notification1Fragment;
import com.smartfood.fragments.ProfileFragment;
import com.smartfood.fragments.SettingFragment;
import com.smartfood.fragments.fav_fragments.FavoriteFragment;
import com.smartfood.fragments.order_fragments.MainOrderFragment;
import com.smartfood.navdrawer.FragmentDrawer;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;
import com.smartfood.utility.Utils;

import org.json.JSONObject;

/**
 * Created by and-05 on 7/11/17.
 */

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, FragmentDrawer.FragmentDrawerListener {

    final String TAG = HomeActivity.class.getSimpleName();
    FragmentDrawer drawerFragment;
    DrawerLayout mDrawerLayout;
    Context context;
    Toolbar toolbar;
    ResponseTask responseTask;
    ProgressHUD progresshud;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        Utility.ChangeLang(context, Utility.getLanSharedPreferences(context, Constants.LANGUAGE, 0));

        Utils.onActivityCreateSetTheme(this, Utility.getIngerSharedPreferences(getApplicationContext(), Constants.THEME));

        setContentView(R.layout.activity_home);
        bindDrawer();

        Utility.ChangeTheme(context, toolbar);

    }

    private void bindDrawer() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("");
        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerFragment.setDrawerListener(this);

        ChangeTheme();

        (mDrawerLayout.findViewById(R.id.about_us)).setOnClickListener(this);
        (mDrawerLayout.findViewById(R.id.share)).setOnClickListener(this);
        (mDrawerLayout.findViewById(R.id.setting)).setOnClickListener(this);
        (mDrawerLayout.findViewById(R.id.notification)).setOnClickListener(this);
        (mDrawerLayout.findViewById(R.id.fav)).setOnClickListener(this);
        (mDrawerLayout.findViewById(R.id.join_us_now)).setOnClickListener(this);

        displayView(0);

    }

    public void ChangeTheme() {

        Utility.ChangeTheme(context, (mDrawerLayout.findViewById(R.id.drawer_title_bar)));
        if (Utility.getIngerSharedPreferences(context, Constants.THEME) == 2) {
            (mDrawerLayout.findViewById(R.id.image)).setBackground(ContextCompat.getDrawable(context, R.drawable.drawer_bg_green));
        }
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.about_us:
                displayView(12);
                break;
            case R.id.share:
                displayView(10);
                break;
            case R.id.setting:
                displayView(9);
                break;
            case R.id.notification:
                displayView(8);
                break;
            case R.id.fav:
                displayView(7);
                break;
            case R.id.join_us_now:
                displayView(11);
                break;
        }
    }

    public void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        if (mDrawerLayout.isDrawerOpen((View) findViewById(R.id.fragment_navigation_drawer))) {
            mDrawerLayout.closeDrawer((View) findViewById(R.id.fragment_navigation_drawer));
        }
        switch (position) {
            case 0:
                fragment = new HomeFragment();
                title = getString(R.string.home);
                break;
            case 2:
                fragment = new MainOrderFragment();
                title = getString(R.string.my_orders);
                break;
            case 3:
                fragment = new ProfileFragment();
                title = getString(R.string.profile_);
                break;
            case 4:
                fragment = new MyAddressFragment();
                title = getString(R.string.my_address);
                break;
            case 5:
                fragment = new HelpSupportFragment();
                title = getString(R.string.help);
                break;
            case 6:
                if (Utility.isConnectingToInternet(context))
                    serverLogout();
                else
                    Utility.ShowToastMessage(context, context.getString(R.string.not_connected_to_internet));
                break;
            case 7:
                fragment = new FavoriteFragment();
                title = getString(R.string.favorite);
                break;
            case 8:
                fragment = new Notification1Fragment();
                title = getString(R.string.notifications);
                break;
            case 9:
                fragment = new SettingFragment();
                title = getString(R.string.setting);
                break;
            case 12:
               /* fragment = new AboutUsFragment();
                title = getString(R.string.about_us);*/
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
            // set the toolbar title
            ((CustomTextView) toolbar.findViewById(R.id.toolbar_title)).setText(title);
        }
    }

    private void serverLogout() {

        progresshud = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));

            responseTask = new ResponseTask(context, jsonObject, Constants.LOGOUT, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                Utility.clearPreference(context);
                                startActivity(new Intent(context, LanguageChangeActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                Utility.activityTransition(context);

                            } else {
                                Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void TurnstoGreen() {
        Utility.setIntegerSharedPreference(context, Constants.THEME, 2);
        Utils.changeToTheme(HomeActivity.this, Utils.THEME_GREEN);
    }

    public void TurnstoYellow() {
        Utility.setIntegerSharedPreference(context, Constants.THEME, 1);
        Utils.changeToTheme(HomeActivity.this, Utils.THEME_DEFAULT);
    }

    public void Dialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(context.getResources().getString(R.string.turned_green_msg));
        builder1.setCancelable(true);

        builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                TurnstoGreen();
                dialog.dismiss();
            }
        });
        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                TurnstoYellow();
                dialog.dismiss();
            }
        });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
//        Utility.ShowToastMessage(context,context.getResources().getString(R.string.backprsed));
        AlertDialog();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 3000);
    }

    public void AlertDialog(){
        final AlertDialog alertDialog = new AlertDialog.Builder(
                HomeActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Alert");

        // Setting Dialog Message
        alertDialog.setMessage(context.getResources().getString(R.string.backprsed));

//        alertDialog.setIcon(R.drawable.tick);

        // Setting OK Button
        alertDialog.setButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                Utility.ShowToastMessage(context, getString(R.string.order_placed_successfully));
               /* startActivity(new Intent(context, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK));
                Utility.activityTransition(context);*/
                alertDialog.dismiss();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


}
