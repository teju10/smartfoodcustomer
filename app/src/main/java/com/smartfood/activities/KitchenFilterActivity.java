package com.smartfood.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.smartfood.R;
import com.smartfood.customwidget.CustomButton;
import com.smartfood.customwidget.CustomCheckBox;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.model.Simple;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;
import com.smartfood.utility.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 1/12/17.
 */

public class KitchenFilterActivity extends BaseActivity {

    Context context;
    ArrayList<Simple> listKitchen = new ArrayList<>();
    ResponseTask responseTask;
    ProgressHUD progressHUD;
    final String TAG = KitchenFilterActivity.class.getSimpleName();
    KitchenTypeAdapter kitchenTypeAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this, Utility.getIngerSharedPreferences(getApplicationContext(), Constants.THEME));

        setContentView(R.layout.activity_kitchen_filter);
        context = this;
        Toolbar toolbar = (Toolbar)findViewById(R.id.ly_toolbar);

        Utility.ChangeTheme(context, toolbar);


        setSupportActionBar(toolbar);setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((CustomTextView)findViewById(R.id.toolbar_title)).setText(R.string.kitchen_types);



        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        kitchenTypeAdapter = new KitchenTypeAdapter(context, listKitchen);
        recyclerView.setAdapter(kitchenTypeAdapter);

        ((CustomButton)findViewById(R.id.btn_done)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StringBuilder stringBuilder = new StringBuilder();

                ArrayList<Simple> list = kitchenTypeAdapter.getList();
                for (int i = 0; i < list.size(); i++) {

                    if (list.get(i).isCheck()){

                        if (stringBuilder.length()==0)
                            stringBuilder = stringBuilder.append(list.get(i).getId());
                        else
                            stringBuilder = stringBuilder.append(",").append(list.get(i).getId());
                    }
                }

                Intent intent = new Intent();
                intent.putExtra(Constants.KITCHEN_TYPES,  stringBuilder.toString());
                setResult(Activity.RESULT_OK, intent);
                finish();

            }
        });

        serverGetKitchen();

    }

    private void serverGetKitchen() {
        progressHUD = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
        try {
            responseTask = new ResponseTask(context, Constants.LIST_KITCHEN, TAG, "get");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progressHUD != null && progressHUD.isShowing()) {
                        progressHUD.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                JSONArray jsonArray = json.getJSONArray(Constants.OBJECT);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    listKitchen.add(new Simple(object.getString(Constants.KITCHEN_ID),
                                            object.getString(Constants.KITCHEN_NAME), false));
                                }
                                String[] str = getIntent().getStringExtra(Constants.KITCHEN_TYPES).split(",");

                                for (int i = 0; i < listKitchen.size() ; i++) {
                                    for (String aStr : str) {
                                        if (listKitchen.get(i).getId().equals(aStr)) {
                                            listKitchen.get(i).setCheck(true);
                                        }
                                    }
                                }

                                kitchenTypeAdapter.notifyDataSetChanged();



                            } else {
                                Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    class KitchenTypeAdapter extends RecyclerView.Adapter<KitchenTypeAdapter.KitchenTypeViewHolder>{

        Context context;
        ArrayList<Simple> listKitchen;
        KitchenTypeAdapter(Context context, ArrayList<Simple> list) {
            this.context = context;
            this.listKitchen = list;
        }

        @Override
        public KitchenTypeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new KitchenTypeViewHolder(LayoutInflater.from(context).inflate(R.layout.item_kitchen_type, parent , false));
        }

        @Override
        public void onBindViewHolder(KitchenTypeViewHolder holder, int position) {

            holder.text.setText(listKitchen.get(position).getValue());
            holder.check.setChecked(listKitchen.get(position).isCheck());
            holder.check.setTag(position);

        }

        @Override
        public int getItemCount() {
            return listKitchen.size();
        }

        class KitchenTypeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            CustomTextView text;
            CustomCheckBox check;
            KitchenTypeViewHolder(View itemView) {
                super(itemView);
                text = (CustomTextView)itemView.findViewById(R.id.text);
                check = (CustomCheckBox) itemView.findViewById(R.id.check);
                itemView.setOnClickListener(this);
                check.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (listKitchen.get(getAdapterPosition()).isCheck()){
                    listKitchen.get(getAdapterPosition()).setCheck(false);
                    check.setChecked(false);

                }else{
                    listKitchen.get(getAdapterPosition()).setCheck(true);
                    check.setChecked(true);

                }


            }
        }

        public ArrayList<Simple> getList(){
            return listKitchen;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
