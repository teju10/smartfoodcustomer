package com.smartfood.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.smartfood.R;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.customwidget.other.Util;
import com.smartfood.model.Spinner_Item;
import com.smartfood.spinner.LuckyWheel;
import com.smartfood.spinner.OnLuckyWheelReachTheTarget;
import com.smartfood.spinner.SpinningWheelView;
import com.smartfood.spinner.WheelItem;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by and-04 on 18/1/18.
 */

public class FeelingLucky_SpinnerActivity extends AppCompatActivity implements SpinningWheelView.OnRotationListener<String> {

    private SpinningWheelView wheelView;
    MediaPlayer mp;
    ResponseTask rt;
    Context context;
    ProgressHUD progresshud;
    private String TAG = "FeelingLucky_SpinnerActivity";
    //ArrayList<Spinner_Item> restarray = new ArrayList<>();
    List<JSONObject> restarray = new ArrayList<>();
    ArrayList<String> mlist;

    Dialog proceeddialog;
    WheelItem item;
    Spinner_Item getitem;
    CustomTextView title;

    Random mRandom = new Random();

    int[] colorarray = {0xffFFE0B2, 0xffFFCC80, 0xffFFF3E0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_felling_lucky_spinner);
        context = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ((CustomTextView) toolbar.findViewById(R.id.toolbar_title)).setText("");
        title = (CustomTextView) findViewById(R.id.title);

        wheelView = (SpinningWheelView) findViewById(R.id.wheel);

        item = new WheelItem();
        mp = new MediaPlayer();

        proceeddialog = new Dialog(context);


        if (Utility.getSharedPreferences(context, Constants.Lucky_Meal_Btn).equals("1")) {
            GetResturantTask();
            title.setText(R.string.restaurant);
        } else if (Utility.getSharedPreferences(context, Constants.Lucky_Meal_Btn).equals("2")) {
            GetFoodMealTask();
            title.setText(R.string.meal);
        }
        wheelView.setOnRotationListener(this);

      /*  List<WheelItem> wheelItems = new ArrayList<>();
        wheelItems.add(new WheelItem(Color.LTGRAY, BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_spin_black), "SAYAJI"));
        wheelItems.add(new WheelItem(Color.BLUE, BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_spin_black), "AMARVILLAS"));
        wheelItems.add(new WheelItem(Color.GREEN, BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_spin_black), "RADISSION"));
        wheelItems.add(new WheelItem(Color.GRAY, BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_spin_black), "TGB"));
        wheelItems.add(new WheelItem(Color.RED, BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_spin_black), "SENSATIONAL"));
        wheelItems.add(new WheelItem(Color.GREEN, BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_spin_black), "hjhfjghfjghj"));*/

//        wheelView.addWheelItems(wheelItems);

        /*item.color = 0xffFFE0B2;
        restarray.add(item);*/


       /* wheelView.setLuckyWheelReachTheTarget(new OnLuckyWheelReachTheTarget() {
            @Override
            public void onReachTarget() {
                if (mp.isPlaying()) {
                    mp.stop();
                }

//                ProceedDialog();
            }
        });

        wheelView.setLuckyRoundItemSelectedListener(new LuckyWheel.LuckyRoundItemSelectedListener() {
            @Override
            public void LuckyRoundItemSelected(int index) {
                Toast.makeText(getApplicationContext(), String.valueOf(index), Toast.LENGTH_SHORT).show();
                System.out.println("Miller=>"+String.valueOf(index));
            }
        });*/
    }

    @Override
    public void onRotation() {
     //   wheelView.rotate(50, 3000, 50);
        /*int index = mRandom.nextInt(5);

        wheelView.rotateWheelTo(index);*/
        Log.w("XXXX", "On Rotation");

        mp = MediaPlayer.create(this, R.raw.elec2);
        if (proceeddialog.isShowing()) {
            proceeddialog.dismiss();
        }

        if (!mp.isPlaying()) {
            mp.start();
        }
    }

    @Override
    public void onStopRotation(String item) {
        Log.w("XXXX", "On Rotation Stop" + item);

        if (mp.isPlaying()) {
            mp.stop();
        }

        ProceedDialog(item, context);
    }

    public void GetResturantTask() {
        progresshud = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.FEELING_LUCKY, "1");
            jo.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
            jo.put(Constants.LATITUDE, "22.6872862");
            jo.put(Constants.LONGITUDE, "75.8589767");
            rt = new ResponseTask(context, jo, Constants.GET_RESTAURANT_LIST, TAG, "post");
            rt.execute();
            rt.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {
                                mlist = new ArrayList<>();
                                mlist.clear();

                                JSONArray jsonArray = json.getJSONArray(Constants.OBJECT);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    restarray.add(jsonArray.getJSONObject(i));
                                   /* restarray.add(new WheelItem(Color.GREEN, BitmapFactory.decodeResource(getResources(),
                                            R.drawable.ic_spin_black), jsonArray.getJSONObject(i).getString("restaurant_name")));*/
//                                        listdataRestro.add(jsonArray.getJSONObject(i));

                                    mlist.add(jsonArray.getJSONObject(i).getString("restaurant_name"));
                                }
                                wheelView.setItems(mlist);
                            } else if (json.getString(Constants.SUCCESS).equals("0")) {
                                wheelView.setVisibility(View.GONE);
                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public void GetFoodMealTask() {
        progresshud = ProgressHUD.show(context, getString(R.string.loading), true, false, null);

        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
            rt = new ResponseTask(context, jo, Constants.GET_LUCKYMEAL_LIST, TAG, "post");
            rt.execute();
            rt.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                JSONArray jsonArray = json.getJSONArray("object");
                                mlist = new ArrayList<>();
                                mlist.clear();

                                for (int i = 0; i < jsonArray.length(); i++) {
                                   /* restarray.add((new WheelItem(Color.GREEN, BitmapFactory.decodeResource(getResources(),
                                            R.drawable.ic_spin_black), jsonArray.getJSONObject(i).getString("menu_name"))));*/
                                    //   restarray.add(getitem);
                                    restarray.add(jsonArray.getJSONObject(i));
                                    mlist.add(jsonArray.getJSONObject(i).getString("menu_name"));

                                }
                                wheelView.setItems(mlist);
                            } else if (json.getString(Constants.SUCCESS).equals("0")) {
                                wheelView.setVisibility(View.GONE);
                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void ProceedDialog(final String MealName, Context con) {
        try {
            proceeddialog = new Dialog(con);
            proceeddialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            proceeddialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

            proceeddialog.setContentView(R.layout.dialog_luckymeal_spinner);
            proceeddialog.setCancelable(false);
            proceeddialog.show();

            final CustomTextView mealname = proceeddialog.findViewById(R.id.mealname);
            mealname.setText(MealName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        proceeddialog.findViewById(R.id.btn_spin_again).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (proceeddialog.isShowing()) {
                    proceeddialog.dismiss();
                }

            }
        });

        proceeddialog.findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (proceeddialog.isShowing()) {
                    proceeddialog.dismiss();
                }
            }
        });

        proceeddialog.findViewById(R.id.btn_continue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (proceeddialog.isShowing()) {
                try {
                    if (Utility.getSharedPreferences(context, Constants.Lucky_Meal_Btn).equals("1")) {
                        for (int i = 0; i < restarray.size(); i++) {

                            if (restarray.get(i).getString(Constants.RESTRO_NAME).equals(MealName)) {

                                startActivity(new Intent(context, RestaurantDetailActivity.class)
                                        .putExtra(Constants.OBJECT, restarray.get(i).toString()));

                                proceeddialog.dismiss();
                            }
                        }
                    } else if (Utility.getSharedPreferences(context, Constants.Lucky_Meal_Btn).equals("2")) {
                        for (int i = 0; i < restarray.size(); i++) {

                            if (restarray.get(i).getString(Constants.MENU_NAME).equals(MealName)) {

                                Bundle bundle = new Bundle();
                                bundle.putString(Constants.RESTAURANT_ID, restarray.get(i).getString(Constants.RESTAURANT_ID));
                                bundle.putString(Constants.MENU_ID, restarray.get(i).getString(Constants.MENU_ID));
                                bundle.putString(Constants.MENU_NAME, restarray.get(i).getString(Constants.MENU_NAME));
                                bundle.putString(Constants.MENU_TITLE, restarray.get(i).getString(Constants.MENU_TITLE));
                                bundle.putString(Constants.MENU_PRICE, restarray.get(i).getString(Constants.MENU_PRICE));
                                bundle.putString(Constants.MENU_IMAGE, restarray.get(i).getString(Constants.MENU_IMAGE));
                                bundle.putString(Constants.DISCOUNT_PRICE, restarray.get(i).getString(Constants.DISCOUNT_PRICE));
                                bundle.putString(Constants.IS_UPDATE, "");
                                proceeddialog.dismiss();

                                startActivity(new Intent(context, MenuDetailsActivity.class)
                                        .putExtras(bundle));
                            }
                        }
                    }

                } catch (JSONException je) {
                    je.printStackTrace();
                }

                proceeddialog.dismiss();

            }
        });
    }

    private int getRandomIndex() {
        Random rand = new Random();
        return rand.nextInt(restarray.size() - 1) + 0;
    }

    private int getRandomRound() {
        Random rand = new Random();
        return rand.nextInt(10) + 15;
    }

}