package com.smartfood.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.smartfood.R;
import com.smartfood.adapter.ViewPagerAdapter;
import com.smartfood.customwidget.CircleImageView;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.customwidget.other.Util;
import com.smartfood.fragments.restaurant_fragments.InfoFragment;
import com.smartfood.fragments.restaurant_fragments.MenuFragment;
import com.smartfood.fragments.restaurant_fragments.ReviewFragment;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;
import com.smartfood.utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by and-05 on 13/11/17.
 */

public class RestaurantDetailActivity extends BaseActivity {

    Context context;
    CustomTextView distance, review_count, restro_time, is_open;
    CircleImageView restro_logo;
    String restro_id;
    ImageView star1, star2, star3, star4, star5, btn_fav;

    ResponseTask responseTask;
    ProgressHUD progresshud;
    final String TAG = RestaurantDetailActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this, Utility.getIngerSharedPreferences(getApplicationContext(), Constants.THEME));
        setContentView(R.layout.activity_restaurant_detail);
        context = this;
        bind();
    }

    private void bind() {
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");

        Utility.ChangeTheme(context, toolbar);

        distance = (CustomTextView)findViewById(R.id.distance);
        review_count = (CustomTextView)findViewById(R.id.review_count);
        restro_time = (CustomTextView)findViewById(R.id.restro_time);
        is_open = (CustomTextView)findViewById(R.id.is_open);
        restro_logo = (CircleImageView) findViewById(R.id.restro_logo);
        star5 = (ImageView)findViewById(R.id.star5);
        star4 = (ImageView)findViewById(R.id.star4);
        star3 = (ImageView)findViewById(R.id.star3);
        star2 = (ImageView)findViewById(R.id.star2);
        star1 = (ImageView)findViewById(R.id.star1);
        btn_fav = (ImageView)findViewById(R.id.btn_fav);

        btn_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serverFavRestaurant();
            }
        });

        try {
            JSONObject object = new JSONObject(getIntent().getStringExtra(Constants.OBJECT));
            Log.e(TAG, "Object data "+ object.toString());

            String str_review_count = "";

            if (!object.getString(Constants.COUNT_REVIEW).equals("0"))
                str_review_count = object.getString(Constants.COUNT_REVIEW)+" "+context.getString(R.string.reviews);
            else
                str_review_count = getString(R.string.no_reviews);

            if (object.getString(Constants.OPEN_NOW).equals("1"))
                is_open.setText(R.string.open_now);
            else
                is_open.setText(R.string.closed_now);

            if (object.getString(Constants.IS_FAV).equals("1"))
                btn_fav.setBackgroundResource(R.drawable.ic_fav_filled);
            else
                btn_fav.setBackgroundResource(R.drawable.ic_fav_b);

            restro_id = object.getString(Constants.RESTAURANT_ID);
            distance.setText(object.getString(Constants.DISTANCE));
            restro_time.setText(object.getString(Constants.RESTRO_TIME));
            review_count.setText(str_review_count);
            ((CustomTextView)findViewById(R.id.toolbar_title))
                    .setText(object.getString(Constants.RESTRO_NAME));

            Glide.with(context).load(object.getString(Constants.LOGO_IMAGE)).into(restro_logo);

            Utility.Rating(Float.parseFloat(object.getString(Constants.AVG_RATING)), star1, star2, star3, star4, star5, R.drawable.ic_star_b_empty,
                    R.drawable.ic_star_b_half, R.drawable.ic_star_b_filled);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                CustomTextView selectedText = (CustomTextView) view.findViewById(android.R.id.text1);
                selectedText.setTextColor(ContextCompat.getColor(context, R.color.colorBlue));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                CustomTextView selectedText = (CustomTextView) view.findViewById(android.R.id.text1);
                selectedText.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            //noinspection ConstantConditions
            TextView tv=(TextView) LayoutInflater.from(this).inflate(R.layout.item_tabs,null);
            tabLayout.getTabAt(i).setCustomView(tv);

            if (i==0) {
                tv.setTextColor(ContextCompat.getColor(context, R.color.colorBlue));
            }
        }
    }

    private void serverFavRestaurant() {
        if (Utility.isConnectingToInternet(context)) {
            progresshud = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
                jsonObject.put(Constants.RESTAURANT_ID, restro_id);

                responseTask = new ResponseTask(context, jsonObject, Constants.FAV_RESTAURANT, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progresshud != null && progresshud.isShowing()) {
                            progresshud.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    JSONObject object = json.getJSONObject(Constants.OBJECT);

                                    if (object.getString(Constants.FAVOURITE).equals("1"))
                                        btn_fav.setBackgroundResource(R.drawable.ic_fav_filled);
                                    else
                                        btn_fav.setBackgroundResource(R.drawable.ic_fav_b);


                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Utility.ShowToastMessage(context , getString(R.string.not_connected_to_internet));
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.RESTAURANT_ID , restro_id);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        MenuFragment menuFragment = new MenuFragment();
        ReviewFragment reviewFragment =new ReviewFragment();
        InfoFragment infoFragment = new InfoFragment();
        menuFragment.setArguments(bundle);
        reviewFragment.setArguments(bundle);
        infoFragment.setArguments(bundle);
        adapter.addFragment(menuFragment,getString(R.string.menu));
        adapter.addFragment(reviewFragment,getString(R.string.review));
        adapter.addFragment(infoFragment,getString(R.string.info));
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.cart){
            startActivity(new Intent(context , CartListActivity.class));
            Utility.activityTransition(context);

        }else {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cart_menu , menu);

        MenuItem item = menu.findItem(R.id.cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();
        // Log.e(TAG, "onCreateOptionsMenu: "+Utility.getSharedPreferences(context, COUNT,0));
        Util.setBadgeCount(this, icon,Utility.getSharedPreferences(context, Constants.CART_COUNT,0));
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }
}
