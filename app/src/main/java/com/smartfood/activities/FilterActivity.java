package com.smartfood.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.smartfood.R;
import com.smartfood.customwidget.CustomButton;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.customwidget.other.ItemOffsetDecoration;
import com.smartfood.model.Simple;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;
import com.smartfood.utility.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 20/11/17.
 */

public class FilterActivity extends BaseActivity implements View.OnClickListener {

    ArrayList<Simple> listKitchen = new ArrayList<>();
    Context context;
    ProgressHUD progressHUD;
    ResponseTask responseTask;
    CustomButton btn_show_restro;
    CustomTextView _5_star_rating,is_open,has_offer,has_discount;
    LinearLayout budget_three, budget_two, budget_one, best_sellers, rating, delivery_time, a_to_z,lowest_price;
    boolean b_5_star = false, b_is_open = false, b_has_offer = false, b_has_discount = false;
    KitchenTypeAdapter kitchenTypeAdapter;
    final String TAG = FilterActivity.class.getSimpleName();
    String str_budget="", str_has_offer="", str_is_open= "",
            str_sort_by="", str_kitchen_types="", str_rating="";
    JSONObject object = new JSONObject();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this, Utility.getIngerSharedPreferences(getApplicationContext(), Constants.THEME));

        setContentView(R.layout.activity_filter);
        context = this;
        bind();

    }

    private void bind() {

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);setTitle("");
        ((CustomTextView)findViewById(R.id.toolbar_title)).setText(R.string.filter);

        Utility.ChangeTheme(context, toolbar);
        RecyclerView recyclerview = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.HORIZONTAL));
        kitchenTypeAdapter = new KitchenTypeAdapter(context , listKitchen);
        recyclerview.setAdapter(kitchenTypeAdapter);
        recyclerview.addItemDecoration(new ItemOffsetDecoration(context, R.dimen.item_offset));

       // serverGetKitchen();

        _5_star_rating =(CustomTextView)findViewById(R.id._5_star_rating);
        is_open =(CustomTextView)findViewById(R.id.is_open);
        has_offer =(CustomTextView)findViewById(R.id.has_offer);
        has_discount =(CustomTextView)findViewById(R.id.has_discount);
        budget_one = (LinearLayout)findViewById(R.id.budget_one);
        budget_two = (LinearLayout)findViewById(R.id.budget_two);
        budget_three = (LinearLayout)findViewById(R.id.budget_three);
        lowest_price = (LinearLayout)findViewById(R.id.lowest_price);
        a_to_z = (LinearLayout)findViewById(R.id.a_to_z);
        delivery_time = (LinearLayout)findViewById(R.id.delivery_time);
        rating = (LinearLayout)findViewById(R.id.rating);
        best_sellers = (LinearLayout)findViewById(R.id.best_sellers);
        btn_show_restro = (CustomButton) findViewById(R.id.btn_show_restro);

        _5_star_rating.setOnClickListener(this);
        is_open.setOnClickListener(this);
        has_offer.setOnClickListener(this);
        has_discount.setOnClickListener(this);
        budget_one.setOnClickListener(this);
        budget_two.setOnClickListener(this);
        budget_three.setOnClickListener(this);
        lowest_price.setOnClickListener(this);
        a_to_z.setOnClickListener(this);
        delivery_time.setOnClickListener(this);
        rating.setOnClickListener(this);
        best_sellers.setOnClickListener(this);
        btn_show_restro.setOnClickListener(this);

        setFilters();
    }

    private void setFilters() {
        Bundle bundle = getIntent().getExtras();

        str_sort_by = bundle.getString(Constants.SORT_BY);
        str_budget = bundle.getString(Constants.BUDGET);
        str_rating = bundle.getString(Constants.STAR_RATING);
        str_has_offer = bundle.getString(Constants.HAS_OFFER);
        str_is_open = bundle.getString(Constants.IS_OPEN);
        str_kitchen_types =bundle.getString(Constants.KITCHEN_TYPES);

        switch (str_sort_by) {
            case Constants.LOWEST_PRICE:
                lowest_price.setBackgroundColor(ContextCompat.getColor(context, R.color.colorYellow));
                break;
            case Constants.BEST_SELLER:
                best_sellers.setBackgroundColor(ContextCompat.getColor(context, R.color.colorYellow));
                break;
            case Constants.RATING:
                rating.setBackgroundColor(ContextCompat.getColor(context, R.color.colorYellow));
                break;
            case Constants.DELIVERY_TIME:
                delivery_time.setBackgroundColor(ContextCompat.getColor(context, R.color.colorYellow));
                break;
            case Constants.ALPHABET:
                a_to_z.setBackgroundColor(ContextCompat.getColor(context, R.color.colorYellow));
                break;
        }

        switch (str_budget) {
            case "1":
                budget_one.setBackgroundColor(ContextCompat.getColor(context, R.color.colorYellow));
                break;
            case "2":
                budget_two.setBackgroundColor(ContextCompat.getColor(context, R.color.colorYellow));
                break;
            case "3":
                budget_three.setBackgroundColor(ContextCompat.getColor(context, R.color.colorYellow));
                break;
        }

        if (str_rating.equals("1")){
            b_5_star = true ;
            _5_star_rating.setBackgroundResource(R.drawable.z_rounded_blue);
            _5_star_rating.setTextColor(ContextCompat.getColor(context,R.color.colorWhite));
        }

        if (str_has_offer.equals("1")){
            b_has_offer = true ;
            has_offer.setBackgroundResource(R.drawable.z_rounded_blue);
            has_offer.setTextColor(ContextCompat.getColor(context,R.color.colorWhite));
        }

        if (str_is_open.equals("1")){
            b_is_open = true ;
            is_open.setBackgroundResource(R.drawable.z_rounded_blue);
            is_open.setTextColor(ContextCompat.getColor(context,R.color.colorWhite));
        }

    }

    private void serverGetKitchen() {
        progressHUD = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
        try {
            responseTask = new ResponseTask(context, Constants.LIST_KITCHEN, TAG, "get");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progressHUD != null && progressHUD.isShowing()) {
                        progressHUD.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                JSONArray jsonArray = json.getJSONArray(Constants.OBJECT);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    listKitchen.add(new Simple(object.getString(Constants.KITCHEN_ID),
                                            object.getString(Constants.KITCHEN_NAME), false));
                                }

                                kitchenTypeAdapter.notifyDataSetChanged();

                            } else {
                                Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id._5_star_rating:
                if (b_5_star){
                    str_rating = "0";
                    b_5_star = false;
                    _5_star_rating.setBackgroundResource(R.drawable.z_border_yellow);
                    _5_star_rating.setTextColor(ContextCompat.getColor(context,R.color.colorBlue));
                }else{
                    str_rating = "1";
                    b_5_star = true ;
                    _5_star_rating.setBackgroundResource(R.drawable.z_rounded_blue);
                    _5_star_rating.setTextColor(ContextCompat.getColor(context,R.color.colorWhite));
                }
                serverGetList();
                break;
            case R.id.is_open:

                if (b_is_open){
                    str_is_open = "0";
                    b_is_open = false;
                    is_open.setBackgroundResource(R.drawable.z_border_yellow);
                    is_open.setTextColor(ContextCompat.getColor(context,R.color.colorBlue));
                }else{
                    str_is_open = "1";
                    b_is_open = true ;
                    is_open.setBackgroundResource(R.drawable.z_rounded_blue);
                    is_open.setTextColor(ContextCompat.getColor(context,R.color.colorWhite));
                }
                serverGetList();

                break;
            case R.id.has_offer:

                if (b_has_offer){
                    str_has_offer = "0";
                    b_has_offer = false;
                    has_offer.setBackgroundResource(R.drawable.z_border_yellow);
                    has_offer.setTextColor(ContextCompat.getColor(context,R.color.colorBlue));
                }else{
                    str_has_offer = "1";
                    b_has_offer = true ;
                    has_offer.setBackgroundResource(R.drawable.z_rounded_blue);
                    has_offer.setTextColor(ContextCompat.getColor(context,R.color.colorWhite));
                }
                serverGetList();

                break;
            case R.id.has_discount:

                if (b_has_discount){
                    b_has_discount = false;
                    has_discount.setBackgroundResource(R.drawable.z_border_yellow);
                    has_discount.setTextColor(ContextCompat.getColor(context,R.color.colorBlue));
                }else{
                    b_has_discount = true ;
                    has_discount.setBackgroundResource(R.drawable.z_rounded_blue);
                    has_discount.setTextColor(ContextCompat.getColor(context,R.color.colorWhite));
                }
                serverGetList();

                break;
            case R.id.budget_one:
                str_budget = "1";
                changeBudgetFilter(budget_one , budget_two , budget_three);
                serverGetList();

                break;
            case R.id.budget_two:
                str_budget = "2";
                changeBudgetFilter(budget_two , budget_three, budget_one);
                serverGetList();

                break;
            case R.id.budget_three:
                str_budget = "3";
                changeBudgetFilter(budget_three,budget_one , budget_two);
                serverGetList();

                break;
            case R.id.lowest_price:
                str_sort_by = Constants.LOWEST_PRICE;
                changeSortByFilter(lowest_price,a_to_z , delivery_time, rating, best_sellers);
                serverGetList();
                break;
            case R.id.a_to_z:
                str_sort_by = Constants.ALPHABET;
                changeSortByFilter(a_to_z ,lowest_price, delivery_time, rating, best_sellers);
                serverGetList();
                break;
            case R.id.delivery_time:
                str_sort_by = Constants.DELIVERY_TIME;
                changeSortByFilter(delivery_time, a_to_z ,lowest_price, rating, best_sellers);
                serverGetList();
                break;
            case R.id.rating:
                str_sort_by = Constants.RATING;
                changeSortByFilter(rating,delivery_time, a_to_z ,lowest_price,  best_sellers);
                serverGetList();
                break;
            case R.id.best_sellers:
                str_sort_by = Constants.BEST_SELLER;
                changeSortByFilter(best_sellers, rating,delivery_time, a_to_z ,lowest_price);
                serverGetList();
                break;
            case R.id.btn_show_restro:
                Intent intent = new Intent();
                intent.putExtra(Constants.OBJECT , object.toString());
                intent.putExtra(Constants.BUDGET, str_budget);
                intent.putExtra(Constants.SORT_BY, str_sort_by);
                intent.putExtra(Constants.HAS_OFFER, str_has_offer);
                intent.putExtra(Constants.IS_OPEN, str_is_open);
                intent.putExtra( Constants.STAR_RATING, str_rating);

                setResult(Activity.RESULT_OK, intent);
                finish();
                break;
        }
    }

    private void changeBudgetFilter(LinearLayout ln1, LinearLayout ln2, LinearLayout ln3) {

        if (Utility.getIngerSharedPreferences(context, Constants.THEME) == 2){
            ln1.setBackgroundColor(ContextCompat.getColor(context,R.color.primary_green));
        }else {
            ln1.setBackgroundColor(ContextCompat.getColor(context,R.color.colorYellow));
        }
        ln2.setBackgroundColor(ContextCompat.getColor(context,R.color.colorWhite));
        ln3.setBackgroundColor(ContextCompat.getColor(context,R.color.colorWhite));
    }

    private void changeSortByFilter(LinearLayout ln1, LinearLayout ln2, LinearLayout ln3,
                                    LinearLayout ln4,  LinearLayout ln5) {

        if (Utility.getIngerSharedPreferences(context, Constants.THEME) == 2){
            ln1.setBackgroundColor(ContextCompat.getColor(context,R.color.primary_green));
        }else {
            ln1.setBackgroundColor(ContextCompat.getColor(context,R.color.colorYellow));
        }
        ln2.setBackgroundColor(ContextCompat.getColor(context,R.color.colorWhite));
        ln3.setBackgroundColor(ContextCompat.getColor(context,R.color.colorWhite));
        ln4.setBackgroundColor(ContextCompat.getColor(context,R.color.colorWhite));
        ln5.setBackgroundColor(ContextCompat.getColor(context,R.color.colorWhite));
    }

    private void resetFilter( LinearLayout ...ln) {
        for (LinearLayout _ln: ln) {
            _ln.setBackgroundColor(ContextCompat.getColor(context,R.color.colorWhite));
        }
    }

    private void resetQuickFilter( CustomTextView ...tv) {
        for (CustomTextView _tv: tv) {
            _tv.setBackgroundResource(R.drawable.z_border_yellow);
            _tv.setTextColor(ContextCompat.getColor(context,R.color.colorBlue));
        }
    }

    private void resetKitchenFilter() {

        for (int i = 0; i < listKitchen.size(); i++) {
            listKitchen.get(i).setCheck(false);
        }
        kitchenTypeAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.filter_menu , menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.clear){
            str_budget="";
            str_has_offer="";
            str_is_open= "";
            str_sort_by="";
            str_kitchen_types="";
            str_rating="";
            resetFilter(lowest_price,a_to_z , delivery_time, rating,
                    best_sellers, budget_two , budget_three, budget_one);

            resetQuickFilter(has_discount, has_offer, is_open,_5_star_rating);

            resetKitchenFilter();

            serverGetList();

        }else {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    private void serverGetList() {
        if (Utility.isConnectingToInternet(context)) {
            progressHUD = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put(Constants.LATITUDE, "22.7195687");
                jsonObject.put(Constants.LONGITUDE, "75.85772580000003");
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
                jsonObject.put(Constants.SORT_BY, str_sort_by);
                jsonObject.put(Constants.BUDGET, str_budget);
                jsonObject.put(Constants.HAS_OFFER, str_has_offer);
                jsonObject.put(Constants.IS_OPEN, str_is_open);
                jsonObject.put(Constants.STAR_RATING, str_rating);
                jsonObject.put(Constants.KITCHEN_TYPES, str_kitchen_types);


                responseTask = new ResponseTask(context, jsonObject, Constants.RESTAURANT_LIST, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progressHUD != null && progressHUD.isShowing()) {
                            progressHUD.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                object = new JSONObject(result);
                                if (object.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    JSONArray jsonArray = object.getJSONArray(Constants.OBJECT);
                                    String str = getString(R.string.show_restaurant)+" "+"("+jsonArray.length()+")";
                                    btn_show_restro.setText(str);

                                }else{
                                    Utility.ShowToastMessage(context, object.getString(Constants.MSG));
                                    btn_show_restro.setText(getString(R.string.show_restaurant));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Utility.ShowToastMessage(context , getString(R.string.not_connected_to_internet));
        }
    }

    //--------------------------Kitchen Adapter-----------------------------------------------
    class KitchenTypeAdapter extends RecyclerView.Adapter<KitchenTypeAdapter.KitchenTypeViewHolder>{

        Context context;
        ArrayList<Simple> list;
        public KitchenTypeAdapter(Context context, ArrayList<Simple> list) {
            this.context = context;
            this.list = list;
        }

        @Override
        public KitchenTypeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new KitchenTypeViewHolder(LayoutInflater.from(context).inflate(R.layout.item_kitchen_type, parent , false));
        }

        @Override
        public void onBindViewHolder(KitchenTypeViewHolder holder, int position) {

            holder.text.setText(list.get(position).getValue());

            if (list.get(position).isCheck()){
                holder.text.setBackgroundResource(R.drawable.z_rounded_blue);
                holder.text.setTextColor(ContextCompat.getColor(context , R.color.colorWhite));
            }else{
                holder.text.setBackgroundResource(R.drawable.z_border_blue);
                holder.text.setTextColor(ContextCompat.getColor(context , R.color.colorBlue));
            }

        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        class KitchenTypeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            CustomTextView text;
            KitchenTypeViewHolder(View itemView) {
                super(itemView);
                text = (CustomTextView)itemView.findViewById(R.id.text);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (listKitchen.get(getAdapterPosition()).isCheck()){
                    listKitchen.get(getAdapterPosition()).setCheck(false);
                    text.setBackgroundResource(R.drawable.z_border_blue);
                    text.setTextColor(ContextCompat.getColor(context , R.color.colorBlue));

                }else{
                    listKitchen.get(getAdapterPosition()).setCheck(true);
                    text.setBackgroundResource(R.drawable.z_rounded_blue);
                    text.setTextColor(ContextCompat.getColor(context , R.color.colorWhite));
                }
                str_kitchen_types = "";

                for (int i = 0; i < listKitchen.size(); i++) {

                    if (listKitchen.get(i).isCheck()){
                        if (!str_kitchen_types.equals(""))
                            str_kitchen_types = str_kitchen_types +","+ listKitchen.get(i).getId();
                        else
                            str_kitchen_types = listKitchen.get(i).getId();
                    }
                }

                serverGetList();
            }
        }
    }
}
