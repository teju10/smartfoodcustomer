package com.smartfood.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.smartfood.R;
import com.smartfood.adapter.AddressAdapter;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.RuntimePermissionsActivity;
import com.smartfood.utility.Utility;
import com.smartfood.utility.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 19/12/17.
 */

public class MyAddressListActivity extends RuntimePermissionsActivity implements View.OnClickListener{

    Context context;
    ResponseTask responseTask;
    ProgressHUD progressHUD;
    final String TAG = MyAddressListActivity.class.getSimpleName();
    ArrayList<JSONObject> list = new ArrayList<>();
    AddressAdapter addressAdapter;
    final int PERMISSION_REQUEST_CODE = 101;
    private FusedLocationProviderClient mFusedLocationClient;
    protected Location mLastLocation;
    Double lat, lon;
    String address_id="";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this, Utility.getIngerSharedPreferences(getApplicationContext(), Constants.THEME));
        setContentView(R.layout.activity_address_list);
        context = this;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        address_id = getIntent().getStringExtra(Constants.ADDRESS_ID);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText(R.string.change_address);

        Utility.ChangeTheme(context, toolbar);

        ((CardView)findViewById(R.id.btn_add_loc)).setOnClickListener(this);
        ((CardView)findViewById(R.id.btn_my_loc)).setOnClickListener(this);
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        addressAdapter = new AddressAdapter(context , list, address_id);
        recyclerView.setAdapter(addressAdapter);
        serverAddress();
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        getLastLocation();
    }

    private void serverAddress() {

        progressHUD = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context , Constants.USER_ID));

            responseTask = new ResponseTask(context, jsonObject,Constants.GET_MY_ADDRESS, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progressHUD != null && progressHUD.isShowing()) {
                        progressHUD.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {
                                list.clear();
                                JSONArray jsonArray = json.getJSONArray(Constants.OBJECT);

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    list.add(jsonArray.getJSONObject(i));
                                }
                                addressAdapter.notifyDataSetChanged();
                            } else {
                                Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_add_loc:
                startActivity(new Intent(context , MapAddressActivity.class)
                        .putExtra(Constants.IS_UPDATE,""));
                Utility.activityTransition(context);
                break;
            case R.id.btn_my_loc:
                if (hasPermissions(context, Manifest.permission.ACCESS_FINE_LOCATION)) {

                    getLastLocation();

                }else{
                    requestAppPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            R.string.permission_needed, PERMISSION_REQUEST_CODE);

                }
                break;
        }
    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.getLastLocation().addOnCompleteListener(this, new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                if (task.isSuccessful() && task.getResult() != null) {
                    mLastLocation = task.getResult();
                    lat = mLastLocation.getLatitude();
                    lon = mLastLocation.getLongitude();

                    Intent intent = new Intent();
                    intent.putExtra(Constants.ADDRESS_ID, "");
                    intent.putExtra(Constants.LATITUDE, String.valueOf(lat));
                    intent.putExtra(Constants.LONGITUDE, String.valueOf(lon));
                    intent.putExtra(Constants.MARKS, context.getResources().getString(R.string.meal_delivered_to_current_location));
                    setResult(Activity.RESULT_OK, intent);
                    finish();

                } else {
                    Log.w(TAG, "getLastLocation:exception", task.getException());
                    Utility.ShowToastMessage(context, getString(R.string.no_location_detected));
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (MapAddressActivity.IsUpdate){
            serverAddress();
            MapAddressActivity.IsUpdate = false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
