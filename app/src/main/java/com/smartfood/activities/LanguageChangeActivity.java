package com.smartfood.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.smartfood.R;
import com.smartfood.utility.Constants;
import com.smartfood.utility.Utility;

/**
 * Created by and-05 on 7/11/17.
 */

public class LanguageChangeActivity extends BaseActivity {

    Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_change);
        context = this;

        if (!Utility.getSharedPreferences(context, Constants.USER_ID).equals("")){
            startActivity(new Intent(context, HomeActivity.class));
            Utility.activityTransition(context);
            finish();
        }
    }

    public void btn_english(View view) {
        Utility.setLanSharedPreference(context , Constants.LANGUAGE, 0);
        startActivity(new Intent(context , LoginActivity.class));
        Utility.activityTransition(context);
    }

    public void btn_arabic(View view) {
        Utility.setLanSharedPreference(context , Constants.LANGUAGE, 1);
        startActivity(new Intent(context , LoginActivity.class));
        Utility.activityTransition(context);
    }
}
