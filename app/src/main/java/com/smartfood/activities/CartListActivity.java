package com.smartfood.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.smartfood.R;
import com.smartfood.adapter.CartAdapter;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.model.OrderItem;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.RuntimePermissionsActivity;
import com.smartfood.utility.Utility;
import com.smartfood.utility.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 18/11/17.
 */

public class CartListActivity extends RuntimePermissionsActivity {

    final String TAG = CartListActivity.class.getSimpleName();
    final int PERMISSION_REQUEST_CODE = 101;
    Context context;
    ArrayList<JSONObject> cartList = new ArrayList<>();
    CartAdapter cartAdapter;
    ResponseTask responseTask;
    ProgressHUD progresshud;
    CustomTextView tv_price;
    LinearLayout ln_total;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this, Utility.getIngerSharedPreferences(getApplicationContext(), Constants.THEME));
        setContentView(R.layout.activity_cartlist);
        context = this;

        bind();
    }

    private void bind() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.ly_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText(R.string.cart_details);
        ln_total = (LinearLayout) findViewById(R.id.ln_total);

        tv_price = (CustomTextView) findViewById(R.id.grand_total);

        Utility.ChangeTheme(context, toolbar,ln_total);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        cartAdapter = new CartAdapter(context, cartList, ln_total, tv_price);
        recyclerView.setAdapter(cartAdapter);

        serverGetCart();

    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    private void serverGetCart() {

        if (Utility.isConnectingToInternet(context)) {
            progresshud = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));

                responseTask = new ResponseTask(context, jsonObject, Constants.GET_CART_ITEMS, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progresshud != null && progresshud.isShowing()) {
                            progresshud.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {
                                    ln_total.setVisibility(View.VISIBLE);
                                    cartList.clear();
                                    JSONObject object = json.getJSONObject(Constants.OBJECT);
                                    JSONArray jsonArray = object.getJSONArray(Constants.CART_ITEMS);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        cartList.add(jsonArray.getJSONObject(i));
                                    }

                                    String grand_total = object.getString(Constants.GRAND_TOTAL) + " " + getString(R.string.currency);

                                    tv_price.setText(grand_total);

                                    cartAdapter.notifyDataSetChanged();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utility.ShowToastMessage(context, getString(R.string.not_connected_to_internet));
        }

    }

    public void btn_proceed(View view) {
        if (hasPermissions(context, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Log.e(TAG, "btn_proceed: " + cartList.size());

            sendData();
        } else {
            requestAppPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    R.string.permission_needed, PERMISSION_REQUEST_CODE);
        }

    }

    private void sendData() {
        ArrayList<OrderItem> listSend = new ArrayList<>();

        try {
            for (int i = 0; i < cartList.size(); i++) {
                JSONObject obj = cartList.get(i);

                JSONArray arrayExtra = obj.getJSONArray(Constants.EXTRA);

                StringBuilder str_extra = new StringBuilder();
                for (int k = 0; k < arrayExtra.length(); k++) {
                    if (k == arrayExtra.length() - 1) {
                        str_extra.append(arrayExtra.getJSONObject(k).getString(Constants.NAME));
                    } else {
                        str_extra.append(arrayExtra.getJSONObject(k).getString(Constants.NAME)).append(" + ");
                    }
                }

                listSend.add(new OrderItem(obj.getString(Constants.MENU_ID), obj.getString(Constants.MENU_NAME),
                        obj.getString(Constants.TOTAL), obj.getString(Constants.MENU_QTY), obj.getString(Constants.MENU_IMAGE),
                        obj.getString(Constants.MENU_TITLE), str_extra.toString(), obj.getString(Constants.SUBTOTAL)));


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        startActivity(new Intent(context, CheckoutActivity.class)
                .putExtra(Constants.MENU_LIST, (ArrayList<OrderItem>) listSend)
                .putExtra(Constants.GRAND_TOTAL, tv_price.getText().toString()));
        Utility.activityTransition(context);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        sendData();
    }

   /* @Override
    protected void onResume() {
        super.onResume();
        serverGetCart();

    }*/
}
