package com.smartfood.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RadioGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.smartfood.R;
import com.smartfood.adapter.MenuAdapter;
import com.smartfood.customwidget.CustomButton;
import com.smartfood.customwidget.CustomEditText;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.customwidget.other.ItemOffsetDecoration;
import com.smartfood.model.OrderItem;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;
import com.smartfood.utility.Utils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static android.content.ContentValues.TAG;

/**
 * Created by and-05 on 17/11/17.
 */

public class CheckoutActivity extends BaseActivity implements OnMapReadyCallback,
        View.OnClickListener, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    final int Request_Code = 101;
    protected Location mLastLocation;
    MapView mapView;
    Double lat, lon;
    ResponseTask responseTask;
    ProgressHUD progresshud;
    MenuAdapter menuAdapter;
    Context context;
    ArrayList<OrderItem> listMenu = new ArrayList<>();
    String str_note_driver = "", str_note_retsro = "", address_id = "",
            strday = "Today", DeliveryChargeKey = "", restaurant_id = "", menu_id = "";
    TimePickerDialog tpd;
    DatePickerDialog dpd;
    CustomTextView delivery_time, btn_coupon, tv_delivery_chages;
    Calendar now = Calendar.getInstance(Locale.ENGLISH);
    String str_date = "", str_time = "", str_coupon_id = "";
    CustomTextView edit_time;
    private GoogleMap mGoogleMap;
    private FusedLocationProviderClient mFusedLocationClient;
    Marker marker;
    int var = 0;
    Dialog diaCoupon;
    RadioGroup rg_edit_day;
    CustomTextView txt_delivery_add;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this, Utility.getIngerSharedPreferences(getApplicationContext(), Constants.THEME));
        setContentView(R.layout.activity_checkout);
        var = 1;

        context = this;
        bind();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        tpd = TimePickerDialog.newInstance(CheckoutActivity.this, 0, 0, false);
        dpd = DatePickerDialog.newInstance(this, now.get(Calendar.YEAR), now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));
        dpd.setMinDate(now);

        rg_edit_day.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.today) {
                    strday = "Today";
                    tpd.setMinTime(now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), now.get(Calendar.SECOND));

                    tpd.show(getFragmentManager(), "Time picker");

                } else {
                    tpd = TimePickerDialog.newInstance(CheckoutActivity.this, 0, 0, false);
                    strday = "Tomorrow";
                    tpd.show(getFragmentManager(), "Time picker");
                }
            }
        });

    }

    @SuppressLint("ClickableViewAccessibility")
    private void bind() {

        rg_edit_day = (RadioGroup) findViewById(R.id.rg_edit_day);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        delivery_time = (CustomTextView) findViewById(R.id.delivery_time);

        Utility.ChangeTheme(context, toolbar);

        btn_coupon = (CustomTextView) findViewById(R.id.btn_coupon);
        tv_delivery_chages = (CustomTextView) findViewById(R.id.delivery_charge);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText(R.string.checkout);
        ((CustomTextView) findViewById(R.id.name)).setText(Utility.getSharedPreferences(context, Constants.FULLNAME));
        ((CustomTextView) findViewById(R.id.date)).setText(Utility.getCurrentTime());
        txt_delivery_add = ((CustomTextView) findViewById(R.id.txt_delivery_add));

        if (!Utility.getSharedPreferences(context, Constants.CONTACT).equals(""))
            ((CustomTextView) findViewById(R.id.phone)).setText(Utility.getSharedPreferences(context, Constants.CONTACT));
        else
            ((CustomTextView) findViewById(R.id.phone)).setText(R.string.n_a);

        DeliveryChargeKey = getIntent().getStringExtra(Constants.DELIVERY_CHARGE_KEY);
        restaurant_id = getIntent().getStringExtra(Constants.RESTAURANT_ID);
        listMenu.addAll((ArrayList<OrderItem>) getIntent().getSerializableExtra(Constants.MENU_LIST));
        for (int i = 0; i < listMenu.size(); i++) {
            menu_id = listMenu.get(i).getId();
        }
        RecyclerView recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerview.addItemDecoration(new ItemOffsetDecoration(context, R.dimen.item_offset));
        menuAdapter = new MenuAdapter(context, listMenu, 1);
        recyclerview.setAdapter(menuAdapter);

        ((CustomTextView) findViewById(R.id.total_amount)).setText(getIntent().getStringExtra(Constants.GRAND_TOTAL));

        mapView = (MapView) findViewById(R.id.mapview);
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

        (findViewById(R.id.change_address)).setOnClickListener(this);
        (findViewById(R.id.add_note)).setOnClickListener(this);
        (findViewById(R.id.btn_confirm)).setOnClickListener(this);
        btn_coupon.setOnClickListener(this);
        edit_time = (CustomTextView) findViewById(R.id.edit_time);

        edit_time.setOnClickListener(this);

    }

    @Override
    public void onStart() {
        super.onStart();
        if (var == 1) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
                    requestLocationPermission();
                } else {
                    Locations();
                }
            } else {
                Locations();
            }

            getLastLocation();
            var = 0;
        }

    }

    public void Locations() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        }
      /*  buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
        checkLocationSettings();*/
    }

    public void requestLocationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.getLastLocation().addOnCompleteListener(this, new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                if (task.isSuccessful() && task.getResult() != null) {
                    mLastLocation = task.getResult();
                    lat = mLastLocation.getLatitude();
                    lon = mLastLocation.getLongitude();
                    marker = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));
                    CameraPosition cameraPosition = CameraPosition.builder().target(new LatLng(lat, lon)).zoom(15).build();
                    LatLng uman = new LatLng(lat, lon);
                    mGoogleMap.addMarker(new MarkerOptions().position(uman).title(""));

                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(uman, 15));

                   /* if (mGoogleMap != null)
                        mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/
                    //serverDeliveryCharges();

                    if (DeliveryChargeKey.equals("1")) {
                        LuckyMealserverDeliveryCharges();
                    } else {
                        serverDeliveryCharges();
                    }

                } else {
                    Log.w(TAG, "getLastLocation:exception", task.getException());
                    Utility.ShowToastMessage(context, getString(R.string.no_location_detected));
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize(context);
        mGoogleMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mGoogleMap.setMyLocationEnabled(true);

       /* mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        // Add a marker in Uman and move the camera
        LatLng uman = new LatLng(lat, lon);
        mGoogleMap.addMarker(new MarkerOptions().position(uman).title(""));
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(uman, 15));*/
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edit_time:
               /* if (edit_time.getText().equals(getString(R.string.edit))) {
                    dpd.show(getFragmentManager(), "Date picker");
                } else if (edit_time.getText().equals(getString(R.string.reset))) {
                    edit_time.setText(getString(R.string.edit));
                    delivery_time.setText(R.string.asap);
                }*/

                rg_edit_day.setVisibility(View.VISIBLE);
                if (edit_time.getText().equals(getString(R.string.reset))) {
                    edit_time.setText(getString(R.string.edit));
                    delivery_time.setText(R.string.asap);
                } /*else if (!delivery_time.getText().toString().equals(R.string.asap)) {
//                    tpd.show(getFragmentManager(), "Time picker");
                }else{
                                        tpd.show(getFragmentManager(), "Time picker");

                }*/
                break;
            case R.id.change_address:
                //  Utility.ShowToastMessage(context, "Coming soon");
                startActivityForResult(new Intent(context, MyAddressListActivity.class)
                        .putExtra(Constants.ADDRESS_ID, address_id), Request_Code);
                Utility.activityTransition(context);

                break;
            case R.id.add_note:
                final Dialog dialog = new Dialog(this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_add_note);
                dialog.getWindow().setLayout(DrawerLayout.LayoutParams.MATCH_PARENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();

                ((CustomEditText) dialog.findViewById(R.id.note_driver)).setText(str_note_driver);
                ((CustomEditText) dialog.findViewById(R.id.note_restro)).setText(str_note_retsro);

                ((CustomButton) dialog.findViewById(R.id.btn_done)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        str_note_driver = ((CustomEditText) dialog.findViewById(R.id.note_driver)).getText().toString();
                        str_note_retsro = ((CustomEditText) dialog.findViewById(R.id.note_restro)).getText().toString();
                        dialog.dismiss();
                    }
                });

                ((ImageView) dialog.findViewById(R.id.btn_close)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                break;
            case R.id.btn_confirm:
               /* if (!strday.equals("")) {

                } else {
                    Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                }*/
                serverConfirmOrder();
//                serverConfirmOrder();
                break;
            case R.id.btn_coupon:
                diaCoupon = new Dialog(this);
                diaCoupon.requestWindowFeature(Window.FEATURE_NO_TITLE);
                diaCoupon.setContentView(R.layout.dialog_coupon);
                diaCoupon.getWindow().setLayout(DrawerLayout.LayoutParams.MATCH_PARENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
                diaCoupon.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                diaCoupon.show();

                final CustomEditText edit_coupon = (CustomEditText) diaCoupon.findViewById(R.id.coupon_name);

                ((CustomButton) diaCoupon.findViewById(R.id.btn_submit)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String amount = ((CustomTextView) findViewById(R.id.total_amount)).getText().toString();
                        if (!amount.equals(""))
                            serverApplyCoupon(edit_coupon.getText().toString(), amount.split(" ")[0]);
                    }
                });

                ((ImageView) diaCoupon.findViewById(R.id.btn_close)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        diaCoupon.dismiss();
                    }
                });
                break;
        }
    }

    private void serverApplyCoupon(String coupon, final String amount) {

        if (Utility.isConnectingToInternet(context)) {
            progresshud = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
                jsonObject.put(Constants.TOTAL_AMOUNT, amount);
                jsonObject.put(Constants.COUPON_NAME, coupon);

                responseTask = new ResponseTask(context, jsonObject, Constants.CHECK_COUPON, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progresshud != null && progresshud.isShowing()) {
                            progresshud.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    Utility.ShowToastMessage(context, getString(R.string.coupon_code_applied_successfully));

                                    str_coupon_id = json.getJSONObject(Constants.OBJECT).getString(Constants.COUPON_ID);
                                    double discount = json.getJSONObject(Constants.OBJECT).getDouble(Constants.COUPON_DISCOUNT);

                                    double discount_amount = (Double.parseDouble(amount) * (100 - discount)) / 100;
                                    double amount = discount_amount + Double.parseDouble(tv_delivery_chages.getText().toString().split(" ")[0]);

                                    String final_amount = String.format(Locale.ENGLISH, "%.2f", amount) + " " + getString(R.string.currency);
                                    ((CustomTextView) findViewById(R.id.grand_total)).setText(final_amount);
                                    diaCoupon.dismiss();

                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utility.ShowToastMessage(context, getString(R.string.not_connected_to_internet));
        }

    }

    private void serverConfirmOrder() {
        if (Utility.isConnectingToInternet(context)) {
            progresshud = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
                jsonObject.put(Constants.DELIVERY_TIME, delivery_time.getText().toString());
                jsonObject.put(Constants.DELIVERY_LAT, lat);
                jsonObject.put(Constants.DELIVERY_LON, lon);
                jsonObject.put(Constants.NOTE_DRIVER, str_note_driver);
                jsonObject.put(Constants.NOTE_RESTRO, str_note_retsro);
                jsonObject.put(Constants.ADDRESS_ID, address_id);
                jsonObject.put(Constants.COUPON_ID, str_coupon_id);
                jsonObject.put(Constants.DAY, strday);

                if (DeliveryChargeKey.equals("1")) {
                    jsonObject.put(Constants.ISLUCKY, "1");
                    jsonObject.put(Constants.MEALID, menu_id);
                    jsonObject.put(Constants.DELIVERY_CHARGE, tv_delivery_chages.getText().toString());
                } else {
                    jsonObject.put(Constants.ISLUCKY, "0");
                    jsonObject.put(Constants.MEALID, "");
                    jsonObject.put(Constants.DELIVERY_CHARGE, "");

                }
                responseTask = new ResponseTask(context, jsonObject, Constants.CONFIRM_ORDER, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progresshud != null && progresshud.isShowing()) {
                            progresshud.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    Utility.setSharedPreference(context, Constants.CART_COUNT, 0);
                                    AlertDialog();

                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utility.ShowToastMessage(context, getString(R.string.not_connected_to_internet));
        }
    }

    private void serverDeliveryCharges() {
        if (Utility.isConnectingToInternet(context)) {
            progresshud = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
                jsonObject.put(Constants.ADDRESS_ID, address_id);
                jsonObject.put(Constants.DELIVERY_LAT, lat);
                jsonObject.put(Constants.DELIVERY_LON, lon);

                responseTask = new ResponseTask(context, jsonObject, Constants.DELIVERY_CHARGE, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progresshud != null && progresshud.isShowing()) {
                            progresshud.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    String estimated_price = json.getString(Constants.ESTIMATE_CHARGE)
                                            + " " + getString(R.string.currency);

                                    tv_delivery_chages.setText(estimated_price);

                                    String total_amount = String.format(Locale.ENGLISH, "%.2f", (Double.parseDouble(getIntent().getStringExtra(Constants.GRAND_TOTAL).split(" ")[0])
                                            + Double.parseDouble(json.getString(Constants.ESTIMATE_CHARGE)))) + " " +
                                            getString(R.string.currency);


                                    ((CustomTextView) findViewById(R.id.grand_total)).setText(total_amount);

                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utility.ShowToastMessage(context, getString(R.string.not_connected_to_internet));
        }
    }

    private void LuckyMealserverDeliveryCharges() {
        if (Utility.isConnectingToInternet(context)) {
            progresshud = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.RESTAURANT_ID, restaurant_id);
                jsonObject.put(Constants.DELIVERY_LAT, lat);
                jsonObject.put(Constants.DELIVERY_LON, lon);
                jsonObject.put(Constants.ADDRESS_ID, address_id);

                responseTask = new ResponseTask(context, jsonObject, Constants.LUCKY_MEAL_DELIVERY_CHARGE, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progresshud != null && progresshud.isShowing()) {
                            progresshud.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    String estimated_price = json.getString(Constants.ESTIMATE_CHARGE)
                                            + " " + getString(R.string.currency);

                                    tv_delivery_chages.setText(estimated_price);

                                    String total_amount = String.format(Locale.ENGLISH, "%.2f", (Double.parseDouble(getIntent().getStringExtra(Constants.GRAND_TOTAL).split(" ")[0])
                                            + Double.parseDouble(json.getString(Constants.ESTIMATE_CHARGE)))) + " " +
                                            getString(R.string.currency);


                                    ((CustomTextView) findViewById(R.id.grand_total)).setText(total_amount);

                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utility.ShowToastMessage(context, getString(R.string.not_connected_to_internet));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Request_Code && resultCode == Activity.RESULT_OK) {

            address_id = data.getStringExtra(Constants.ADDRESS_ID);
            lat = Double.parseDouble(data.getStringExtra(Constants.LATITUDE));
            lon = Double.parseDouble(data.getStringExtra(Constants.LONGITUDE));
            txt_delivery_add.setText(data.getStringExtra(Constants.MARKS));

            if (marker != null) {
                marker.remove();
            }
            LatLng uman = new LatLng(lat, lon);

            marker = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)));
            if (lat != null && lon != null) {
                CameraPosition cameraPosition = CameraPosition.builder().target(uman).zoom(10).build();
                mGoogleMap.addMarker(new MarkerOptions().position(uman).title(""));

                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(uman, 15));


                /*if (mGoogleMap != null)
                    mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/
            }

//
            if (DeliveryChargeKey.equals("1")) {
                LuckyMealserverDeliveryCharges();
            } else {
                serverDeliveryCharges();
            }
        }
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

        String AM_PM;

        if (hourOfDay < 12) {
            AM_PM = "AM";
        } else {
            AM_PM = "PM";
        }

        if (hourOfDay > 12) {
            hourOfDay = hourOfDay - 12;
        }
//        tpd.setMinTime(0, 0, 0);

        str_time = String.format(Locale.ENGLISH, "%02d", hourOfDay) + ":"
                + String.format(Locale.ENGLISH, "%02d", minute) + AM_PM;

        String str = str_date + " " + str_time;
        delivery_time.setText(str);
        edit_time.setText(R.string.reset);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        str_date = String.format(Locale.ENGLISH, "%02d", dayOfMonth) + "-" +
                String.format(Locale.ENGLISH, "%02d", monthOfYear + 1) + "-" + String.format(Locale.ENGLISH, "%02d", year);

        if (str_date.equals(Utility.getCurrentDate())) {
            tpd.setMinTime(now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), now.get(Calendar.SECOND));
        } else {
            tpd.setMinTime(0, 0, 0);
        }

        tpd.show(getFragmentManager(), "Time picker");
    }


    public void AlertDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(
                CheckoutActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle(context.getResources().getString(R.string.ordercnfrim));

        // Setting Dialog Message
        alertDialog.setMessage(context.getResources().getString(R.string.orderconfirm_wait));

//        alertDialog.setIcon(R.drawable.tick);

        // Setting OK Button
        alertDialog.setButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                Utility.ShowToastMessage(context, getString(R.string.order_placed_successfully));
                startActivity(new Intent(context, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK));
                Utility.activityTransition(context);
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
}
