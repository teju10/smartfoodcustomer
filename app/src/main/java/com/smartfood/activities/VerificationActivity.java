package com.smartfood.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import com.smartfood.R;
import com.smartfood.customwidget.CustomEditText;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.json.JSONObject;

/**
 * Created by and-05 on 14/11/17.
 */

public class VerificationActivity extends AppCompatActivity {

    CustomEditText edit1, edit2, edit3, edit4, edit5;
    ResponseTask responseTask;
    ProgressHUD progresshud;
    Context context;
    final String TAG =VerificationActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        Utility.ChangeLang(context , Utility.getLanSharedPreferences(context , Constants.LANGUAGE, 0));
        setContentView(R.layout.activity_verification);

        edit1 = (CustomEditText)findViewById(R.id.edit1);
        edit2 = (CustomEditText)findViewById(R.id.edit2);
        edit3 = (CustomEditText)findViewById(R.id.edit3);
        edit4 = (CustomEditText)findViewById(R.id.edit4);
        edit5 = (CustomEditText)findViewById(R.id.edit5);

        edit1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
                if (s.length()>0){
                    edit2.requestFocus();
                }


            }
        });

        edit2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e(TAG, "onTextChanged: "+"fffffffffff" );
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                Log.e(TAG, "beforeTextChanged: "+"fhhhhhhhh" );

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length()>0){
                    edit3.requestFocus();
                }else if (s.length() == 0){
                    edit1.requestFocus();
                }
            }
        });

        edit3.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length()>0){
                    edit4.requestFocus();
                }else if (s.length() == 0){
                    edit2.requestFocus();
                }
            }
        });

        edit4.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
                if (s.length()>0){
                    edit5.requestFocus();
                }else if (s.length() == 0){
                    edit3.requestFocus();
                }
            }
        });

        edit5.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
                if (s.length() == 0){
                    edit4.requestFocus();
                }
            }
        });

        edit2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if (edit2.getText().toString().equals("")){
                        edit1.requestFocus();
                    }
                }
                return false;
            }
        });

        edit3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if (edit3.getText().toString().equals("")){
                        edit2.requestFocus();
                    }
                }
                return false;
            }
        });

        edit4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if (edit4.getText().toString().equals("")){
                        edit3.requestFocus();
                    }
                }
                return false;
            }
        });

        edit5.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if (edit5.getText().toString().equals("")){
                        edit4.requestFocus();
                    }
                }
                return false;
            }
        });


    }

    public void btn_confirm(View view) {
        String token = edit1.getText().toString() + edit2.getText().toString()
                + edit3.getText().toString() + edit4.getText().toString()+ edit5.getText().toString();

        progresshud = ProgressHUD.show(context, getString(R.string.loading),true, false, null);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.USER_ID, getIntent().getStringExtra(Constants.USER_ID));
            jsonObject.put(Constants.TOKEN, token );

            responseTask = new ResponseTask(context, jsonObject,Constants.VARIFY_ACCOUNT, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                Utility.setSharedPreference(context , Constants.USER_ID,getIntent().getStringExtra(Constants.USER_ID));
                                Utility.setSharedPreference(context , Constants.FULLNAME,getIntent().getStringExtra(Constants.FULLNAME));
                                Utility.setSharedPreference(context , Constants.CONTACT,getIntent().getStringExtra(Constants.CONTACT));
                                Utility.setSharedPreference(context , Constants.CART_COUNT,getIntent().getIntExtra(Constants.CART_COUNT, 0));

                                startActivity(new Intent(context , HomeActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                Utility.activityTransition(context);


                            } else {
                                Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
