package com.smartfood.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.smartfood.R;
import com.smartfood.adapter.RestaurantAdapter;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;
import com.smartfood.utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 28/12/17.
 */

public class FeelingLuckyActivity extends BaseActivity {

    final String TAG = FeelingLuckyActivity.class.getSimpleName();
    ProgressHUD progressHUD;
    ResponseTask responseTask;
    Context context;
    ArrayList<JSONObject> listRestro = new ArrayList<>();
    RestaurantAdapter restaurantAdapter;
    RecyclerView rv_restaurant;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this, Utility.getIngerSharedPreferences(getApplicationContext(), Constants.THEME));
        setContentView(R.layout.activity_feeling_lucky);
        context = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText(R.string.restaurants);

        Utility.ChangeTheme(context, toolbar);

        rv_restaurant = (RecyclerView) findViewById(R.id.rv_restaurant);
        rv_restaurant.setLayoutManager(new LinearLayoutManager(this));

        serverGetList();
    }

    private void serverGetList() {

        progressHUD = ProgressHUD.show(context, getString(R.string.loading), true, false, null);
        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.LATITUDE, "22.7195687");
            jsonObject.put(Constants.LONGITUDE, "75.85772580000003");
            jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
            jsonObject.put(Constants.FEELING_LUCKY, "1");

            responseTask = new ResponseTask(context, jsonObject, Constants.RESTAURANT_LIST, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progressHUD != null && progressHUD.isShowing()) {
                        progressHUD.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(context, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            try {
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {
                                    listRestro.clear();
                                    JSONArray jsonArray = json.getJSONArray(Constants.OBJECT);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject obj = jsonArray.getJSONObject(i);
                                        listRestro.add(obj);
                                    }

                                    //restaurantAdapter.notifyDataSetChanged();
                                    restaurantAdapter = new RestaurantAdapter(context, listRestro);
                                    rv_restaurant.setAdapter(restaurantAdapter);
                                    LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
                                    rv_restaurant.setLayoutAnimation(animation);


                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                    listRestro.clear();
                                    restaurantAdapter.notifyDataSetChanged();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
