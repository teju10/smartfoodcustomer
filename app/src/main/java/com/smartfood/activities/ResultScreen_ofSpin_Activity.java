package com.smartfood.activities;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smartfood.Cursor_WheelLayout.RippleBackground;
import com.smartfood.R;
import com.smartfood.customwidget.CircleImageView;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.model.OrderItem;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-04 on 9/2/18.
 */

public class ResultScreen_ofSpin_Activity extends AppCompatActivity implements View.OnClickListener {

    CircleImageView splash;
    private Animation aright, abottom, atop;
    Context mContext;
    ProgressHUD progresshud;
    ResponseTask rt;
    JSONObject jo;
    private static final String TAG = "ResultScreen_ofSpin_Activity";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_result_spinnerscreen);
        mContext = this;

        /*Toolbar toolbar = (Toolbar) findViewById(R.id.ly_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");*/


        splash = (CircleImageView) findViewById(R.id.item_img);
        final Animation bounce = AnimationUtils.loadAnimation(getBaseContext(), R.anim.bounce);
        final RippleBackground rippleBackground = (RippleBackground) findViewById(R.id.content);
        final Handler handler = new Handler();

        //splash.startAnimation(bounce);
        rippleBackground.startRippleAnimation();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rippleBackground.stopRippleAnimation();

            }
        }, 3000);

        // foundDevice();

        findViewById(R.id.spin_again).setOnClickListener(this);
        findViewById(R.id.meal_spin_again).setOnClickListener(this);
        findViewById(R.id.meal_ordernow_btn).setOnClickListener(this);
        findViewById(R.id.order_now).setOnClickListener(this);
        findViewById(R.id.cross_back).setOnClickListener(this);

        GetData();

        if (Utility.isConnectingToInternet(mContext)) {
              SendCountTask();
        } else {
            Utility.ShowToastMessage(mContext, mContext.getResources().getString(R.string.not_connected_to_internet));
        }

    }

    @SuppressLint("LongLogTag")
    private void GetData() {
        try {
            jo = new JSONObject(getIntent().getStringExtra(Constants.OBJECT));
            System.out.println("JO========> " + jo);

              findViewById(R.id.main_layout).setBackgroundColor(Color.parseColor(jo.getString("colors")));

            if (Utility.getSharedPreferences(mContext, Constants.SPIN_TYPE_CLICK).equals("1")) {
                findViewById(R.id.rest_bar).setVisibility(View.VISIBLE);
                findViewById(R.id.meal_bar).setVisibility(View.GONE);
                findViewById(R.id.price_layout).setVisibility(View.GONE);
                Glide.with(mContext).load(jo.getString("restaurant_image")).into((CircleImageView) findViewById(R.id.item_img));
                ((CustomTextView) findViewById(R.id.item_name)).setText(jo.getString("restaurant_name"));
                ((CustomTextView) findViewById(R.id.sub_item_name)).setText(jo.getString("restaurant_title"));

            } else if (Utility.getSharedPreferences(mContext, Constants.SPIN_TYPE_CLICK).equals("2")) {

                findViewById(R.id.rest_bar).setVisibility(View.GONE);
                findViewById(R.id.meal_bar).setVisibility(View.VISIBLE);

                Glide.with(mContext).load(jo.getString("menu_image")).into((CircleImageView) findViewById(R.id.item_img));
                Glide.with(mContext).load(jo.getString("logo_image")).into((CircleImageView) findViewById(R.id.meal_img));
                ((CustomTextView) findViewById(R.id.item_name)).setText(jo.getString("menu_name"));
                ((CustomTextView) findViewById(R.id.sub_item_name)).setText(jo.getString("menu_title"));
                ((CustomTextView) findViewById(R.id.meal_price)).setText(jo.getString("discount_price"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void foundDevice() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(100);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(splash, "ScaleX", 0f, 1.2f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(splash, "ScaleY", 0f, 1.2f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        splash.setVisibility(View.VISIBLE);
        animatorSet.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.meal_ordernow_btn:
                sendMealData();
                break;

            case R.id.spin_again:
                finish();
                break;

            case R.id.meal_spin_again:
                finish();
                break;

            case R.id.order_now:
                SendRestaurantData();
                break;

                case R.id.cross_back:
                finish();
                break;
        }
    }

    public void SendCountTask() {
        //  progresshud = ProgressHUD.show(mContext, getString(R.string.loading), true, false, null);

        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.USER_ID, Utility.getSharedPreferences(mContext, Constants.USER_ID));
            rt = new ResponseTask(mContext, jo, Constants.UPDATESPINCOUNT, TAG, "post");
            rt.execute();
            rt.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    /*if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }*/
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, getString(R.string.server_not_responding));
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                JSONObject jobj = json.getJSONObject("object");

                                Utility.setSharedPreference(mContext, Constants.SPINCOUNT, jobj.getString("count"));

                            } else if (json.getString(Constants.SUCCESS).equals("0")) {

                            }
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            });
        } catch (JSONException je) {
            je.printStackTrace();
        }
    }

    //Send Data to Checkout

    private void sendMealData() {
        ArrayList<OrderItem> listSend = new ArrayList<>();

        try {

            /*StringBuilder str_extra = new StringBuilder();
            for (int k = 0; k < arrayExtra.length(); k++) {
                if (k == arrayExtra.length() - 1) {
                    str_extra.append(arrayExtra.getJSONObject(k).getString(Constants.NAME));
                } else {
                    str_extra.append(arrayExtra.getJSONObject(k).getString(Constants.NAME)).append(" + ");
                }
            }*/

            listSend.add(new OrderItem(jo.getString(Constants.MENU_ID), jo.getString(Constants.MENU_NAME),
                    jo.getString(Constants.TOTAL), jo.getString(Constants.MENU_QTY), jo.getString(Constants.MENU_IMAGE),
                    jo.getString(Constants.MENU_TITLE), ""/*str_extra.toString()*/, jo.getString(Constants.SUBTOTAL)));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            startActivity(new Intent(mContext, CheckoutActivity.class)
                    .putExtra(Constants.MENU_LIST, (ArrayList<OrderItem>) listSend)
                    .putExtra(Constants.DELIVERY_CHARGE_KEY, "1")
                    .putExtra(Constants.RESTAURANT_ID, jo.getString("restaurant_id")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Utility.activityTransition(mContext);
    }

    public void SendRestaurantData() {

        startActivity(new Intent(mContext, RestaurantDetailActivity.class)
                .putExtra(Constants.OBJECT, jo.toString()));

    }
}
