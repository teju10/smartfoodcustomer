package com.smartfood.spinner;

import android.graphics.Bitmap;

/**
 * Created by mohamed on 22/04/17.
 */

public class WheelItem {

    public int color;

    public WheelItem() {
    }

    public Bitmap bitmap;
    public String str;


    public WheelItem(int color, Bitmap bitmap, String str) {
        this.color = color;
        this.bitmap = bitmap;
        this.str = str;
    }
}
