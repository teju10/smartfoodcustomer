package com.smartfood.spinner;

/**
 * Created by mohamed on 24/04/17.
 */

public interface OnLuckyWheelReachTheTarget {

    void onReachTarget();

}
