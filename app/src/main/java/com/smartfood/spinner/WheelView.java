package com.smartfood.spinner;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import java.util.List;

/**
 * Created by mohamed on 22/04/17.
 */

public class WheelView extends View {
    private final int DEFAULT_PADDING = 5, DEFAULT_ROTATION_TIME = 9000;

    private RectF range = new RectF();
    private Paint archPaint;
    private int padding, radius, center, mWheelBackground;
    private List<WheelItem> mWheelItems;
    private OnLuckyWheelReachTheTarget mOnLuckyWheelReachTheTarget;
    private int mTargetIndex;
    private boolean isRunning = false;
    private int mRoundOfNumber = 4;


    private Paint textPaint;
    private Circle circle;
    private float wheelStrokeRadius;
    private float wheelStrokeWidth = 10;

    private PieRotateListener mPieRotateListener;

    public interface PieRotateListener {
        void rotateDone(int index);
    }
    public void setPieRotateListener(PieRotateListener listener) {
        this.mPieRotateListener = listener;
    }

    private final static float ANGLE = 360f;


    public WheelView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public WheelView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initComponents() {
        //arc paint object
        archPaint = new Paint();
        archPaint.setAntiAlias(true);
        archPaint.setDither(true);

        //-------TExt------------------
        textPaint = new Paint();
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(40);
        textPaint.setFakeBoldText(true);

        //rect rang of the arc
        range = new RectF(padding, padding, padding + radius, padding + radius);

        initWheelStrokeRadius();
    }

    private void initCircle() {
        int width = getMeasuredWidth() == 0 ? getWidth() : getMeasuredWidth();
        int height = getMeasuredHeight() == 0 ? getHeight() : getMeasuredHeight();

        circle = new Circle(width, height);
    }


    /**
     * Get the angele of the target
     *
     * @return Number of angle
     */
    private float getAngleOfIndexTarget(int target) {
        return (360 / mWheelItems.size()) * target;
    }

    /**
     * Function to set wheel background
     *
     * @param wheelBackground Wheel background color
     */
    public void setWheelBackgoundWheel(int wheelBackground) {
        mWheelBackground = wheelBackground;
        invalidate();
    }

    /**
     * Function to set wheel listener
     *
     * @param onLuckyWheelReachTheTarget target reach listener
     */
    public void setWheelListener(OnLuckyWheelReachTheTarget onLuckyWheelReachTheTarget) {
        mOnLuckyWheelReachTheTarget = onLuckyWheelReachTheTarget;
    }

    /**
     * Function to add wheels items
     *
     * @param wheelItems Wheels model item
     */
    public void addWheelItems(List<WheelItem> wheelItems) {
        mWheelItems = wheelItems;
        invalidate();
    }

    /**
     * Function to draw wheel background
     *
     * @param canvas Canvas of draw
     */
    private void drawWheelBackground(Canvas canvas) {
        Paint backgroundPainter = new Paint();
        backgroundPainter.setAntiAlias(true);
        backgroundPainter.setDither(true);
        backgroundPainter.setColor(mWheelBackground);
        canvas.drawCircle(center, center, center, backgroundPainter);
    }

    private void initWheelStrokeRadius() {
        wheelStrokeRadius = wheelStrokeWidth / 2;
        wheelStrokeRadius = wheelStrokeRadius == 0 ? 1 : wheelStrokeRadius;
    }


    private void drawImage(Canvas canvas) {
        float cx = circle.getCx();
        float cy = circle.getCy();
        float radius = circle.getRadius();
        float x = cx - radius + (wheelStrokeRadius * 5);
        float y = cy;
        float textWidth = radius - (wheelStrokeRadius * 10);

        TextPaint textPaint = new TextPaint();
        textPaint.set(this.textPaint);

        float angle = getAnglePerItem() / 2;

        for (int i = 0; i < mWheelItems.size(); i++) {
            CharSequence item = TextUtils
                    .ellipsize(mWheelItems.get(i).str, textPaint, textWidth, TextUtils.TruncateAt.END);
            canvas.save();
            canvas.rotate(angle + 180, cx, cy); // +180 for start from right
            canvas.drawText(item.toString(), x, y, this.textPaint);
            canvas.restore();

            angle += getAnglePerItem();
        }

        // Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        // canvas.drawBitmap(rotatedBitmap, null, rect, null);
    }

    private int getItemSize() {
        return mWheelItems == null ? 0 : mWheelItems.size();
    }

    private float getAnglePerItem() {
        return ANGLE / (float) getItemSize();
    }

    /**
     * Function to rotate wheel to target
     *
     * @param target target number
     */
    public void rotateWheelToTarget(int target) {

        float wheelItemCenter = 270 - getAngleOfIndexTarget(target) + (360 / mWheelItems.size()) / 2;
        animate().setInterpolator(new DecelerateInterpolator())
                .setDuration(DEFAULT_ROTATION_TIME)
                .rotation((360 * 15) + wheelItemCenter)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (mOnLuckyWheelReachTheTarget != null) {
                            mOnLuckyWheelReachTheTarget.onReachTarget();
                        }
                        clearAnimation();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .start();
    }

    /**
     * Function to rotate to zero angle
     *
     * @param target target to reach
     */
    public void resetRotationLocationToZeroAngle(final int target) {
        animate().setDuration(0).rotation(0).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                rotateWheelToTarget(target);
                clearAnimation();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        try {
            if (circle == null) {
                initCircle();
            }

            drawWheelBackground(canvas);
            initComponents();

            float tempAngle = 0;
            float sweepAngle = 360 / mWheelItems.size();

            for (int i = 0; i < mWheelItems.size(); i++) {
                archPaint.setColor(mWheelItems.get(i).color);
                canvas.drawArc(range, tempAngle, sweepAngle, true, archPaint);
                drawImage(canvas);
                tempAngle += sweepAngle;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = Math.min(getMeasuredWidth(), getMeasuredHeight());
        padding = getPaddingLeft() == 0 ? DEFAULT_PADDING : getPaddingLeft();
        radius = width - padding * 2;
        center = width / 2;
        setMeasuredDimension(width, width);

    }
    public void setRound(int numberOfRound) {
        mRoundOfNumber = numberOfRound;
    }

    public void rotateTo(int index) {
        if (isRunning) {
            return;
        }
        mTargetIndex = index;
        setRotation(0);
        float targetAngle = 360 * mRoundOfNumber + 270 - getAngleOfIndexTarget() + (360 / mWheelItems.size()) / 2;
        animate()
                .setInterpolator(new DecelerateInterpolator())
                .setDuration(mRoundOfNumber * 1000 + 900L)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        isRunning = true;
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        isRunning = false;
                        if (mPieRotateListener != null) {
                            mPieRotateListener.rotateDone(mTargetIndex);
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {}

                    @Override
                    public void onAnimationRepeat(Animator animation) {}
                })
                .rotation(targetAngle)
                .start();
    }

    private float getAngleOfIndexTarget() {
        int tempIndex = mTargetIndex == 0 ? 1 : mTargetIndex;
        return (360 / mWheelItems.size()) * tempIndex;
    }
}
