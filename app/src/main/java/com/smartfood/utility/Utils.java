package com.smartfood.utility;

import android.app.Activity;
import android.content.Intent;

import com.smartfood.R;

public class Utils {
    public final static int THEME_DEFAULT = 1;
    public final static int THEME_GREEN = 2;
    private static int sTheme;

    public static void changeToTheme(Activity activity, int theme) {

        sTheme = theme;
        activity.startActivity(new Intent(activity, activity.getClass()));
        activity.finish();
    }

    /**
     * Set the theme of the activity, according to the configuration.
     */
    public static void onActivityCreateSetTheme(Activity activity,int i) {
        switch (i) {
            default:
            case THEME_DEFAULT:
                activity.setTheme(R.style.AppTheme_Base_Yellow);
                break;
            case THEME_GREEN:
                activity.setTheme(R.style.AppTheme_Base_Green);
                break;
        }
    }
}