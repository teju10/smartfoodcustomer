package com.smartfood.utility;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

/**
 * Created by Nilesh Kansal on 01-Aug-16.
 */
public class ResponseTask extends AsyncTask<String, String, String> {
    String result = "", action = "";
    Context mContext;
    String TAG = "", method = "";
    JSONObject params;
    Fragment fragment;
    Activity activity;
    ResponseListener mListener;

    public ResponseTask(Activity activity, JSONObject parameters, String URL_ACTION, String TAG, String METHOD) {
        this.activity = activity;
        this.params = parameters;
        this.action = URL_ACTION;
        this.TAG = TAG;
        this.method = METHOD;
    }

    public ResponseTask(Activity activity, String URL_ACTION, String TAG, String METHOD) {
        this.activity = activity;
        this.action = URL_ACTION;
        this.TAG = TAG;
        this.method = METHOD;
    }

    public ResponseTask(Context context, JSONObject parameters, String URL_ACTION, String TAG, String METHOD) {
        this.mContext = context;
        this.params = parameters;
        this.action = URL_ACTION;
        this.TAG = TAG;
        this.method = METHOD;
    }

    public ResponseTask(Context context, String URL_ACTION, String TAG, String METHOD) {
        this.mContext = context;
        this.action = URL_ACTION;
        this.TAG = TAG;
        this.method = METHOD;
    }

    public ResponseTask(Fragment fragment, JSONObject parameters, String URL_ACTION, String TAG, String METHOD) {
        this.fragment = fragment;
        this.params = parameters;
        this.action = URL_ACTION;
        this.TAG = TAG;
        this.method = METHOD;
    }

    public ResponseTask(Fragment fragment, String URL_ACTION, String TAG, String METHOD) {
        this.fragment = fragment;
        this.action = URL_ACTION;
        this.TAG = TAG;
        this.method = METHOD;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        if (method.equalsIgnoreCase("GET")) {
            result = FindJSONFromUrl(Constants.SERVER_URL + action);
        } else {
            result = PostParamsAndFindJson(Constants.SERVER_URL + action, params);
        }
        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.e(TAG, "result ===== " + s);
        try {
            returnResult(s);
        } catch (Exception e) {
            e.printStackTrace();
            returnResult(null);
            //returnResult(e);
        }
    }

    public void setListener(ResponseListener listener) {
        mListener = listener;
    }

    private void returnResult(String result) {
        if (mListener != null) {
            mListener.onPickSuccess(result);
            /*if (result != null) {
                mListener.onPickSuccess(result);
            } else {
                if (result == null) {
                    mListener.onPickError(new NullPointerException());
                }
            }*/
        }
    }

    /*private void returnResult(Exception e) {
        if (mListener != null) {
            mListener.onPickError(e);
        }
    }*/

    public String PostParamsAndFindJson(String targetURL, JSONObject params) {
        StringBuffer response = new StringBuffer();
        System.out.println("URL comes in json parser class is:  " + targetURL + " " + params);
        try {
            URL myURL = new URL(targetURL);
            HttpURLConnection myURLConnection = (HttpURLConnection) (myURL).openConnection();
            myURLConnection.setReadTimeout(30000); /* milliseconds */
            myURLConnection.setConnectTimeout(30000); /* milliseconds */
            myURLConnection.setRequestMethod("POST");
            myURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            myURLConnection.setUseCaches(false);
            myURLConnection.setDoInput(true);
            myURLConnection.setDoOutput(true);
            OutputStream os = myURLConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(params));
            writer.flush();
            writer.close();
            os.close();
            myURLConnection.connect();
            int responseStatus = myURLConnection.getResponseCode();
            System.out.println("status in json parser class ........" + responseStatus);
            BufferedReader in = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            System.out.println("result in json parser class ........" + response.toString());
            return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        Iterator<String> itr = params.keys();
        while (itr.hasNext()) {
            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    public String FindJSONFromUrl(String targetURL) {
        StringBuffer response = new StringBuffer();

        System.out.println("URL comes in json parser class is ========= " + targetURL);
        try {
            URL myURL = new URL(targetURL);
            HttpURLConnection myURLConnection = (HttpURLConnection) (myURL).openConnection();
            myURLConnection.setReadTimeout(10000); /* milliseconds */
            myURLConnection.setConnectTimeout(10000); /* milliseconds */
            myURLConnection.setRequestMethod("GET");
            myURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            myURLConnection.setUseCaches(false);
            myURLConnection.setDoInput(true);
            myURLConnection.setDoOutput(true);
            myURLConnection.connect();
            int responseStatus = myURLConnection.getResponseCode();
            System.out.println("status in json parser class ........" + responseStatus);
            BufferedReader in = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            System.out.println("result in json parser class ........" + response.toString());
            return response.toString();
        } catch (Exception e) {
            System.out.println("exception in jsonparser class ........");
            e.printStackTrace();
            return null;
        }
    }


}