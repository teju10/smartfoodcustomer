package com.smartfood.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.smartfood.R;
import com.smartfood.customwidget.CircleImageView;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.model.OrderItem;

import java.util.ArrayList;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder> {

        private Context context;
        private ArrayList<OrderItem> listMenu;
        int i = 0;

        public MenuAdapter(Context context, ArrayList<OrderItem> listMenu, int i) {
            this.context = context;
            this.listMenu = listMenu;
            this.i = i;
        }

        @Override
        public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (i==1){
                return new MenuViewHolder(LayoutInflater.from(context).inflate(R.layout.item_checkout_menu, parent, false));
            }else{
                return new MenuViewHolder(LayoutInflater.from(context).inflate(R.layout.item_menu, parent, false));
            }
        }

        @Override
        public void onBindViewHolder(MenuViewHolder holder, int position) {

            String item_price = listMenu.get(position).getPrice()+context.getString(R.string.currency);
            String total_price = listMenu.get(position).getSubTotal()+context.getString(R.string.currency);
            String quant =context.getString(R.string.qty)+" "+ listMenu.get(position).getQty();
            holder.item_price.setText(item_price);
            holder.quantity.setText(quant);
            holder.total_price.setText(total_price);
            holder.description.setText(listMenu.get(position).getTitle());
            holder.menu_name.setText(listMenu.get(position).getItem_name());

            if (!listMenu.get(position).getExtras().equals("")){
                holder.extras.setVisibility(View.VISIBLE);
                holder.extras.setText(listMenu.get(position).getExtras());
            }else{
                holder.extras.setVisibility(View.GONE);
            }

            Glide.with(context).load(listMenu.get(position).getImg()).into(holder.ic_menu);
        }

        @Override
        public int getItemCount() {
            return listMenu.size();
        }


        class MenuViewHolder extends RecyclerView.ViewHolder  {


            CustomTextView menu_name, description, quantity, item_price, extras, total_price;
            CircleImageView ic_menu;

            MenuViewHolder(View itemView) {
                super(itemView);

                item_price= (CustomTextView)itemView.findViewById(R.id.item_price);
                quantity= (CustomTextView)itemView.findViewById(R.id.quantity);
                description= (CustomTextView)itemView.findViewById(R.id.description);
                menu_name= (CustomTextView)itemView.findViewById(R.id.menu_name);
                extras= (CustomTextView)itemView.findViewById(R.id.extras);
                total_price= (CustomTextView)itemView.findViewById(R.id.total_price);
                ic_menu= (CircleImageView) itemView.findViewById(R.id.ic_menu);

            }

        }
    }

