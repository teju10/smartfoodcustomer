package com.smartfood.adapter;

import android.content.Context;
import android.support.v13.app.ActivityCompat;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.smartfood.Cursor_WheelLayout.CursorWheelLayout;
import com.smartfood.Cursor_WheelLayout.MenuItemData;
import com.smartfood.R;

import java.util.List;

/**
 * Created by and-04 on 14/2/18.
 */

public class SimpleTextAdapter1  extends CursorWheelLayout.CycleWheelAdapter {
    private List<MenuItemData> mMenuItemDatas;
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    public static final int INDEX_SPEC = 9;
    private int mGravity;

    public SimpleTextAdapter1(Context context, List<MenuItemData> menuItemDatas) {
        this(context, menuItemDatas, Gravity.CENTER);
    }

    public SimpleTextAdapter1(Context context, List<MenuItemData> menuItemDatas, int gravity) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mMenuItemDatas = menuItemDatas;
        mGravity = gravity;
    }

    @Override
    public int getCount() {
        return mMenuItemDatas == null ? 0 : mMenuItemDatas.size();
    }

    @Override
    public View getView(View parent, int position) {
        MenuItemData item = getItem(position);
        View root = mLayoutInflater.inflate(R.layout.wheel_menu_item, null, false);
        TextView textView = (TextView) root.findViewById(R.id.wheel_menu_item_tv);
        textView.setVisibility(View.VISIBLE);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        textView.setText(item.mTitle);
        if (textView.getLayoutParams() instanceof FrameLayout.LayoutParams) {
            ((FrameLayout.LayoutParams) textView.getLayoutParams()).gravity = mGravity;
        }
        if (position == INDEX_SPEC) {
            textView.setTextColor(ActivityCompat.getColor(mContext, R.color.mdtp_red));
        }
        return root;
    }

    @Override
    public MenuItemData getItem(int position) {
        return mMenuItemDatas.get(position);
    }
}
