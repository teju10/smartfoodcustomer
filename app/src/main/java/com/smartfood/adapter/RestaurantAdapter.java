package com.smartfood.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smartfood.R;
import com.smartfood.activities.RestaurantDetailActivity;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.utility.Constants;
import com.smartfood.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 13/11/17.
 */

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.MainViewHolder> {

    private Context context;
    private ArrayList<JSONObject> listRestro;
    private final int EMPTY_TYPE =0;
    private final int Main_TYPE =1;

    public RestaurantAdapter(Context context, ArrayList<JSONObject> listRestro) {
        this.context = context;
        this.listRestro = listRestro;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == EMPTY_TYPE){
            return new EmptyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_empty_textview, parent, false));

        }else{
            return new RestaurantViewHolder(LayoutInflater.from(context).inflate(R.layout.item_restaurant, parent, false));

        }

    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {

        if (holder instanceof EmptyViewHolder){
            EmptyViewHolder vh = (EmptyViewHolder)holder;
            vh.textView.setText(R.string.no_restaurant_availabe);

        }else {
            RestaurantViewHolder _vh = (RestaurantViewHolder) holder;

            try {
                JSONObject object = listRestro.get(position);
                String review_count = "";

                if (!object.getString(Constants.COUNT_REVIEW).equals("0"))
                    review_count = object.getString(Constants.COUNT_REVIEW) + " " + context.getString(R.string.reviews);

                if (object.getString(Constants.OPEN_NOW).equals("1"))
                    _vh.is_open.setText(R.string.open_now);
                else
                    _vh.is_open.setText(R.string.closed_now);


                if (object.getString(Constants.DISTANCE).equals(""))
                    _vh.distance.setText(R.string.n_a);
                else
                    _vh.distance.setText(object.getString(Constants.DISTANCE));

                _vh.restaurant_name.setText(object.getString(Constants.RESTRO_NAME));
                _vh.tag_line.setText(object.getString(Constants.RESTRO_TITLE));
                _vh.open_time.setText(object.getString(Constants.RESTRO_TIME));
                _vh.review_count.setText(review_count);

                Glide.with(context).load(object.getString(Constants.RESTRO_IMAGE)).into(_vh.restro_image);
                Glide.with(context).load(object.getString(Constants.LOGO_IMAGE)).into(_vh.restro_logo);

                Utility.Rating(Float.parseFloat(object.getString(Constants.AVG_RATING)),
                        _vh.star1, _vh.star2, _vh.star3, _vh.star4,
                        _vh.star5, R.drawable.ic_star_y_empty, R.drawable.ic_star_y_half,
                        R.drawable.ic_star_y_filled);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        if (listRestro.size() == 0){
            return listRestro.size()+1;
        }else{
            return listRestro.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (listRestro.size() == 0){
            return EMPTY_TYPE;
        }else{
            return Main_TYPE;
        }
    }



    class MainViewHolder extends RecyclerView.ViewHolder{

        MainViewHolder(View itemView) {
            super(itemView);
        }
    }


    class RestaurantViewHolder extends MainViewHolder implements View.OnClickListener {

        ImageView restro_image, restro_logo, star1, star2, star3, star4, star5;
        CustomTextView restaurant_name, distance, tag_line,
                review_count, is_open, open_time;

        RestaurantViewHolder(View itemView) {
            super(itemView);
            restro_image = (ImageView)itemView.findViewById(R.id.restro_image);
            restro_logo = (ImageView)itemView.findViewById(R.id.restro_logo);
            star1 = (ImageView)itemView.findViewById(R.id.star1);
            star2 = (ImageView)itemView.findViewById(R.id.star2);
            star3 = (ImageView)itemView.findViewById(R.id.star3);
            star4 = (ImageView)itemView.findViewById(R.id.star4);
            star5 = (ImageView)itemView.findViewById(R.id.star5);
            restaurant_name = (CustomTextView)itemView.findViewById(R.id.restaurant_name);
            distance = (CustomTextView)itemView.findViewById(R.id.distance);
            tag_line = (CustomTextView)itemView.findViewById(R.id.tag_line);
            review_count = (CustomTextView)itemView.findViewById(R.id.review_count);
            is_open = (CustomTextView)itemView.findViewById(R.id.is_open);
            open_time = (CustomTextView)itemView.findViewById(R.id.open_time);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            JSONObject object = listRestro.get(getAdapterPosition());
            context.startActivity(new Intent(context , RestaurantDetailActivity.class)
                    .putExtra(Constants.OBJECT , object.toString()));

            Utility.activityTransition(context);
        }
    }

    public class EmptyViewHolder extends MainViewHolder {

        CustomTextView textView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            textView = (CustomTextView)itemView.findViewById(R.id.text);
        }
    }
}
