package com.smartfood.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.smartfood.R;
import com.smartfood.activities.MapAddressActivity;
import com.smartfood.activities.MyAddressListActivity;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 13/12/17.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.AddressViewHolder> {

    private Context context;
    private ArrayList<JSONObject> listAddress;
    private ProgressHUD progresshud;
    int selectedPos = -1;
    String address_id;

    public AddressAdapter(Context context, ArrayList<JSONObject> listAddress, String address_id) {
        this.context = context;
        this.listAddress = listAddress;
        this.address_id = address_id;
    }

    @Override
    public AddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AddressViewHolder(LayoutInflater.from(context).inflate(R.layout.item_address, parent, false));
    }

    @Override
    public void onBindViewHolder(AddressViewHolder holder, final int position) {
        try {
            holder.app_no.setText(listAddress.get(position).getString(Constants.APPARTMENT_NO));
            holder.floor.setText(listAddress.get(position).getString(Constants.FLOOR));
            holder.building.setText(listAddress.get(position).getString(Constants.BUILDING));
            holder.street.setText(listAddress.get(position).getString(Constants.STREET));
            holder.address_type.setText(listAddress.get(position).getString(Constants.MARKS));
            holder.address.setText(listAddress.get(position).getString(Constants.ADDRESS));
            holder.other_address_note.setText(listAddress.get(position).getString(Constants.OTHER_NOTE));

            if (listAddress.get(position).getString(Constants.ID).equals(address_id)){
                holder.right_icon.setVisibility(View.VISIBLE);
//                holder.row_id.setBackground(context.getResources().getDrawable(R.drawable.selector_item_address));
            }else{
              holder.right_icon.setVisibility(View.GONE);
//                holder.row_id.setBackground(context.getResources().getDrawable(R.color.colorWhite));

            }

           /* holder.row_id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   *//* row_index =position;
                    notifyDataSetChanged();*//*
                    *//*if (position == selectedPos) {
                        view.setBackgroundColor((view.getResources()
                                .getColor(R.color.colorLightYellow)));
                    } else {
                        view.setBackgroundColor((view.getResources()
                                .getColor(R.color.colorWhite)));
                    }*//*
                }
            });*/

            /*if(row_index ==position){
                holder.row_id.setBackgroundColor(Color.parseColor("#567845"));
            }
            else
            {
                holder.row_id.setBackgroundColor(Color.parseColor("#ffffff"));
            }
*/


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return listAddress.size();
    }


    class AddressViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CustomTextView address_type, street, building, floor, app_no, address,other_address_note;
        ImageView btn_delete, btn_edit,right_icon;
        LinearLayout row_id;

        AddressViewHolder(View itemView) {
            super(itemView);
            app_no = (CustomTextView) itemView.findViewById(R.id.app_no);
            floor = (CustomTextView) itemView.findViewById(R.id.floor);
            building = (CustomTextView) itemView.findViewById(R.id.building);
            street = (CustomTextView) itemView.findViewById(R.id.street);
            other_address_note = (CustomTextView) itemView.findViewById(R.id.other_address_note);
            address_type = (CustomTextView) itemView.findViewById(R.id.address_type);
            address = (CustomTextView) itemView.findViewById(R.id.address);
            btn_edit = (ImageView) itemView.findViewById(R.id.btn_edit);
            btn_delete = (ImageView) itemView.findViewById(R.id.btn_delete);
            right_icon = (ImageView) itemView.findViewById(R.id.right_icon);
            row_id = (LinearLayout) itemView.findViewById(R.id.row_id);

            btn_delete.setOnClickListener(this);
            btn_edit.setOnClickListener(this);

            if (context instanceof MyAddressListActivity) {
                itemView.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_edit:
                    try {
                        context.startActivity(new Intent(context, MapAddressActivity.class)
                                .putExtra(Constants.OBJECT, listAddress.get(getAdapterPosition()).toString())
                                .putExtra(Constants.IS_UPDATE, "1"));
                        Utility.activityTransition(context);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                case R.id.btn_delete:
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
                    } else {
                        builder = new AlertDialog.Builder(context);
                    }
                    builder.setMessage(R.string.are_you_sure_to_delete_address)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        serverDeleteAddress(getAdapterPosition(),
                                                listAddress.get(getAdapterPosition()).getString(Constants.ID));

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();


                    break;
                default:
                    try {
                        Intent intent = new Intent();
                        intent.putExtra(Constants.ADDRESS_ID, listAddress.get(getAdapterPosition()).getString(Constants.ID));
                        intent.putExtra(Constants.LATITUDE, listAddress.get(getAdapterPosition()).getString(Constants.LATITUDE));
                        intent.putExtra(Constants.LONGITUDE, listAddress.get(getAdapterPosition()).getString(Constants.LONGITUDE));
                        intent.putExtra(Constants.MARKS, context.getResources().getString(R.string.change_loc));



                        ((Activity) context).setResult(Activity.RESULT_OK, intent);
                        ((Activity) context).finish();
//                        view.setBackgroundResource(R.color.accent_green);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
            }
        }
    }

    private void serverDeleteAddress(final int adapterPosition, String id) {


        if (Utility.isConnectingToInternet(context)) {
            progresshud = ProgressHUD.show(context, context.getString(R.string.loading), true, false, null);
            try {
                final JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
                jsonObject.put(Constants.ADDRESS_ID, id);

                ResponseTask responseTask = new ResponseTask(context, jsonObject, Constants.REMOVE_ADDRESS, "AddressAdapter", "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progresshud != null && progresshud.isShowing()) {
                            progresshud.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, context.getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                    listAddress.remove(adapterPosition);
                                    notifyDataSetChanged();

                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utility.ShowToastMessage(context, context.getString(R.string.not_connected_to_internet));
        }


    }
}

