package com.smartfood.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.smartfood.R;
import com.smartfood.activities.MenuDetailsActivity;
import com.smartfood.customwidget.CircleImageView;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.customwidget.ProgressHUD;
import com.smartfood.model.PriceCal;
import com.smartfood.utility.Constants;
import com.smartfood.utility.ResponseListener;
import com.smartfood.utility.ResponseTask;
import com.smartfood.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by and-05 on 20/11/17.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MainViewHolder> {

    private ArrayList<JSONObject> cartList;
    private Context context;
    private final int EMPTY_TYPE =0;
    private final int MAIN_TYPE =1;
    private ProgressHUD progresshud;
    private final String TAG = CartAdapter.class.getSimpleName();
    private LinearLayout ln_total;
    private CustomTextView tv_price;

    public CartAdapter(Context context, ArrayList<JSONObject> cartList, LinearLayout ln_total, CustomTextView tv_price) {
        this.context = context;
        this.cartList = cartList;
        this.ln_total = ln_total;
        this.tv_price = tv_price;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == EMPTY_TYPE){
            return new EmptyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_empty_textview, parent, false));

        }else{
            return new CartViewHolder(LayoutInflater.from(context).inflate(R.layout.item_cart, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {

        if (holder instanceof EmptyViewHolder){
            EmptyViewHolder vh = (EmptyViewHolder)holder;
            vh.textView.setText(R.string.cart_is_empty);

        }else if (holder instanceof CartViewHolder){
            CartViewHolder vh = (CartViewHolder)holder;

            try {
                String item_price = cartList.get(position).getString(Constants.TOTAL)+" "+context.getString(R.string.currency);
                String quant =context.getString(R.string.qty)+" "+ cartList.get(position).getString(Constants.MENU_QTY);
                String total_price = cartList.get(position).getString(Constants.SUBTOTAL)+" "+context.getString(R.string.currency);
                vh.item_price.setText(item_price);
                vh.quantity.setText(quant);
                vh.total_price.setText(total_price);
                vh.description.setText(cartList.get(position).getString(Constants.MENU_TITLE));
                vh.menu_name.setText(cartList.get(position).getString(Constants.MENU_NAME));

                Glide.with(context).load(cartList.get(position).getString(Constants.MENU_IMAGE)).into(vh.ic_menu);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    public int getItemCount() {
        if (cartList.size() == 0){
            return cartList.size()+1;
        }else{
            return cartList.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (cartList.size() == 0){
            return EMPTY_TYPE;
        }else{
            return MAIN_TYPE;
        }
    }

    public class MainViewHolder extends RecyclerView.ViewHolder {
        public MainViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class CartViewHolder extends MainViewHolder implements View.OnClickListener{

        CustomTextView menu_name, description, total_price, quantity, item_price;
        ImageView ic_delete;
        CircleImageView ic_menu;

        public CartViewHolder(View itemView) {
            super(itemView);
            item_price= (CustomTextView)itemView.findViewById(R.id.item_price);
            quantity= (CustomTextView)itemView.findViewById(R.id.quantity);
            total_price= (CustomTextView)itemView.findViewById(R.id.total_price);
            description= (CustomTextView)itemView.findViewById(R.id.description);
            menu_name= (CustomTextView)itemView.findViewById(R.id.menu_name);
            ic_delete= (ImageView) itemView.findViewById(R.id.ic_delete);
            ic_menu= (CircleImageView) itemView.findViewById(R.id.ic_menu);

            ic_delete.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.ic_delete:
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
                    } else {
                        builder = new AlertDialog.Builder(context);
                    }
                    builder.setMessage(R.string.are_you_sure_to_delete)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        serverDeleteMenu(getAdapterPosition(), cartList.get(getAdapterPosition()).getString(Constants.MENU_ID));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                    break;
                    default:
                        try {
                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.RESTAURANT_ID,cartList.get(getAdapterPosition()).getString(Constants.RESTAURANT_ID));
                            bundle.putString(Constants.MENU_ID,cartList.get(getAdapterPosition()).getString(Constants.MENU_ID));
                            bundle.putString(Constants.MENU_NAME,cartList.get(getAdapterPosition()).getString(Constants.MENU_NAME));
                            bundle.putString(Constants.MENU_TITLE,cartList.get(getAdapterPosition()).getString(Constants.MENU_TITLE));
                            bundle.putString(Constants.MENU_PRICE,cartList.get(getAdapterPosition()).getString(Constants.MENU_PRICE));
                            bundle.putString(Constants.MENU_IMAGE,cartList.get(getAdapterPosition()).getString(Constants.MENU_IMAGE));
                            bundle.putString(Constants.DISCOUNT_PRICE,cartList.get(getAdapterPosition()).getString(Constants.DISCOUNT_PRICE));
                            bundle.putString(Constants.IS_UPDATE, "1" );

                            bundle.putString(Constants.QUANTITY, cartList.get(getAdapterPosition()).getString(Constants.MENU_QTY));
                            bundle.putString(Constants.SPECIAL_REQ, cartList.get(getAdapterPosition()).getString(Constants.SPECIAL_REQ));

                            ArrayList<PriceCal> priceList = new ArrayList<>();
                            JSONArray jsonArray = cartList.get(getAdapterPosition()).getJSONArray(Constants.EXTRA);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                priceList.add(new PriceCal(object.getString(Constants.ID), object.getString(Constants.PRICE)));
                            }

                            context.startActivity(new Intent(context , MenuDetailsActivity.class)
                                    .putExtra(Constants.MENU_LIST, (ArrayList<PriceCal>) priceList).putExtras(bundle));
                            Utility.activityTransition(context);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

            }
        }
    }

    public class EmptyViewHolder extends MainViewHolder{

        CustomTextView textView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            textView = (CustomTextView)itemView.findViewById(R.id.text);
        }
    }


    private void serverDeleteMenu(final int adapterPosition, String product_id) {


        if (Utility.isConnectingToInternet(context)) {
            progresshud = ProgressHUD.show(context, context.getString(R.string.loading),true, false, null);
            try {
                final JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.USER_ID, Utility.getSharedPreferences(context, Constants.USER_ID));
                jsonObject.put(Constants.PRODUCT_ID, product_id);

                ResponseTask responseTask = new ResponseTask(context, jsonObject, Constants.REMOVE_TO_CART, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progresshud != null && progresshud.isShowing()) {
                            progresshud.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(context, context.getString(R.string.server_not_responding));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString(Constants.SUCCESS).equalsIgnoreCase("1")) {

                                  cartList.remove(adapterPosition);
                                  notifyDataSetChanged();

                                  if (cartList.size()==0){
                                      ln_total.setVisibility(View.GONE);
                                  }else{
                                      String price = json.getJSONObject(Constants.OBJECT).getString(Constants.GRAND_TOTAL)
                                              +" "+context.getString(R.string.currency);

                                      tv_price.setText(price);
                                  }



                                    Utility.setSharedPreference(context,Constants.CART_COUNT, json.getJSONObject(Constants.OBJECT).getInt(Constants.CART_COUNT));

                                } else {
                                    Utility.ShowToastMessage(context, json.getString(Constants.MSG));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Utility.ShowToastMessage(context , context.getString(R.string.not_connected_to_internet));
        }


    }

}
