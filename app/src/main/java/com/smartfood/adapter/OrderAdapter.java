package com.smartfood.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smartfood.R;
import com.smartfood.activities.OrderDetailsActivity;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.model.OrderParent;
import com.smartfood.utility.Constants;
import com.smartfood.utility.Utility;

import java.util.ArrayList;

/**
 * Created by and-05 on 5/12/17.
 */

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MainViewHolder> {

    private ArrayList<OrderParent> orderList;
    private Context context;
    private final int EMPTY_TYPE =0;

    public OrderAdapter(Context context, ArrayList<OrderParent> list) {
        this.context = context;
        this.orderList = list;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == EMPTY_TYPE){
            return new EmptyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_empty_textview, parent, false));

        }else{
            return new OrderViewHolder(LayoutInflater.from(context).inflate(R.layout.item_my_order, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        if (holder instanceof EmptyViewHolder){
            EmptyViewHolder vh = (EmptyViewHolder)holder;
            vh.textView.setText(R.string.there_are_no_orders);

        }else if (holder instanceof OrderViewHolder){
            OrderViewHolder viewHolder = (OrderViewHolder)holder;
            String date = context.getString(R.string.date)+" "+orderList.get(position).getDate();
            String order_no = context.getString(R.string.order_no)+" "+orderList.get(position).getOrder_no();
            String total = context.getString(R.string.total_is)+" "+orderList.get(position).getTotal()+""+
                    context.getString(R.string.currency)+" + "+orderList.get(position).getDelivery_charges()+
                    context.getString(R.string.currency);
            viewHolder.order_no.setText(order_no);
            viewHolder.order_date.setText(date);
            viewHolder.total_price.setText(total);
            viewHolder.restro_name.setText(orderList.get(position).getRestro_name());

            Glide.with(context).load(orderList.get(position).getImage()).into(viewHolder.restro_logo);

            viewHolder.rv_item.setAdapter(new OrderItemAdapter(context ,
                    orderList.get(position).getListItem(), orderList.get(position) ));

            String status="";
            if (orderList.get(position).getStatus().equals("0")){
                status = context.getString(R.string.status)+" "+context.getString(R.string.pending);
                viewHolder.status.setText(status);
                viewHolder.status.setBackgroundResource(R.drawable.z_back_pending);
                viewHolder.btn_track.setVisibility(View.GONE);

            }else  if (orderList.get(position).getStatus().equals("1")) {
                status = context.getString(R.string.status)+" "+context.getString(R.string.in_progress);
                viewHolder.status.setText(status);
                viewHolder.status.setBackgroundResource(R.drawable.z_back_inprogress);
                viewHolder.btn_track.setVisibility(View.GONE);

            }else  if (orderList.get(position).getStatus().equals("2")){
                status = context.getString(R.string.status)+" "+context.getString(R.string.on_the_way);
                viewHolder.status.setText(status);
                viewHolder.btn_track.setVisibility(View.VISIBLE);
                viewHolder.status.setBackgroundResource(R.drawable.z_rounded_blue);
            }else if (orderList.get(position).getStatus().equals("3")){
                status = context.getString(R.string.status)+" "+context.getString(R.string.completed);
                viewHolder.status.setText(status);
                viewHolder.status.setBackgroundResource(R.drawable.z_back_completed);
                viewHolder.btn_track.setVisibility(View.GONE);

            }else if (orderList.get(position).getStatus().equals("4")){
                status = context.getString(R.string.status)+" "+context.getString(R.string.rejected);
                viewHolder.status.setText(status);
                viewHolder.status.setBackgroundResource(R.drawable.z_back_rejected);
                viewHolder.btn_track.setVisibility(View.GONE);
            }else if (orderList.get(position).getStatus().equals("5")){
                status = context.getString(R.string.status)+" "+context.getString(R.string.cancelled);
                viewHolder.status.setText(status);
                viewHolder.status.setBackgroundResource(R.drawable.z_back_rejected);
                viewHolder.btn_track.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public int getItemCount() {
        if (orderList.size() == 0){
            return orderList.size()+1;
        }else{
            return orderList.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (orderList.size() == 0){
            return EMPTY_TYPE;
        }else{
            return 1;
        }
    }


    class MainViewHolder extends RecyclerView.ViewHolder {
        MainViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class OrderViewHolder extends MainViewHolder implements View.OnClickListener{

        CustomTextView order_date, order_no, restro_name, status, total_price, btn_track;
        RecyclerView rv_item;
        ImageView restro_logo;

        OrderViewHolder(View itemView) {
            super(itemView);
            total_price = (CustomTextView)itemView.findViewById(R.id.total_price);
            status = (CustomTextView)itemView.findViewById(R.id.status);
            restro_name = (CustomTextView)itemView.findViewById(R.id.restro_name);
            order_no = (CustomTextView)itemView.findViewById(R.id.order_no);
            order_date = (CustomTextView)itemView.findViewById(R.id.order_date);
            btn_track = (CustomTextView)itemView.findViewById(R.id.btn_track);
            rv_item = (RecyclerView)itemView.findViewById(R.id.rv_menu);
            restro_logo = (ImageView)itemView.findViewById(R.id.restro_logo);

            rv_item.setLayoutManager(new LinearLayoutManager(context , LinearLayoutManager.HORIZONTAL, false));
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            OrderParent orderParent = orderList.get(getAdapterPosition());
            context.startActivity(new Intent(context , OrderDetailsActivity.class)
                .putExtra(Constants.ORDER_ID, orderParent.getOrder_id()));
            Utility.activityTransition(context);
        }
    }

    public class EmptyViewHolder extends MainViewHolder {

        CustomTextView textView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            textView = (CustomTextView)itemView.findViewById(R.id.text);
        }
    }
}
