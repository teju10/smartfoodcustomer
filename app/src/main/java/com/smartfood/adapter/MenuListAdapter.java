package com.smartfood.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smartfood.R;
import com.smartfood.activities.MenuDetailsActivity;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.model.MenuItem;
import com.smartfood.model.MenuList;
import com.smartfood.utility.Constants;
import com.smartfood.utility.Utility;

import java.util.ArrayList;

public class MenuListAdapter extends BaseExpandableListAdapter {
 
    private Context _context;

    private ArrayList<MenuItem> listMenuItem = new ArrayList<>();
    private ArrayList<MenuList> listMenu = new ArrayList<>();
    private String restro_id;

    public MenuListAdapter(Context context, ArrayList<MenuList> listMenu, ArrayList<MenuItem> listMenuItem, String restro_id) {

        this._context = context;
        this.listMenu = listMenu;
        this.listMenuItem = listMenuItem;
        this.restro_id = restro_id;

    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return listMenu.get(groupPosition).getList().get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
 
        final MenuItem childText = (MenuItem) getChild(groupPosition, childPosition);
 
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_menu_child, null);
        }

        CustomTextView menu_name = (CustomTextView)convertView.findViewById(R.id.menu_name);
        CustomTextView menu_title = (CustomTextView)convertView.findViewById(R.id.menu_title);
        CustomTextView price = (CustomTextView)convertView.findViewById(R.id.price);
        CustomTextView dis_price = (CustomTextView)convertView.findViewById(R.id.dis_price);
        ImageView img_price = (ImageView) convertView.findViewById(R.id.img_menu);

        String str_price = childText.getMenu_price()+" "+_context.getString(R.string.currency);
        String str_off_price = childText.getMenu_off_price()+" "+_context.getString(R.string.currency);

        if (str_price.equals(str_off_price)){
            dis_price.setText(str_off_price);
            price.setText("");
        }else{
            price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            dis_price.setText(str_off_price);
            price.setText(str_price);
        }


        menu_name.setText(childText.getMenu_name());
        menu_title.setText(childText.getMenu_title());
        Glide.with(_context).load(childText.getMenu_image()).into(img_price);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.RESTAURANT_ID,restro_id);
                bundle.putString(Constants.MENU_ID,childText.getMenu_id());
                bundle.putString(Constants.MENU_NAME,childText.getMenu_name());
                bundle.putString(Constants.MENU_TITLE,childText.getMenu_title());
                bundle.putString(Constants.MENU_PRICE,childText.getMenu_price());
                bundle.putString(Constants.MENU_IMAGE,childText.getMenu_image());
                bundle.putString(Constants.DISCOUNT_PRICE,childText.getMenu_off_price());
                bundle.putString(Constants.IS_UPDATE,"");
                _context.startActivity(new Intent(_context , MenuDetailsActivity.class)
                                .putExtras(bundle));
                Utility.activityTransition(_context);

            }
        });

        return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
        int size=0;
        if(listMenu.get(groupPosition).getList()!=null)
            size = listMenu.get(groupPosition).getList().size();
        return size;
    }
 
    @Override
    public Object getGroup(int groupPosition) {
        return this.listMenu.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this.listMenu.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
//        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_menu_parent, null);
        }

        CustomTextView cat_name = (CustomTextView)convertView.findViewById(R.id.cat_name);
        cat_name.setText(listMenu.get(groupPosition).getCat_name());

        if (listMenu.get(groupPosition).getList().size()>0){
            ((ImageView) convertView.findViewById(R.id.arrow)).setVisibility(View.VISIBLE);
        }else{
            ((ImageView) convertView.findViewById(R.id.arrow)).setVisibility(View.GONE);
        }

        ((ImageView) convertView.findViewById(R.id.arrow))
                .setBackgroundResource(isExpanded?R.drawable.ic_arrow_up:R.drawable.ic_arrow_down);

        return convertView;
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);

    }


    @Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}