package com.smartfood.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.smartfood.R;
import com.smartfood.activities.OrderDetailsActivity;
import com.smartfood.customwidget.CircleImageView;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.model.OrderParent;
import com.smartfood.utility.Constants;
import com.smartfood.utility.Utility;

import java.util.ArrayList;

/**
 * Created by and-05 on 9/11/17.
 */

public class HomeOrderAdapter extends RecyclerView.Adapter<HomeOrderAdapter.MainViewHolder> {

    private ArrayList<OrderParent> orderList;
    private Context context;
    private final int EMPTY_TYPE =0;

    public HomeOrderAdapter(Context context, ArrayList<OrderParent> orderList) {
        this.context = context;
        this.orderList = orderList;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == EMPTY_TYPE){
            return new EmptyViewHolder(LayoutInflater.from(context).inflate(R.layout.item_empty_textview, parent, false));

        }else{
            return new OrderViewHolder(LayoutInflater.from(context).inflate(R.layout.item_home_order, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {

        if (holder instanceof EmptyViewHolder){
            EmptyViewHolder vh = (EmptyViewHolder)holder;
            vh.textView.setText(R.string.there_are_no_orders);

        }else if (holder instanceof OrderViewHolder){
            OrderViewHolder vh = (OrderViewHolder)holder;

            String date = context.getString(R.string.date)+" "+orderList.get(position).getDate();
            String order_no = context.getString(R.string.order_no)+" "+orderList.get(position).getOrder_no();

            vh.restro_name.setText(orderList.get(position).getRestro_name());
            vh.date.setText(date);
            vh.order_no.setText(order_no);

            Glide.with(context).load(orderList.get(position).getImage()).into(vh.ic_menu);

            if (orderList.get(position).getStatus().equals("0")){
                vh.status.setText(R.string.order_is_pending);
            }else  if (orderList.get(position).getStatus().equals("1")) {
                vh.status.setText(R.string.in_progress);
            }else  if (orderList.get(position).getStatus().equals("2")){
                vh.status.setText(R.string.on_the_way_track_order);
            }else if (orderList.get(position).getStatus().equals("3")){
                vh.status.setText(R.string.completed);
            }else if (orderList.get(position).getStatus().equals("4")){
                vh.status.setText(R.string.rejected);
            }else if (orderList.get(position).getStatus().equals("5")){
                vh.status.setText(R.string.cancelled);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (orderList.size() == 0){
            return orderList.size()+1;
        }else{
            return orderList.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (orderList.size() == 0){
            return EMPTY_TYPE;
        }else{
            return 1;
        }
    }

    class MainViewHolder extends RecyclerView.ViewHolder{

        MainViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class OrderViewHolder extends MainViewHolder implements View.OnClickListener{

        CustomTextView order_no,date, status, restro_name;
        CircleImageView ic_menu;

        OrderViewHolder(View itemView) {
            super(itemView);
            restro_name = (CustomTextView)itemView.findViewById(R.id.restro_name);
            status = (CustomTextView)itemView.findViewById(R.id.status);
            date = (CustomTextView)itemView.findViewById(R.id.date);
            order_no = (CustomTextView)itemView.findViewById(R.id.order_no);
            ic_menu = (CircleImageView)itemView.findViewById(R.id.ic_menu);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            context.startActivity(new Intent(context , OrderDetailsActivity.class)
                    .putExtra(Constants.ORDER_ID, orderList.get(getAdapterPosition()).getOrder_id()));
            Utility.activityTransition(context);
        }
    }

    public class EmptyViewHolder extends MainViewHolder{

        CustomTextView textView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            textView = (CustomTextView)itemView.findViewById(R.id.text);
        }
    }
}
