package com.smartfood.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.smartfood.R;
import com.smartfood.activities.OrderDetailsActivity;
import com.smartfood.customwidget.CircleImageView;
import com.smartfood.customwidget.CustomTextView;
import com.smartfood.model.OrderItem;
import com.smartfood.model.OrderParent;
import com.smartfood.utility.Constants;
import com.smartfood.utility.Utility;

import java.util.ArrayList;

/**
 * Created by and-05 on 5/12/17.
 */

public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemAdapter.OrderItemViewHolder> {

    private ArrayList<OrderItem> list;
    private Context context;
    private OrderParent orderParent;

    OrderItemAdapter(Context context, ArrayList<OrderItem> listItem, OrderParent orderParent) {
        this.context = context;
        this.list = listItem;
        this.orderParent = orderParent;
    }

    @Override
    public OrderItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OrderItemViewHolder(LayoutInflater.from(context).inflate(R.layout.item_order_item, parent, false));
    }

    @Override
    public void onBindViewHolder(OrderItemViewHolder holder, int position) {

        String price = list.get(position).getPrice()+" "+context.getString(R.string.currency);
        String qty =context.getString(R.string.qty)+" "+list.get(position).getQty();
        holder.menu_name.setText(list.get(position).getItem_name());
        holder.menu_price.setText(price);
        holder.quantity.setText(qty);
        Glide.with(context).load(list.get(position).getImg()).into(holder.ic_menu);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    

    class OrderItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        CustomTextView quantity, menu_price, menu_name;
        CircleImageView ic_menu;

        OrderItemViewHolder(View itemView) {
            super(itemView);
            menu_name = (CustomTextView)itemView.findViewById(R.id.menu_name);
            menu_price = (CustomTextView)itemView.findViewById(R.id.menu_price);
            quantity = (CustomTextView)itemView.findViewById(R.id.quantity);
            ic_menu = (CircleImageView) itemView.findViewById(R.id.ic_menu);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            context.startActivity(new Intent(context , OrderDetailsActivity.class)
                    .putExtra(Constants.ORDER_ID , orderParent.getOrder_id()));
            Utility.activityTransition(context);
        }
    }
}
