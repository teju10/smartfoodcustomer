package com.smartfood.services;

import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.smartfood.activities.OrderDetailsActivity;
import com.smartfood.utility.Constants;
import com.smartfood.utility.NotificationUtils;

import org.json.JSONObject;

/**
 * Created by Infograins on 9/26/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "From: " + remoteMessage.getData());
        JSONObject object = new JSONObject( remoteMessage.getData());
        Log.e(TAG, "onMessageReceived: "+object.toString() );

        try{

            if (remoteMessage.getData().size() > 0) {

                Intent intent = new Intent(this, OrderDetailsActivity.class)
                        .putExtra(Constants.ORDER_ID, object.getString(Constants.ORDER_ID));
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.showUniqueNotification(object.getString(Constants.NOTIFICATION),
                        pendingIntent, true, "",0);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
       /* Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra(Constants.MYDELIVERYFRAGMENT, "my_delivery");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 *//* Request code *//*, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 *//* ID of notification *//*, notificationBuilder.build());*/
    }
}