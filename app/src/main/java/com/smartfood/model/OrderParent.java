package com.smartfood.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by and-05 on 5/12/17.
 */

public class OrderParent implements Serializable{

    private String order_id;
    private String restro_name;
    private String status;
    private String total;
    private String date;
    private String order_no;
    private String image;
    private String cancel_reason;
    private String delivery_charges;
    private String delivery_lat;
    private String delivery_lon;
    private ArrayList<OrderItem> listItem;

    public OrderParent(String order_id, String restro_name, String status,
                       String cancel_reason,String delivery_charges, String total, String date, String order_no,
                       String image,String delivery_lat,String delivery_lon, ArrayList<OrderItem> listItem) {
        this.order_id = order_id;
        this.restro_name = restro_name;
        this.status = status;
        this.total = total;
        this.date = date;
        this.order_no = order_no;
        this.image = image;
        this.listItem = listItem;
        this.cancel_reason = cancel_reason;
        this.delivery_charges = delivery_charges;
        this.delivery_lat = delivery_lat;
        this.delivery_lon = delivery_lon;
    }

    public String getOrder_id() {
        return order_id;
    }

    public String getRestro_name() {
        return restro_name;
    }

    public String getStatus() {
        return status;
    }

    public String getTotal() {
        return total;
    }

    public String getDate() {
        return date;
    }

    public String getOrder_no() {
        return order_no;
    }

    public String getImage() {
        return image;
    }

    public String getCancel_reason() {
        return cancel_reason;
    }

    public String getDelivery_charges() {
        return delivery_charges;
    }

    public String getDelivery_lat() {
        return delivery_lat;
    }

    public String getDelivery_lon() {
        return delivery_lon;
    }

    public ArrayList<OrderItem> getListItem() {
        return listItem;
    }
}
