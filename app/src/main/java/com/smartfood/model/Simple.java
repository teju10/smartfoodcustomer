package com.smartfood.model;

/**
 * Created by and-05 on 16/11/17.
 */

public class Simple {

    private String id;
    private boolean isCheck;
    private String value;

    public Simple(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public Simple(String id, String value , boolean isCheck) {
        this.id = id;
        this.value = value;
        this.isCheck = isCheck;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    @Override
    public String toString() {
        return value;
    }
}
