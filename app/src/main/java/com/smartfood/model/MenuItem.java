package com.smartfood.model;

/**
 * Created by and-05 on 21/11/17.
 */

public class MenuItem {

    private String menu_id;
    private String menu_name;
    private String menu_price;
    private String menu_off_price;
    private String menu_image;
    private String menu_title;

    public MenuItem(String menu_id, String menu_name, String menu_price,String menu_off_price, String menu_image, String menu_title) {
        this.menu_id = menu_id;
        this.menu_name = menu_name;
        this.menu_price = menu_price;
        this.menu_image = menu_image;
        this.menu_title = menu_title;
        this.menu_off_price = menu_off_price;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public String getMenu_price() {
        return menu_price;
    }

    public String getMenu_image() {
        return menu_image;
    }

    public String getMenu_title() {
        return menu_title;
    }

    public String getMenu_off_price() {
        return menu_off_price;
    }
}
