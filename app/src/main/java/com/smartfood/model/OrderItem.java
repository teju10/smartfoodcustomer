package com.smartfood.model;

import java.io.Serializable;

/**
 * Created by and-05 on 5/12/17.
 */

public class OrderItem implements Serializable {

    private String id;
    private String item_name;
    private String price;
    private String qty;
    private String img;
    private String title;
    private String extras;
    private String subTotal;

    public OrderItem(String id, String item_name, String price, String qty,
                     String img, String title, String extras, String subTotal) {
        this.id = id;
        this.item_name = item_name;
        this.price = price;
        this.qty = qty;
        this.img = img;
        this.title = title;
        this.extras = extras;
        this.subTotal = subTotal;
    }

    public String getId() {
        return id;
    }

    public String getItem_name() {
        return item_name;
    }

    public String getPrice() {
        return price;
    }

    public String getQty() {
        return qty;
    }

    public String getImg() {
        return img;
    }

    public String getTitle() {
        return title;
    }

    public String getExtras() {
        return extras;
    }

    public String getSubTotal() {
        return subTotal;
    }
}
