package com.smartfood.model;

import java.util.ArrayList;

/**
 * Created by and-05 on 21/11/17.
 */

public class MenuList {

    private String cat_id;
    private String cat_name;
    private ArrayList<MenuItem> list;

    public MenuList(String cat_id, String cat_name, ArrayList<MenuItem> list) {
        this.cat_id = cat_id;
        this.cat_name = cat_name;
        this.list = list;
    }

    public String getCat_id() {
        return cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public ArrayList<MenuItem> getList() {
        return list;
    }
}
