package com.smartfood.model;

/**
 * Created by and-05 on 19/12/17.
 */

public class Notification {

    private String id;
    private String value;
    private String status;

    public Notification(String id, String value, String status) {
        this.id = id;
        this.value = value;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public String getStatus() {
        return status;
    }
}
