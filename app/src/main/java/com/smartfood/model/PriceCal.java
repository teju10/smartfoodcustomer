package com.smartfood.model;

import java.io.Serializable;

/**
 * Created by and-05 on 29/11/17.
 */

public class PriceCal implements Serializable {

    private String id;
    private String price;

    public PriceCal(String id, String price) {
        this.id = id;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
