package com.smartfood.model;

/**
 * Created by and-05 on 28/11/17.
 */

public class MenuExtra {

    private String id;
    private boolean isCheck;
    private String extra_name;
    private String extra_price;


    public MenuExtra(String id, String extra_name, String extra_price, boolean isCheck) {
        this.id = id;
        this.isCheck = isCheck;
        this.extra_name = extra_name;
        this.extra_price = extra_price;
    }


    public String getId() {
        return id;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public String getExtra_name() {
        return extra_name;
    }

    public String getExtra_price() {
        return extra_price;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }
}
